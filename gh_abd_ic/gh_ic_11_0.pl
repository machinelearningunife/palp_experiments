:- use_module(library(pita)).


:- pita.

:- begin_lpad.
% CP-theory for size 11

a0 :- a1.
abducible aba1.
0.5::a1:- aba1.
0.4999999975::a0;0.4999999975::a1:-a2.

abducible aba2.
0.5::a2:- aba2.
0.333333332222::a0;0.333333332222::a1;0.333333332222::a2:-a3.

abducible aba3.
0.5::a3:- aba3.
0.249999999375::a0;0.249999999375::a1;0.249999999375::a2;0.249999999375::a3:-a4.

abducible aba4.
0.5::a4:- aba4.
0.1999999996::a0;0.1999999996::a1;0.1999999996::a2;0.1999999996::a3;0.1999999996::a4:-a5.

abducible aba5.
0.5::a5:- aba5.
0.166666666389::a0;0.166666666389::a1;0.166666666389::a2;0.166666666389::a3;0.166666666389::a4;0.166666666389::a5:-a6.

abducible aba6.
0.5::a6:- aba6.
0.142857142653::a0;0.142857142653::a1;0.142857142653::a2;0.142857142653::a3;0.142857142653::a4;0.142857142653::a5;0.142857142653::a6:-a7.

abducible aba7.
0.5::a7:- aba7.
0.124999999844::a0;0.124999999844::a1;0.124999999844::a2;0.124999999844::a3;0.124999999844::a4;0.124999999844::a5;0.124999999844::a6;0.124999999844::a7:-a8.

abducible aba8.
0.5::a8:- aba8.
0.111111110988::a0;0.111111110988::a1;0.111111110988::a2;0.111111110988::a3;0.111111110988::a4;0.111111110988::a5;0.111111110988::a6;0.111111110988::a7;0.111111110988::a8:-a9.

abducible aba9.
0.5::a9:- aba9.
0.0999999999::a0;0.0999999999::a1;0.0999999999::a2;0.0999999999::a3;0.0999999999::a4;0.0999999999::a5;0.0999999999::a6;0.0999999999::a7;0.0999999999::a8;0.0999999999::a9:-a10.

abducible aba10.
0.5::a10:- aba10.

cons :- aba1,aba2.
cons :- aba1,aba3.
cons :- aba1,aba4.
cons :- aba1,aba5.
cons :- aba1,aba6.
cons :- aba1,aba7.
cons :- aba1,aba8.
cons :- aba1,aba9.
cons :- aba1,aba10.
cons :- aba2,aba3.
cons :- aba2,aba4.
cons :- aba2,aba5.
cons :- aba2,aba6.
cons :- aba2,aba7.
cons :- aba2,aba8.
cons :- aba2,aba9.
cons :- aba2,aba10.
cons :- aba3,aba4.
cons :- aba3,aba5.
cons :- aba3,aba6.
cons :- aba3,aba7.
cons :- aba3,aba8.
cons :- aba3,aba9.
cons :- aba3,aba10.
cons :- aba4,aba5.
cons :- aba4,aba6.
cons :- aba4,aba7.
cons :- aba4,aba8.
cons :- aba4,aba9.
cons :- aba4,aba10.
cons :- aba5,aba6.
cons :- aba5,aba7.
cons :- aba5,aba8.
cons :- aba5,aba9.
cons :- aba5,aba10.
cons :- aba6,aba7.
cons :- aba6,aba8.
cons :- aba6,aba9.
cons :- aba6,aba10.
cons :- aba7,aba8.
cons :- aba7,aba9.
cons :- aba7,aba10.
cons :- aba8,aba9.
cons :- aba8,aba10.
cons :- aba9,aba10.
cons :- \+aba1, \+aba2, \+aba3, \+aba4, \+aba5, \+aba6, \+aba7, \+aba8, \+aba9, \+aba10.
:- cons.

ev :- a0.

:- end_lpad.

run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

