if __name__ == "__main__":
    for i in range(2, 101):
        to_read = "bl" + str(i) + "_0.pl"
        f = open(to_read, "r")
        to_write = "prob_bl" + str(i) + "_0.pl"

        f_write = open(to_write, "w")
        # prob = 0.5
        counter = 0
        # abducible_printed = []
        for line in f:
            if ':- m(X),p(X).' in line:
                print('0.5:- m(X),p(X).', file=f_write)
            else:
                print(line, file=f_write)

        # print('\nrun:-\n\tstatistics(runtime, [Start | _]), \n\tabd_prob(ev, Prob, Abd),\n\tstatistics(runtime, [Stop | _]),\n\tRuntime is Stop - Start,\n\tformat(\'Prob ~w, Abd ~w: Runtime: ~w~n\', [Prob, Abd, Runtime]).\n', file=f_write)
        print('Generated dataset ' + str(i))
        f.close()
        f_write.close()
