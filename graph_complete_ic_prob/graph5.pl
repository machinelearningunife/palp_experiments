:- use_module(library(pita)).
:- pita.
:- begin_lpad.
path(A, B, N):-
path_(A, B, L),flatten(L, LF),length(LF, N).
path_(X, X, X).
path_(X, Y, [Y | T]):- path_(X, Z, T), edge(Z, Y).

0.5::edge(1,2):- e12.

abducible e12.

0.5::edge(1,3):- e13.

abducible e13.

0.5::edge(2,3):- e23.

abducible e23.

0.5::edge(1,4):- e14.

abducible e14.

0.5::edge(2,4):- e24.

abducible e24.

0.5::edge(3,4):- e34.

abducible e34.

0.5::edge(1,5):- e15.

abducible e15.

0.5::edge(2,5):- e25.

abducible e25.

0.5::edge(3,5):- e35.

abducible e35.

0.5::edge(4,5):- e45.

abducible e45.

ev:- path(1,5,_).
0.5:- path(1,5,2).
0.5:- path(1,5,3).
0.5:-path(1,5,4).
:- end_lpad.


run:- statistics(runtime, [Start | _]),abd_prob(ev, Prob, Abd),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).
