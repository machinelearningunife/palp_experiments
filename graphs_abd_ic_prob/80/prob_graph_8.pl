:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e04.

0.5::edge(0,4):- e04.

abducible e017.

0.5::edge(0,17):- e017.

abducible e035.

0.5::edge(0,35):- e035.

abducible e052.

0.5::edge(0,52):- e052.

abducible e056.

0.5::edge(0,56):- e056.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e15.

0.5::edge(1,5):- e15.

abducible e116.

0.5::edge(1,16):- e116.

abducible e118.

0.5::edge(1,18):- e118.

abducible e120.

0.5::edge(1,20):- e120.

abducible e121.

0.5::edge(1,21):- e121.

abducible e123.

0.5::edge(1,23):- e123.

abducible e124.

0.5::edge(1,24):- e124.

abducible e128.

0.5::edge(1,28):- e128.

abducible e131.

0.5::edge(1,31):- e131.

abducible e138.

0.5::edge(1,38):- e138.

abducible e143.

0.5::edge(1,43):- e143.

abducible e155.

0.5::edge(1,55):- e155.

abducible e160.

0.5::edge(1,60):- e160.

abducible e177.

0.5::edge(1,77):- e177.

abducible e23.

0.5::edge(2,3):- e23.

abducible e25.

0.5::edge(2,5):- e25.

abducible e27.

0.5::edge(2,7):- e27.

abducible e211.

0.5::edge(2,11):- e211.

abducible e212.

0.5::edge(2,12):- e212.

abducible e213.

0.5::edge(2,13):- e213.

abducible e219.

0.5::edge(2,19):- e219.

abducible e220.

0.5::edge(2,20):- e220.

abducible e223.

0.5::edge(2,23):- e223.

abducible e225.

0.5::edge(2,25):- e225.

abducible e234.

0.5::edge(2,34):- e234.

abducible e245.

0.5::edge(2,45):- e245.

abducible e247.

0.5::edge(2,47):- e247.

abducible e251.

0.5::edge(2,51):- e251.

abducible e258.

0.5::edge(2,58):- e258.

abducible e261.

0.5::edge(2,61):- e261.

abducible e262.

0.5::edge(2,62):- e262.

abducible e265.

0.5::edge(2,65):- e265.

abducible e34.

0.5::edge(3,4):- e34.

abducible e36.

0.5::edge(3,6):- e36.

abducible e316.

0.5::edge(3,16):- e316.

abducible e319.

0.5::edge(3,19):- e319.

abducible e321.

0.5::edge(3,21):- e321.

abducible e46.

0.5::edge(4,6):- e46.

abducible e49.

0.5::edge(4,9):- e49.

abducible e414.

0.5::edge(4,14):- e414.

abducible e417.

0.5::edge(4,17):- e417.

abducible e422.

0.5::edge(4,22):- e422.

abducible e426.

0.5::edge(4,26):- e426.

abducible e427.

0.5::edge(4,27):- e427.

abducible e430.

0.5::edge(4,30):- e430.

abducible e441.

0.5::edge(4,41):- e441.

abducible e446.

0.5::edge(4,46):- e446.

abducible e452.

0.5::edge(4,52):- e452.

abducible e57.

0.5::edge(5,7):- e57.

abducible e58.

0.5::edge(5,8):- e58.

abducible e510.

0.5::edge(5,10):- e510.

abducible e514.

0.5::edge(5,14):- e514.

abducible e611.

0.5::edge(6,11):- e611.

abducible e626.

0.5::edge(6,26):- e626.

abducible e627.

0.5::edge(6,27):- e627.

abducible e637.

0.5::edge(6,37):- e637.

abducible e666.

0.5::edge(6,66):- e666.

abducible e78.

0.5::edge(7,8):- e78.

abducible e79.

0.5::edge(7,9):- e79.

abducible e710.

0.5::edge(7,10):- e710.

abducible e713.

0.5::edge(7,13):- e713.

abducible e718.

0.5::edge(7,18):- e718.

abducible e729.

0.5::edge(7,29):- e729.

abducible e736.

0.5::edge(7,36):- e736.

abducible e737.

0.5::edge(7,37):- e737.

abducible e740.

0.5::edge(7,40):- e740.

abducible e755.

0.5::edge(7,55):- e755.

abducible e766.

0.5::edge(7,66):- e766.

abducible e767.

0.5::edge(7,67):- e767.

abducible e770.

0.5::edge(7,70):- e770.

abducible e771.

0.5::edge(7,71):- e771.

abducible e812.

0.5::edge(8,12):- e812.

abducible e815.

0.5::edge(8,15):- e815.

abducible e824.

0.5::edge(8,24):- e824.

abducible e833.

0.5::edge(8,33):- e833.

abducible e873.

0.5::edge(8,73):- e873.

abducible e915.

0.5::edge(9,15):- e915.

abducible e922.

0.5::edge(9,22):- e922.

abducible e928.

0.5::edge(9,28):- e928.

abducible e938.

0.5::edge(9,38):- e938.

abducible e939.

0.5::edge(9,39):- e939.

abducible e945.

0.5::edge(9,45):- e945.

abducible e959.

0.5::edge(9,59):- e959.

abducible e974.

0.5::edge(9,74):- e974.

abducible e1033.

0.5::edge(10,33):- e1033.

abducible e1050.

0.5::edge(10,50):- e1050.

abducible e1259.

0.5::edge(12,59):- e1259.

abducible e1264.

0.5::edge(12,64):- e1264.

abducible e1265.

0.5::edge(12,65):- e1265.

abducible e1332.

0.5::edge(13,32):- e1332.

abducible e1351.

0.5::edge(13,51):- e1351.

abducible e1439.

0.5::edge(14,39):- e1439.

abducible e1546.

0.5::edge(15,46):- e1546.

abducible e1578.

0.5::edge(15,78):- e1578.

abducible e1647.

0.5::edge(16,47):- e1647.

abducible e1663.

0.5::edge(16,63):- e1663.

abducible e1844.

0.5::edge(18,44):- e1844.

abducible e1854.

0.5::edge(18,54):- e1854.

abducible e1931.

0.5::edge(19,31):- e1931.

abducible e1932.

0.5::edge(19,32):- e1932.

abducible e1942.

0.5::edge(19,42):- e1942.

abducible e1961.

0.5::edge(19,61):- e1961.

abducible e1962.

0.5::edge(19,62):- e1962.

abducible e1979.

0.5::edge(19,79):- e1979.

abducible e2029.

0.5::edge(20,29):- e2029.

abducible e2125.

0.5::edge(21,25):- e2125.

abducible e2144.

0.5::edge(21,44):- e2144.

abducible e2149.

0.5::edge(21,49):- e2149.

abducible e2168.

0.5::edge(21,68):- e2168.

abducible e2169.

0.5::edge(21,69):- e2169.

abducible e2175.

0.5::edge(21,75):- e2175.

abducible e2240.

0.5::edge(22,40):- e2240.

abducible e2268.

0.5::edge(22,68):- e2268.

abducible e2269.

0.5::edge(22,69):- e2269.

abducible e2277.

0.5::edge(22,77):- e2277.

abducible e2367.

0.5::edge(23,67):- e2367.

abducible e2448.

0.5::edge(24,48):- e2448.

abducible e2673.

0.5::edge(26,73):- e2673.

abducible e2830.

0.5::edge(28,30):- e2830.

abducible e3134.

0.5::edge(31,34):- e3134.

abducible e3149.

0.5::edge(31,49):- e3149.

abducible e3235.

0.5::edge(32,35):- e3235.

abducible e3253.

0.5::edge(32,53):- e3253.

abducible e3257.

0.5::edge(32,57):- e3257.

abducible e3336.

0.5::edge(33,36):- e3336.

abducible e3472.

0.5::edge(34,72):- e3472.

abducible e3548.

0.5::edge(35,48):- e3548.

abducible e3558.

0.5::edge(35,58):- e3558.

abducible e3774.

0.5::edge(37,74):- e3774.

abducible e3841.

0.5::edge(38,41):- e3841.

abducible e3842.

0.5::edge(38,42):- e3842.

abducible e3850.

0.5::edge(38,50):- e3850.

abducible e3871.

0.5::edge(38,71):- e3871.

abducible e4157.

0.5::edge(41,57):- e4157.

abducible e4172.

0.5::edge(41,72):- e4172.

abducible e4178.

0.5::edge(41,78):- e4178.

abducible e4243.

0.5::edge(42,43):- e4243.

abducible e4654.

0.5::edge(46,54):- e4654.

abducible e4853.

0.5::edge(48,53):- e4853.

abducible e4860.

0.5::edge(48,60):- e4860.

abducible e4975.

0.5::edge(49,75):- e4975.

abducible e5356.

0.5::edge(53,56):- e5356.

abducible e5576.

0.5::edge(55,76):- e5576.

abducible e5970.

0.5::edge(59,70):- e5970.

abducible e6063.

0.5::edge(60,63):- e6063.

abducible e6164.

0.5::edge(61,64):- e6164.

abducible e6579.

0.5::edge(65,79):- e6579.

abducible e6876.

0.5::edge(68,76):- e6876.



:- path(0, 79 ,L), L < 6.





ev :- path(0,79,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

