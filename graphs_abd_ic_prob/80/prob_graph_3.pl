:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e03.

0.5::edge(0,3):- e03.

abducible e04.

0.5::edge(0,4):- e04.

abducible e06.

0.5::edge(0,6):- e06.

abducible e07.

0.5::edge(0,7):- e07.

abducible e08.

0.5::edge(0,8):- e08.

abducible e09.

0.5::edge(0,9):- e09.

abducible e010.

0.5::edge(0,10):- e010.

abducible e012.

0.5::edge(0,12):- e012.

abducible e016.

0.5::edge(0,16):- e016.

abducible e026.

0.5::edge(0,26):- e026.

abducible e028.

0.5::edge(0,28):- e028.

abducible e030.

0.5::edge(0,30):- e030.

abducible e032.

0.5::edge(0,32):- e032.

abducible e040.

0.5::edge(0,40):- e040.

abducible e044.

0.5::edge(0,44):- e044.

abducible e045.

0.5::edge(0,45):- e045.

abducible e049.

0.5::edge(0,49):- e049.

abducible e059.

0.5::edge(0,59):- e059.

abducible e061.

0.5::edge(0,61):- e061.

abducible e068.

0.5::edge(0,68):- e068.

abducible e12.

0.5::edge(1,2):- e12.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e27.

0.5::edge(2,7):- e27.

abducible e219.

0.5::edge(2,19):- e219.

abducible e220.

0.5::edge(2,20):- e220.

abducible e228.

0.5::edge(2,28):- e228.

abducible e237.

0.5::edge(2,37):- e237.

abducible e255.

0.5::edge(2,55):- e255.

abducible e260.

0.5::edge(2,60):- e260.

abducible e35.

0.5::edge(3,5):- e35.

abducible e38.

0.5::edge(3,8):- e38.

abducible e39.

0.5::edge(3,9):- e39.

abducible e311.

0.5::edge(3,11):- e311.

abducible e315.

0.5::edge(3,15):- e315.

abducible e317.

0.5::edge(3,17):- e317.

abducible e322.

0.5::edge(3,22):- e322.

abducible e334.

0.5::edge(3,34):- e334.

abducible e338.

0.5::edge(3,38):- e338.

abducible e340.

0.5::edge(3,40):- e340.

abducible e341.

0.5::edge(3,41):- e341.

abducible e345.

0.5::edge(3,45):- e345.

abducible e350.

0.5::edge(3,50):- e350.

abducible e351.

0.5::edge(3,51):- e351.

abducible e352.

0.5::edge(3,52):- e352.

abducible e353.

0.5::edge(3,53):- e353.

abducible e358.

0.5::edge(3,58):- e358.

abducible e367.

0.5::edge(3,67):- e367.

abducible e374.

0.5::edge(3,74):- e374.

abducible e375.

0.5::edge(3,75):- e375.

abducible e378.

0.5::edge(3,78):- e378.

abducible e45.

0.5::edge(4,5):- e45.

abducible e46.

0.5::edge(4,6):- e46.

abducible e413.

0.5::edge(4,13):- e413.

abducible e418.

0.5::edge(4,18):- e418.

abducible e422.

0.5::edge(4,22):- e422.

abducible e424.

0.5::edge(4,24):- e424.

abducible e425.

0.5::edge(4,25):- e425.

abducible e426.

0.5::edge(4,26):- e426.

abducible e431.

0.5::edge(4,31):- e431.

abducible e441.

0.5::edge(4,41):- e441.

abducible e449.

0.5::edge(4,49):- e449.

abducible e452.

0.5::edge(4,52):- e452.

abducible e473.

0.5::edge(4,73):- e473.

abducible e530.

0.5::edge(5,30):- e530.

abducible e613.

0.5::edge(6,13):- e613.

abducible e614.

0.5::edge(6,14):- e614.

abducible e615.

0.5::edge(6,15):- e615.

abducible e623.

0.5::edge(6,23):- e623.

abducible e644.

0.5::edge(6,44):- e644.

abducible e661.

0.5::edge(6,61):- e661.

abducible e810.

0.5::edge(8,10):- e810.

abducible e812.

0.5::edge(8,12):- e812.

abducible e817.

0.5::edge(8,17):- e817.

abducible e820.

0.5::edge(8,20):- e820.

abducible e823.

0.5::edge(8,23):- e823.

abducible e836.

0.5::edge(8,36):- e836.

abducible e839.

0.5::edge(8,39):- e839.

abducible e842.

0.5::edge(8,42):- e842.

abducible e856.

0.5::edge(8,56):- e856.

abducible e911.

0.5::edge(9,11):- e911.

abducible e914.

0.5::edge(9,14):- e914.

abducible e932.

0.5::edge(9,32):- e932.

abducible e948.

0.5::edge(9,48):- e948.

abducible e955.

0.5::edge(9,55):- e955.

abducible e956.

0.5::edge(9,56):- e956.

abducible e966.

0.5::edge(9,66):- e966.

abducible e971.

0.5::edge(9,71):- e971.

abducible e1027.

0.5::edge(10,27):- e1027.

abducible e1031.

0.5::edge(10,31):- e1031.

abducible e1066.

0.5::edge(10,66):- e1066.

abducible e1143.

0.5::edge(11,43):- e1143.

abducible e1229.

0.5::edge(12,29):- e1229.

abducible e1234.

0.5::edge(12,34):- e1234.

abducible e1243.

0.5::edge(12,43):- e1243.

abducible e1264.

0.5::edge(12,64):- e1264.

abducible e1318.

0.5::edge(13,18):- e1318.

abducible e1416.

0.5::edge(14,16):- e1416.

abducible e1435.

0.5::edge(14,35):- e1435.

abducible e1527.

0.5::edge(15,27):- e1527.

abducible e1621.

0.5::edge(16,21):- e1621.

abducible e1654.

0.5::edge(16,54):- e1654.

abducible e1663.

0.5::edge(16,63):- e1663.

abducible e1665.

0.5::edge(16,65):- e1665.

abducible e1671.

0.5::edge(16,71):- e1671.

abducible e1719.

0.5::edge(17,19):- e1719.

abducible e1725.

0.5::edge(17,25):- e1725.

abducible e1735.

0.5::edge(17,35):- e1735.

abducible e1736.

0.5::edge(17,36):- e1736.

abducible e1742.

0.5::edge(17,42):- e1742.

abducible e1748.

0.5::edge(17,48):- e1748.

abducible e1824.

0.5::edge(18,24):- e1824.

abducible e1870.

0.5::edge(18,70):- e1870.

abducible e2021.

0.5::edge(20,21):- e2021.

abducible e2029.

0.5::edge(20,29):- e2029.

abducible e2033.

0.5::edge(20,33):- e2033.

abducible e2039.

0.5::edge(20,39):- e2039.

abducible e2046.

0.5::edge(20,46):- e2046.

abducible e2053.

0.5::edge(20,53):- e2053.

abducible e2054.

0.5::edge(20,54):- e2054.

abducible e2065.

0.5::edge(20,65):- e2065.

abducible e2133.

0.5::edge(21,33):- e2133.

abducible e2162.

0.5::edge(21,62):- e2162.

abducible e2351.

0.5::edge(23,51):- e2351.

abducible e2537.

0.5::edge(25,37):- e2537.

abducible e2538.

0.5::edge(25,38):- e2538.

abducible e2677.

0.5::edge(26,77):- e2677.

abducible e2769.

0.5::edge(27,69):- e2769.

abducible e2872.

0.5::edge(28,72):- e2872.

abducible e2957.

0.5::edge(29,57):- e2957.

abducible e3246.

0.5::edge(32,46):- e3246.

abducible e3347.

0.5::edge(33,47):- e3347.

abducible e3350.

0.5::edge(33,50):- e3350.

abducible e3647.

0.5::edge(36,47):- e3647.

abducible e3675.

0.5::edge(36,75):- e3675.

abducible e3877.

0.5::edge(38,77):- e3877.

abducible e3979.

0.5::edge(39,79):- e3979.

abducible e4158.

0.5::edge(41,58):- e4158.

abducible e4167.

0.5::edge(41,67):- e4167.

abducible e4262.

0.5::edge(42,62):- e4262.

abducible e4969.

0.5::edge(49,69):- e4969.

abducible e5078.

0.5::edge(50,78):- e5078.

abducible e5079.

0.5::edge(50,79):- e5079.

abducible e5257.

0.5::edge(52,57):- e5257.

abducible e5263.

0.5::edge(52,63):- e5263.

abducible e5760.

0.5::edge(57,60):- e5760.

abducible e5776.

0.5::edge(57,76):- e5776.

abducible e5859.

0.5::edge(58,59):- e5859.

abducible e5872.

0.5::edge(58,72):- e5872.

abducible e6364.

0.5::edge(63,64):- e6364.

abducible e6676.

0.5::edge(66,76):- e6676.

abducible e6768.

0.5::edge(67,68):- e6768.

abducible e6770.

0.5::edge(67,70):- e6770.

abducible e6974.

0.5::edge(69,74):- e6974.

abducible e7273.

0.5::edge(72,73):- e7273.



:- path(0, 79 ,L), L < 6.





ev :- path(0,79,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

