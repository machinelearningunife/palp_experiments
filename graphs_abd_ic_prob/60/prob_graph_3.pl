:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e04.

0.5::edge(0,4):- e04.

abducible e010.

0.5::edge(0,10):- e010.

abducible e013.

0.5::edge(0,13):- e013.

abducible e025.

0.5::edge(0,25):- e025.

abducible e044.

0.5::edge(0,44):- e044.

abducible e047.

0.5::edge(0,47):- e047.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e15.

0.5::edge(1,5):- e15.

abducible e16.

0.5::edge(1,6):- e16.

abducible e17.

0.5::edge(1,7):- e17.

abducible e18.

0.5::edge(1,8):- e18.

abducible e19.

0.5::edge(1,9):- e19.

abducible e113.

0.5::edge(1,13):- e113.

abducible e115.

0.5::edge(1,15):- e115.

abducible e119.

0.5::edge(1,19):- e119.

abducible e121.

0.5::edge(1,21):- e121.

abducible e122.

0.5::edge(1,22):- e122.

abducible e129.

0.5::edge(1,29):- e129.

abducible e130.

0.5::edge(1,30):- e130.

abducible e131.

0.5::edge(1,31):- e131.

abducible e136.

0.5::edge(1,36):- e136.

abducible e151.

0.5::edge(1,51):- e151.

abducible e23.

0.5::edge(2,3):- e23.

abducible e27.

0.5::edge(2,7):- e27.

abducible e212.

0.5::edge(2,12):- e212.

abducible e216.

0.5::edge(2,16):- e216.

abducible e220.

0.5::edge(2,20):- e220.

abducible e223.

0.5::edge(2,23):- e223.

abducible e225.

0.5::edge(2,25):- e225.

abducible e246.

0.5::edge(2,46):- e246.

abducible e255.

0.5::edge(2,55):- e255.

abducible e257.

0.5::edge(2,57):- e257.

abducible e35.

0.5::edge(3,5):- e35.

abducible e36.

0.5::edge(3,6):- e36.

abducible e311.

0.5::edge(3,11):- e311.

abducible e316.

0.5::edge(3,16):- e316.

abducible e319.

0.5::edge(3,19):- e319.

abducible e320.

0.5::edge(3,20):- e320.

abducible e324.

0.5::edge(3,24):- e324.

abducible e355.

0.5::edge(3,55):- e355.

abducible e359.

0.5::edge(3,59):- e359.

abducible e438.

0.5::edge(4,38):- e438.

abducible e452.

0.5::edge(4,52):- e452.

abducible e517.

0.5::edge(5,17):- e517.

abducible e526.

0.5::edge(5,26):- e526.

abducible e538.

0.5::edge(5,38):- e538.

abducible e610.

0.5::edge(6,10):- e610.

abducible e632.

0.5::edge(6,32):- e632.

abducible e633.

0.5::edge(6,33):- e633.

abducible e78.

0.5::edge(7,8):- e78.

abducible e79.

0.5::edge(7,9):- e79.

abducible e711.

0.5::edge(7,11):- e711.

abducible e712.

0.5::edge(7,12):- e712.

abducible e714.

0.5::edge(7,14):- e714.

abducible e717.

0.5::edge(7,17):- e717.

abducible e718.

0.5::edge(7,18):- e718.

abducible e723.

0.5::edge(7,23):- e723.

abducible e734.

0.5::edge(7,34):- e734.

abducible e749.

0.5::edge(7,49):- e749.

abducible e753.

0.5::edge(7,53):- e753.

abducible e756.

0.5::edge(7,56):- e756.

abducible e839.

0.5::edge(8,39):- e839.

abducible e840.

0.5::edge(8,40):- e840.

abducible e921.

0.5::edge(9,21):- e921.

abducible e929.

0.5::edge(9,29):- e929.

abducible e945.

0.5::edge(9,45):- e945.

abducible e951.

0.5::edge(9,51):- e951.

abducible e1128.

0.5::edge(11,28):- e1128.

abducible e1131.

0.5::edge(11,31):- e1131.

abducible e1142.

0.5::edge(11,42):- e1142.

abducible e1145.

0.5::edge(11,45):- e1145.

abducible e1150.

0.5::edge(11,50):- e1150.

abducible e1227.

0.5::edge(12,27):- e1227.

abducible e1230.

0.5::edge(12,30):- e1230.

abducible e1234.

0.5::edge(12,34):- e1234.

abducible e1236.

0.5::edge(12,36):- e1236.

abducible e1250.

0.5::edge(12,50):- e1250.

abducible e1314.

0.5::edge(13,14):- e1314.

abducible e1315.

0.5::edge(13,15):- e1315.

abducible e1318.

0.5::edge(13,18):- e1318.

abducible e1354.

0.5::edge(13,54):- e1354.

abducible e1526.

0.5::edge(15,26):- e1526.

abducible e1535.

0.5::edge(15,35):- e1535.

abducible e1622.

0.5::edge(16,22):- e1622.

abducible e1858.

0.5::edge(18,58):- e1858.

abducible e1924.

0.5::edge(19,24):- e1924.

abducible e1927.

0.5::edge(19,27):- e1927.

abducible e2032.

0.5::edge(20,32):- e2032.

abducible e2042.

0.5::edge(20,42):- e2042.

abducible e2153.

0.5::edge(21,53):- e2153.

abducible e2328.

0.5::edge(23,28):- e2328.

abducible e2333.

0.5::edge(23,33):- e2333.

abducible e2335.

0.5::edge(23,35):- e2335.

abducible e2637.

0.5::edge(26,37):- e2637.

abducible e3054.

0.5::edge(30,54):- e3054.

abducible e3141.

0.5::edge(31,41):- e3141.

abducible e3148.

0.5::edge(31,48):- e3148.

abducible e3248.

0.5::edge(32,48):- e3248.

abducible e3258.

0.5::edge(32,58):- e3258.

abducible e3339.

0.5::edge(33,39):- e3339.

abducible e3340.

0.5::edge(33,40):- e3340.

abducible e3343.

0.5::edge(33,43):- e3343.

abducible e3349.

0.5::edge(33,49):- e3349.

abducible e3357.

0.5::edge(33,57):- e3357.

abducible e3546.

0.5::edge(35,46):- e3546.

abducible e3637.

0.5::edge(36,37):- e3637.

abducible e3644.

0.5::edge(36,44):- e3644.

abducible e3941.

0.5::edge(39,41):- e3941.

abducible e3943.

0.5::edge(39,43):- e3943.

abducible e3956.

0.5::edge(39,56):- e3956.

abducible e4347.

0.5::edge(43,47):- e4347.

abducible e4959.

0.5::edge(49,59):- e4959.

abducible e5152.

0.5::edge(51,52):- e5152.



:- path(0, 59 ,L), L < 6.





ev :- path(0,59,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

