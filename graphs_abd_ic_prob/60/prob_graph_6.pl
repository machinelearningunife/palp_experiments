:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e05.

0.5::edge(0,5):- e05.

abducible e016.

0.5::edge(0,16):- e016.

abducible e038.

0.5::edge(0,38):- e038.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e15.

0.5::edge(1,5):- e15.

abducible e16.

0.5::edge(1,6):- e16.

abducible e18.

0.5::edge(1,8):- e18.

abducible e19.

0.5::edge(1,9):- e19.

abducible e111.

0.5::edge(1,11):- e111.

abducible e117.

0.5::edge(1,17):- e117.

abducible e119.

0.5::edge(1,19):- e119.

abducible e120.

0.5::edge(1,20):- e120.

abducible e125.

0.5::edge(1,25):- e125.

abducible e126.

0.5::edge(1,26):- e126.

abducible e128.

0.5::edge(1,28):- e128.

abducible e129.

0.5::edge(1,29):- e129.

abducible e137.

0.5::edge(1,37):- e137.

abducible e140.

0.5::edge(1,40):- e140.

abducible e147.

0.5::edge(1,47):- e147.

abducible e148.

0.5::edge(1,48):- e148.

abducible e150.

0.5::edge(1,50):- e150.

abducible e151.

0.5::edge(1,51):- e151.

abducible e156.

0.5::edge(1,56):- e156.

abducible e158.

0.5::edge(1,58):- e158.

abducible e159.

0.5::edge(1,59):- e159.

abducible e23.

0.5::edge(2,3):- e23.

abducible e26.

0.5::edge(2,6):- e26.

abducible e27.

0.5::edge(2,7):- e27.

abducible e212.

0.5::edge(2,12):- e212.

abducible e220.

0.5::edge(2,20):- e220.

abducible e227.

0.5::edge(2,27):- e227.

abducible e232.

0.5::edge(2,32):- e232.

abducible e234.

0.5::edge(2,34):- e234.

abducible e245.

0.5::edge(2,45):- e245.

abducible e256.

0.5::edge(2,56):- e256.

abducible e34.

0.5::edge(3,4):- e34.

abducible e316.

0.5::edge(3,16):- e316.

abducible e321.

0.5::edge(3,21):- e321.

abducible e328.

0.5::edge(3,28):- e328.

abducible e337.

0.5::edge(3,37):- e337.

abducible e47.

0.5::edge(4,7):- e47.

abducible e436.

0.5::edge(4,36):- e436.

abducible e515.

0.5::edge(5,15):- e515.

abducible e527.

0.5::edge(5,27):- e527.

abducible e543.

0.5::edge(5,43):- e543.

abducible e69.

0.5::edge(6,9):- e69.

abducible e615.

0.5::edge(6,15):- e615.

abducible e624.

0.5::edge(6,24):- e624.

abducible e631.

0.5::edge(6,31):- e631.

abducible e652.

0.5::edge(6,52):- e652.

abducible e658.

0.5::edge(6,58):- e658.

abducible e78.

0.5::edge(7,8):- e78.

abducible e710.

0.5::edge(7,10):- e710.

abducible e712.

0.5::edge(7,12):- e712.

abducible e713.

0.5::edge(7,13):- e713.

abducible e714.

0.5::edge(7,14):- e714.

abducible e723.

0.5::edge(7,23):- e723.

abducible e724.

0.5::edge(7,24):- e724.

abducible e725.

0.5::edge(7,25):- e725.

abducible e742.

0.5::edge(7,42):- e742.

abducible e811.

0.5::edge(8,11):- e811.

abducible e846.

0.5::edge(8,46):- e846.

abducible e910.

0.5::edge(9,10):- e910.

abducible e917.

0.5::edge(9,17):- e917.

abducible e934.

0.5::edge(9,34):- e934.

abducible e944.

0.5::edge(9,44):- e944.

abducible e1029.

0.5::edge(10,29):- e1029.

abducible e1113.

0.5::edge(11,13):- e1113.

abducible e1118.

0.5::edge(11,18):- e1118.

abducible e1119.

0.5::edge(11,19):- e1119.

abducible e1138.

0.5::edge(11,38):- e1138.

abducible e1140.

0.5::edge(11,40):- e1140.

abducible e1214.

0.5::edge(12,14):- e1214.

abducible e1222.

0.5::edge(12,22):- e1222.

abducible e1223.

0.5::edge(12,23):- e1223.

abducible e1231.

0.5::edge(12,31):- e1231.

abducible e1236.

0.5::edge(12,36):- e1236.

abducible e1239.

0.5::edge(12,39):- e1239.

abducible e1246.

0.5::edge(12,46):- e1246.

abducible e1259.

0.5::edge(12,59):- e1259.

abducible e1326.

0.5::edge(13,26):- e1326.

abducible e1330.

0.5::edge(13,30):- e1330.

abducible e1333.

0.5::edge(13,33):- e1333.

abducible e1335.

0.5::edge(13,35):- e1335.

abducible e1339.

0.5::edge(13,39):- e1339.

abducible e1351.

0.5::edge(13,51):- e1351.

abducible e1353.

0.5::edge(13,53):- e1353.

abducible e1450.

0.5::edge(14,50):- e1450.

abducible e1647.

0.5::edge(16,47):- e1647.

abducible e1655.

0.5::edge(16,55):- e1655.

abducible e1718.

0.5::edge(17,18):- e1718.

abducible e1721.

0.5::edge(17,21):- e1721.

abducible e1757.

0.5::edge(17,57):- e1757.

abducible e1832.

0.5::edge(18,32):- e1832.

abducible e1949.

0.5::edge(19,49):- e1949.

abducible e2022.

0.5::edge(20,22):- e2022.

abducible e2054.

0.5::edge(20,54):- e2054.

abducible e2130.

0.5::edge(21,30):- e2130.

abducible e2149.

0.5::edge(21,49):- e2149.

abducible e2241.

0.5::edge(22,41):- e2241.

abducible e2343.

0.5::edge(23,43):- e2343.

abducible e2642.

0.5::edge(26,42):- e2642.

abducible e2735.

0.5::edge(27,35):- e2735.

abducible e2945.

0.5::edge(29,45):- e2945.

abducible e3054.

0.5::edge(30,54):- e3054.

abducible e3133.

0.5::edge(31,33):- e3133.

abducible e3152.

0.5::edge(31,52):- e3152.

abducible e3241.

0.5::edge(32,41):- e3241.

abducible e3748.

0.5::edge(37,48):- e3748.

abducible e3757.

0.5::edge(37,57):- e3757.

abducible e4044.

0.5::edge(40,44):- e4044.

abducible e5153.

0.5::edge(51,53):- e5153.

abducible e5155.

0.5::edge(51,55):- e5155.



:- path(0, 59 ,L), L < 6.





ev :- path(0,59,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

