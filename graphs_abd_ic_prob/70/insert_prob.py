if __name__ == "__main__":
    for i in range(1, 11):
        to_read = "graph_" + str(i) + ".pl"
        f = open(to_read, "r")
        to_write = "prob_graph_" + str(i) + ".pl"

        f_write = open(to_write, "w")
        # prob = 0.5
        counter = 0
        # abducible_printed = []
        for line in f:
            if ':- path(0, 49 ,L), L < 6.' in line:
                print('0.5:- path(0, 49 ,L), L < 6.', file=f_write)
            else:
                print(line, file=f_write)

        # print('\nrun:-\n\tstatistics(runtime, [Start | _]), \n\tabd_prob(ev, Prob, Abd),\n\tstatistics(runtime, [Stop | _]),\n\tRuntime is Stop - Start,\n\tformat(\'Prob ~w, Abd ~w: Runtime: ~w~n\', [Prob, Abd, Runtime]).\n', file=f_write)
        print('Generated dataset ' + str(i))
        f.close()
        f_write.close()
