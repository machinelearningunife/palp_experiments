:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e04.

0.5::edge(0,4):- e04.

abducible e07.

0.5::edge(0,7):- e07.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e17.

0.5::edge(1,7):- e17.

abducible e18.

0.5::edge(1,8):- e18.

abducible e110.

0.5::edge(1,10):- e110.

abducible e116.

0.5::edge(1,16):- e116.

abducible e117.

0.5::edge(1,17):- e117.

abducible e119.

0.5::edge(1,19):- e119.

abducible e121.

0.5::edge(1,21):- e121.

abducible e132.

0.5::edge(1,32):- e132.

abducible e135.

0.5::edge(1,35):- e135.

abducible e137.

0.5::edge(1,37):- e137.

abducible e143.

0.5::edge(1,43):- e143.

abducible e153.

0.5::edge(1,53):- e153.

abducible e163.

0.5::edge(1,63):- e163.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e25.

0.5::edge(2,5):- e25.

abducible e26.

0.5::edge(2,6):- e26.

abducible e28.

0.5::edge(2,8):- e28.

abducible e214.

0.5::edge(2,14):- e214.

abducible e218.

0.5::edge(2,18):- e218.

abducible e221.

0.5::edge(2,21):- e221.

abducible e225.

0.5::edge(2,25):- e225.

abducible e229.

0.5::edge(2,29):- e229.

abducible e233.

0.5::edge(2,33):- e233.

abducible e236.

0.5::edge(2,36):- e236.

abducible e240.

0.5::edge(2,40):- e240.

abducible e241.

0.5::edge(2,41):- e241.

abducible e252.

0.5::edge(2,52):- e252.

abducible e256.

0.5::edge(2,56):- e256.

abducible e39.

0.5::edge(3,9):- e39.

abducible e318.

0.5::edge(3,18):- e318.

abducible e322.

0.5::edge(3,22):- e322.

abducible e326.

0.5::edge(3,26):- e326.

abducible e330.

0.5::edge(3,30):- e330.

abducible e332.

0.5::edge(3,32):- e332.

abducible e334.

0.5::edge(3,34):- e334.

abducible e338.

0.5::edge(3,38):- e338.

abducible e346.

0.5::edge(3,46):- e346.

abducible e351.

0.5::edge(3,51):- e351.

abducible e355.

0.5::edge(3,55):- e355.

abducible e368.

0.5::edge(3,68):- e368.

abducible e369.

0.5::edge(3,69):- e369.

abducible e45.

0.5::edge(4,5):- e45.

abducible e411.

0.5::edge(4,11):- e411.

abducible e412.

0.5::edge(4,12):- e412.

abducible e413.

0.5::edge(4,13):- e413.

abducible e416.

0.5::edge(4,16):- e416.

abducible e429.

0.5::edge(4,29):- e429.

abducible e440.

0.5::edge(4,40):- e440.

abducible e449.

0.5::edge(4,49):- e449.

abducible e457.

0.5::edge(4,57):- e457.

abducible e460.

0.5::edge(4,60):- e460.

abducible e461.

0.5::edge(4,61):- e461.

abducible e466.

0.5::edge(4,66):- e466.

abducible e467.

0.5::edge(4,67):- e467.

abducible e56.

0.5::edge(5,6):- e56.

abducible e59.

0.5::edge(5,9):- e59.

abducible e512.

0.5::edge(5,12):- e512.

abducible e513.

0.5::edge(5,13):- e513.

abducible e514.

0.5::edge(5,14):- e514.

abducible e519.

0.5::edge(5,19):- e519.

abducible e523.

0.5::edge(5,23):- e523.

abducible e524.

0.5::edge(5,24):- e524.

abducible e530.

0.5::edge(5,30):- e530.

abducible e531.

0.5::edge(5,31):- e531.

abducible e536.

0.5::edge(5,36):- e536.

abducible e539.

0.5::edge(5,39):- e539.

abducible e547.

0.5::edge(5,47):- e547.

abducible e554.

0.5::edge(5,54):- e554.

abducible e557.

0.5::edge(5,57):- e557.

abducible e654.

0.5::edge(6,54):- e654.

abducible e710.

0.5::edge(7,10):- e710.

abducible e715.

0.5::edge(7,15):- e715.

abducible e728.

0.5::edge(7,28):- e728.

abducible e739.

0.5::edge(7,39):- e739.

abducible e748.

0.5::edge(7,48):- e748.

abducible e750.

0.5::edge(7,50):- e750.

abducible e764.

0.5::edge(7,64):- e764.

abducible e835.

0.5::edge(8,35):- e835.

abducible e861.

0.5::edge(8,61):- e861.

abducible e869.

0.5::edge(8,69):- e869.

abducible e911.

0.5::edge(9,11):- e911.

abducible e917.

0.5::edge(9,17):- e917.

abducible e920.

0.5::edge(9,20):- e920.

abducible e924.

0.5::edge(9,24):- e924.

abducible e926.

0.5::edge(9,26):- e926.

abducible e933.

0.5::edge(9,33):- e933.

abducible e942.

0.5::edge(9,42):- e942.

abducible e943.

0.5::edge(9,43):- e943.

abducible e948.

0.5::edge(9,48):- e948.

abducible e962.

0.5::edge(9,62):- e962.

abducible e965.

0.5::edge(9,65):- e965.

abducible e1115.

0.5::edge(11,15):- e1115.

abducible e1128.

0.5::edge(11,28):- e1128.

abducible e1163.

0.5::edge(11,63):- e1163.

abducible e1220.

0.5::edge(12,20):- e1220.

abducible e1247.

0.5::edge(12,47):- e1247.

abducible e1327.

0.5::edge(13,27):- e1327.

abducible e1342.

0.5::edge(13,42):- e1342.

abducible e1453.

0.5::edge(14,53):- e1453.

abducible e1565.

0.5::edge(15,65):- e1565.

abducible e1634.

0.5::edge(16,34):- e1634.

abducible e1727.

0.5::edge(17,27):- e1727.

abducible e2023.

0.5::edge(20,23):- e2023.

abducible e2122.

0.5::edge(21,22):- e2122.

abducible e2344.

0.5::edge(23,44):- e2344.

abducible e2345.

0.5::edge(23,45):- e2345.

abducible e2367.

0.5::edge(23,67):- e2367.

abducible e2425.

0.5::edge(24,25):- e2425.

abducible e2464.

0.5::edge(24,64):- e2464.

abducible e2837.

0.5::edge(28,37):- e2837.

abducible e2838.

0.5::edge(28,38):- e2838.

abducible e3031.

0.5::edge(30,31):- e3031.

abducible e3160.

0.5::edge(31,60):- e3160.

abducible e3259.

0.5::edge(32,59):- e3259.

abducible e3556.

0.5::edge(35,56):- e3556.

abducible e3641.

0.5::edge(36,41):- e3641.

abducible e3746.

0.5::edge(37,46):- e3746.

abducible e3844.

0.5::edge(38,44):- e3844.

abducible e4050.

0.5::edge(40,50):- e4050.

abducible e4345.

0.5::edge(43,45):- e4345.

abducible e4552.

0.5::edge(45,52):- e4552.

abducible e4568.

0.5::edge(45,68):- e4568.

abducible e4658.

0.5::edge(46,58):- e4658.

abducible e4849.

0.5::edge(48,49):- e4849.

abducible e4855.

0.5::edge(48,55):- e4855.

abducible e4859.

0.5::edge(48,59):- e4859.

abducible e4866.

0.5::edge(48,66):- e4866.

abducible e5051.

0.5::edge(50,51):- e5051.

abducible e5558.

0.5::edge(55,58):- e5558.

abducible e5762.

0.5::edge(57,62):- e5762.



:- path(0, 69 ,L), L < 6.





ev :- path(0,69,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

