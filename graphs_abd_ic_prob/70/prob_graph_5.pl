:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e03.

0.5::edge(0,3):- e03.

abducible e04.

0.5::edge(0,4):- e04.

abducible e06.

0.5::edge(0,6):- e06.

abducible e07.

0.5::edge(0,7):- e07.

abducible e08.

0.5::edge(0,8):- e08.

abducible e014.

0.5::edge(0,14):- e014.

abducible e019.

0.5::edge(0,19):- e019.

abducible e021.

0.5::edge(0,21):- e021.

abducible e023.

0.5::edge(0,23):- e023.

abducible e025.

0.5::edge(0,25):- e025.

abducible e026.

0.5::edge(0,26):- e026.

abducible e027.

0.5::edge(0,27):- e027.

abducible e030.

0.5::edge(0,30):- e030.

abducible e031.

0.5::edge(0,31):- e031.

abducible e055.

0.5::edge(0,55):- e055.

abducible e061.

0.5::edge(0,61):- e061.

abducible e12.

0.5::edge(1,2):- e12.

abducible e131.

0.5::edge(1,31):- e131.

abducible e23.

0.5::edge(2,3):- e23.

abducible e34.

0.5::edge(3,4):- e34.

abducible e35.

0.5::edge(3,5):- e35.

abducible e310.

0.5::edge(3,10):- e310.

abducible e312.

0.5::edge(3,12):- e312.

abducible e314.

0.5::edge(3,14):- e314.

abducible e317.

0.5::edge(3,17):- e317.

abducible e319.

0.5::edge(3,19):- e319.

abducible e327.

0.5::edge(3,27):- e327.

abducible e330.

0.5::edge(3,30):- e330.

abducible e352.

0.5::edge(3,52):- e352.

abducible e361.

0.5::edge(3,61):- e361.

abducible e362.

0.5::edge(3,62):- e362.

abducible e364.

0.5::edge(3,64):- e364.

abducible e365.

0.5::edge(3,65):- e365.

abducible e45.

0.5::edge(4,5):- e45.

abducible e47.

0.5::edge(4,7):- e47.

abducible e48.

0.5::edge(4,8):- e48.

abducible e411.

0.5::edge(4,11):- e411.

abducible e416.

0.5::edge(4,16):- e416.

abducible e422.

0.5::edge(4,22):- e422.

abducible e424.

0.5::edge(4,24):- e424.

abducible e428.

0.5::edge(4,28):- e428.

abducible e440.

0.5::edge(4,40):- e440.

abducible e442.

0.5::edge(4,42):- e442.

abducible e456.

0.5::edge(4,56):- e456.

abducible e465.

0.5::edge(4,65):- e465.

abducible e56.

0.5::edge(5,6):- e56.

abducible e513.

0.5::edge(5,13):- e513.

abducible e517.

0.5::edge(5,17):- e517.

abducible e524.

0.5::edge(5,24):- e524.

abducible e553.

0.5::edge(5,53):- e553.

abducible e69.

0.5::edge(6,9):- e69.

abducible e611.

0.5::edge(6,11):- e611.

abducible e632.

0.5::edge(6,32):- e632.

abducible e644.

0.5::edge(6,44):- e644.

abducible e648.

0.5::edge(6,48):- e648.

abducible e668.

0.5::edge(6,68):- e668.

abducible e746.

0.5::edge(7,46):- e746.

abducible e750.

0.5::edge(7,50):- e750.

abducible e755.

0.5::edge(7,55):- e755.

abducible e762.

0.5::edge(7,62):- e762.

abducible e89.

0.5::edge(8,9):- e89.

abducible e810.

0.5::edge(8,10):- e810.

abducible e813.

0.5::edge(8,13):- e813.

abducible e815.

0.5::edge(8,15):- e815.

abducible e821.

0.5::edge(8,21):- e821.

abducible e822.

0.5::edge(8,22):- e822.

abducible e823.

0.5::edge(8,23):- e823.

abducible e834.

0.5::edge(8,34):- e834.

abducible e841.

0.5::edge(8,41):- e841.

abducible e845.

0.5::edge(8,45):- e845.

abducible e858.

0.5::edge(8,58):- e858.

abducible e863.

0.5::edge(8,63):- e863.

abducible e864.

0.5::edge(8,64):- e864.

abducible e868.

0.5::edge(8,68):- e868.

abducible e938.

0.5::edge(9,38):- e938.

abducible e940.

0.5::edge(9,40):- e940.

abducible e1112.

0.5::edge(11,12):- e1112.

abducible e1125.

0.5::edge(11,25):- e1125.

abducible e1129.

0.5::edge(11,29):- e1129.

abducible e1215.

0.5::edge(12,15):- e1215.

abducible e1220.

0.5::edge(12,20):- e1220.

abducible e1243.

0.5::edge(12,43):- e1243.

abducible e1316.

0.5::edge(13,16):- e1316.

abducible e1328.

0.5::edge(13,28):- e1328.

abducible e1363.

0.5::edge(13,63):- e1363.

abducible e1369.

0.5::edge(13,69):- e1369.

abducible e1433.

0.5::edge(14,33):- e1433.

abducible e1518.

0.5::edge(15,18):- e1518.

abducible e1618.

0.5::edge(16,18):- e1618.

abducible e1635.

0.5::edge(16,35):- e1635.

abducible e1636.

0.5::edge(16,36):- e1636.

abducible e1637.

0.5::edge(16,37):- e1637.

abducible e1647.

0.5::edge(16,47):- e1647.

abducible e1720.

0.5::edge(17,20):- e1720.

abducible e1735.

0.5::edge(17,35):- e1735.

abducible e1826.

0.5::edge(18,26):- e1826.

abducible e1841.

0.5::edge(18,41):- e1841.

abducible e1842.

0.5::edge(18,42):- e1842.

abducible e1845.

0.5::edge(18,45):- e1845.

abducible e1850.

0.5::edge(18,50):- e1850.

abducible e1937.

0.5::edge(19,37):- e1937.

abducible e2036.

0.5::edge(20,36):- e2036.

abducible e2160.

0.5::edge(21,60):- e2160.

abducible e2349.

0.5::edge(23,49):- e2349.

abducible e2429.

0.5::edge(24,29):- e2429.

abducible e2447.

0.5::edge(24,47):- e2447.

abducible e2453.

0.5::edge(24,53):- e2453.

abducible e2457.

0.5::edge(24,57):- e2457.

abducible e2759.

0.5::edge(27,59):- e2759.

abducible e2933.

0.5::edge(29,33):- e2933.

abducible e2960.

0.5::edge(29,60):- e2960.

abducible e3051.

0.5::edge(30,51):- e3051.

abducible e3132.

0.5::edge(31,32):- e3132.

abducible e3138.

0.5::edge(31,38):- e3138.

abducible e3139.

0.5::edge(31,39):- e3139.

abducible e3143.

0.5::edge(31,43):- e3143.

abducible e3234.

0.5::edge(32,34):- e3234.

abducible e3257.

0.5::edge(32,57):- e3257.

abducible e3639.

0.5::edge(36,39):- e3639.

abducible e3654.

0.5::edge(36,54):- e3654.

abducible e3666.

0.5::edge(36,66):- e3666.

abducible e3667.

0.5::edge(36,67):- e3667.

abducible e3744.

0.5::edge(37,44):- e3744.

abducible e3852.

0.5::edge(38,52):- e3852.

abducible e4058.

0.5::edge(40,58):- e4058.

abducible e4146.

0.5::edge(41,46):- e4146.

abducible e4154.

0.5::edge(41,54):- e4154.

abducible e4348.

0.5::edge(43,48):- e4348.

abducible e4551.

0.5::edge(45,51):- e4551.

abducible e4849.

0.5::edge(48,49):- e4849.

abducible e4866.

0.5::edge(48,66):- e4866.

abducible e5256.

0.5::edge(52,56):- e5256.

abducible e5459.

0.5::edge(54,59):- e5459.

abducible e5867.

0.5::edge(58,67):- e5867.

abducible e6669.

0.5::edge(66,69):- e6669.



:- path(0, 69 ,L), L < 6.





ev :- path(0,69,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

