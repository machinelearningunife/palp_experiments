:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e03.

0.5::edge(0,3):- e03.

abducible e05.

0.5::edge(0,5):- e05.

abducible e010.

0.5::edge(0,10):- e010.

abducible e013.

0.5::edge(0,13):- e013.

abducible e014.

0.5::edge(0,14):- e014.

abducible e015.

0.5::edge(0,15):- e015.

abducible e024.

0.5::edge(0,24):- e024.

abducible e032.

0.5::edge(0,32):- e032.

abducible e036.

0.5::edge(0,36):- e036.

abducible e055.

0.5::edge(0,55):- e055.

abducible e12.

0.5::edge(1,2):- e12.

abducible e14.

0.5::edge(1,4):- e14.

abducible e17.

0.5::edge(1,7):- e17.

abducible e18.

0.5::edge(1,8):- e18.

abducible e111.

0.5::edge(1,11):- e111.

abducible e126.

0.5::edge(1,26):- e126.

abducible e142.

0.5::edge(1,42):- e142.

abducible e146.

0.5::edge(1,46):- e146.

abducible e147.

0.5::edge(1,47):- e147.

abducible e148.

0.5::edge(1,48):- e148.

abducible e163.

0.5::edge(1,63):- e163.

abducible e165.

0.5::edge(1,65):- e165.

abducible e166.

0.5::edge(1,66):- e166.

abducible e23.

0.5::edge(2,3):- e23.

abducible e25.

0.5::edge(2,5):- e25.

abducible e28.

0.5::edge(2,8):- e28.

abducible e212.

0.5::edge(2,12):- e212.

abducible e216.

0.5::edge(2,16):- e216.

abducible e221.

0.5::edge(2,21):- e221.

abducible e229.

0.5::edge(2,29):- e229.

abducible e231.

0.5::edge(2,31):- e231.

abducible e234.

0.5::edge(2,34):- e234.

abducible e245.

0.5::edge(2,45):- e245.

abducible e249.

0.5::edge(2,49):- e249.

abducible e251.

0.5::edge(2,51):- e251.

abducible e34.

0.5::edge(3,4):- e34.

abducible e36.

0.5::edge(3,6):- e36.

abducible e39.

0.5::edge(3,9):- e39.

abducible e312.

0.5::edge(3,12):- e312.

abducible e324.

0.5::edge(3,24):- e324.

abducible e333.

0.5::edge(3,33):- e333.

abducible e336.

0.5::edge(3,36):- e336.

abducible e347.

0.5::edge(3,47):- e347.

abducible e46.

0.5::edge(4,6):- e46.

abducible e418.

0.5::edge(4,18):- e418.

abducible e422.

0.5::edge(4,22):- e422.

abducible e444.

0.5::edge(4,44):- e444.

abducible e465.

0.5::edge(4,65):- e465.

abducible e467.

0.5::edge(4,67):- e467.

abducible e67.

0.5::edge(6,7):- e67.

abducible e613.

0.5::edge(6,13):- e613.

abducible e615.

0.5::edge(6,15):- e615.

abducible e617.

0.5::edge(6,17):- e617.

abducible e627.

0.5::edge(6,27):- e627.

abducible e646.

0.5::edge(6,46):- e646.

abducible e659.

0.5::edge(6,59):- e659.

abducible e669.

0.5::edge(6,69):- e669.

abducible e718.

0.5::edge(7,18):- e718.

abducible e723.

0.5::edge(7,23):- e723.

abducible e759.

0.5::edge(7,59):- e759.

abducible e89.

0.5::edge(8,9):- e89.

abducible e811.

0.5::edge(8,11):- e811.

abducible e820.

0.5::edge(8,20):- e820.

abducible e833.

0.5::edge(8,33):- e833.

abducible e837.

0.5::edge(8,37):- e837.

abducible e840.

0.5::edge(8,40):- e840.

abducible e853.

0.5::edge(8,53):- e853.

abducible e857.

0.5::edge(8,57):- e857.

abducible e910.

0.5::edge(9,10):- e910.

abducible e914.

0.5::edge(9,14):- e914.

abducible e964.

0.5::edge(9,64):- e964.

abducible e969.

0.5::edge(9,69):- e969.

abducible e1226.

0.5::edge(12,26):- e1226.

abducible e1237.

0.5::edge(12,37):- e1237.

abducible e1316.

0.5::edge(13,16):- e1316.

abducible e1327.

0.5::edge(13,27):- e1327.

abducible e1329.

0.5::edge(13,29):- e1329.

abducible e1331.

0.5::edge(13,31):- e1331.

abducible e1342.

0.5::edge(13,42):- e1342.

abducible e1363.

0.5::edge(13,63):- e1363.

abducible e1419.

0.5::edge(14,19):- e1419.

abducible e1425.

0.5::edge(14,25):- e1425.

abducible e1456.

0.5::edge(14,56):- e1456.

abducible e1517.

0.5::edge(15,17):- e1517.

abducible e1519.

0.5::edge(15,19):- e1519.

abducible e1534.

0.5::edge(15,34):- e1534.

abducible e1538.

0.5::edge(15,38):- e1538.

abducible e1557.

0.5::edge(15,57):- e1557.

abducible e1558.

0.5::edge(15,58):- e1558.

abducible e1620.

0.5::edge(16,20):- e1620.

abducible e1722.

0.5::edge(17,22):- e1722.

abducible e1739.

0.5::edge(17,39):- e1739.

abducible e1749.

0.5::edge(17,49):- e1749.

abducible e1752.

0.5::edge(17,52):- e1752.

abducible e1764.

0.5::edge(17,64):- e1764.

abducible e1821.

0.5::edge(18,21):- e1821.

abducible e1823.

0.5::edge(18,23):- e1823.

abducible e1835.

0.5::edge(18,35):- e1835.

abducible e1845.

0.5::edge(18,45):- e1845.

abducible e1854.

0.5::edge(18,54):- e1854.

abducible e2040.

0.5::edge(20,40):- e2040.

abducible e2051.

0.5::edge(20,51):- e2051.

abducible e2125.

0.5::edge(21,25):- e2125.

abducible e2128.

0.5::edge(21,28):- e2128.

abducible e2252.

0.5::edge(22,52):- e2252.

abducible e2432.

0.5::edge(24,32):- e2432.

abducible e2450.

0.5::edge(24,50):- e2450.

abducible e2456.

0.5::edge(24,56):- e2456.

abducible e2461.

0.5::edge(24,61):- e2461.

abducible e2528.

0.5::edge(25,28):- e2528.

abducible e2535.

0.5::edge(25,35):- e2535.

abducible e2539.

0.5::edge(25,39):- e2539.

abducible e2630.

0.5::edge(26,30):- e2630.

abducible e2655.

0.5::edge(26,55):- e2655.

abducible e2660.

0.5::edge(26,60):- e2660.

abducible e2730.

0.5::edge(27,30):- e2730.

abducible e2748.

0.5::edge(27,48):- e2748.

abducible e2941.

0.5::edge(29,41):- e2941.

abducible e2961.

0.5::edge(29,61):- e2961.

abducible e2962.

0.5::edge(29,62):- e2962.

abducible e2968.

0.5::edge(29,68):- e2968.

abducible e3053.

0.5::edge(30,53):- e3053.

abducible e3062.

0.5::edge(30,62):- e3062.

abducible e3141.

0.5::edge(31,41):- e3141.

abducible e3250.

0.5::edge(32,50):- e3250.

abducible e3343.

0.5::edge(33,43):- e3343.

abducible e3438.

0.5::edge(34,38):- e3438.

abducible e3460.

0.5::edge(34,60):- e3460.

abducible e3658.

0.5::edge(36,58):- e3658.

abducible e4043.

0.5::edge(40,43):- e4043.

abducible e4044.

0.5::edge(40,44):- e4044.

abducible e4266.

0.5::edge(42,66):- e4266.

abducible e4567.

0.5::edge(45,67):- e4567.

abducible e5254.

0.5::edge(52,54):- e5254.

abducible e6668.

0.5::edge(66,68):- e6668.



:- path(0, 69 ,L), L < 6.





ev :- path(0,69,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

