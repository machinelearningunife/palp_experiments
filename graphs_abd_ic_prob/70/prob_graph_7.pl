:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e07.

0.5::edge(0,7):- e07.

abducible e018.

0.5::edge(0,18):- e018.

abducible e030.

0.5::edge(0,30):- e030.

abducible e033.

0.5::edge(0,33):- e033.

abducible e037.

0.5::edge(0,37):- e037.

abducible e066.

0.5::edge(0,66):- e066.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e15.

0.5::edge(1,5):- e15.

abducible e19.

0.5::edge(1,9):- e19.

abducible e111.

0.5::edge(1,11):- e111.

abducible e112.

0.5::edge(1,12):- e112.

abducible e117.

0.5::edge(1,17):- e117.

abducible e119.

0.5::edge(1,19):- e119.

abducible e120.

0.5::edge(1,20):- e120.

abducible e122.

0.5::edge(1,22):- e122.

abducible e128.

0.5::edge(1,28):- e128.

abducible e129.

0.5::edge(1,29):- e129.

abducible e154.

0.5::edge(1,54):- e154.

abducible e169.

0.5::edge(1,69):- e169.

abducible e23.

0.5::edge(2,3):- e23.

abducible e25.

0.5::edge(2,5):- e25.

abducible e26.

0.5::edge(2,6):- e26.

abducible e28.

0.5::edge(2,8):- e28.

abducible e210.

0.5::edge(2,10):- e210.

abducible e211.

0.5::edge(2,11):- e211.

abducible e214.

0.5::edge(2,14):- e214.

abducible e216.

0.5::edge(2,16):- e216.

abducible e223.

0.5::edge(2,23):- e223.

abducible e226.

0.5::edge(2,26):- e226.

abducible e231.

0.5::edge(2,31):- e231.

abducible e232.

0.5::edge(2,32):- e232.

abducible e233.

0.5::edge(2,33):- e233.

abducible e240.

0.5::edge(2,40):- e240.

abducible e248.

0.5::edge(2,48):- e248.

abducible e251.

0.5::edge(2,51):- e251.

abducible e252.

0.5::edge(2,52):- e252.

abducible e253.

0.5::edge(2,53):- e253.

abducible e266.

0.5::edge(2,66):- e266.

abducible e34.

0.5::edge(3,4):- e34.

abducible e39.

0.5::edge(3,9):- e39.

abducible e319.

0.5::edge(3,19):- e319.

abducible e339.

0.5::edge(3,39):- e339.

abducible e341.

0.5::edge(3,41):- e341.

abducible e351.

0.5::edge(3,51):- e351.

abducible e363.

0.5::edge(3,63):- e363.

abducible e56.

0.5::edge(5,6):- e56.

abducible e57.

0.5::edge(5,7):- e57.

abducible e510.

0.5::edge(5,10):- e510.

abducible e512.

0.5::edge(5,12):- e512.

abducible e513.

0.5::edge(5,13):- e513.

abducible e514.

0.5::edge(5,14):- e514.

abducible e515.

0.5::edge(5,15):- e515.

abducible e517.

0.5::edge(5,17):- e517.

abducible e518.

0.5::edge(5,18):- e518.

abducible e521.

0.5::edge(5,21):- e521.

abducible e527.

0.5::edge(5,27):- e527.

abducible e529.

0.5::edge(5,29):- e529.

abducible e534.

0.5::edge(5,34):- e534.

abducible e538.

0.5::edge(5,38):- e538.

abducible e550.

0.5::edge(5,50):- e550.

abducible e555.

0.5::edge(5,55):- e555.

abducible e565.

0.5::edge(5,65):- e565.

abducible e68.

0.5::edge(6,8):- e68.

abducible e625.

0.5::edge(6,25):- e625.

abducible e641.

0.5::edge(6,41):- e641.

abducible e661.

0.5::edge(6,61):- e661.

abducible e725.

0.5::edge(7,25):- e725.

abducible e730.

0.5::edge(7,30):- e730.

abducible e922.

0.5::edge(9,22):- e922.

abducible e934.

0.5::edge(9,34):- e934.

abducible e935.

0.5::edge(9,35):- e935.

abducible e967.

0.5::edge(9,67):- e967.

abducible e1113.

0.5::edge(11,13):- e1113.

abducible e1120.

0.5::edge(11,20):- e1120.

abducible e1124.

0.5::edge(11,24):- e1124.

abducible e1145.

0.5::edge(11,45):- e1145.

abducible e1147.

0.5::edge(11,47):- e1147.

abducible e1164.

0.5::edge(11,64):- e1164.

abducible e1215.

0.5::edge(12,15):- e1215.

abducible e1216.

0.5::edge(12,16):- e1216.

abducible e1242.

0.5::edge(12,42):- e1242.

abducible e1340.

0.5::edge(13,40):- e1340.

abducible e1354.

0.5::edge(13,54):- e1354.

abducible e1357.

0.5::edge(13,57):- e1357.

abducible e1358.

0.5::edge(13,58):- e1358.

abducible e1361.

0.5::edge(13,61):- e1361.

abducible e1368.

0.5::edge(13,68):- e1368.

abducible e1421.

0.5::edge(14,21):- e1421.

abducible e1427.

0.5::edge(14,27):- e1427.

abducible e1528.

0.5::edge(15,28):- e1528.

abducible e1539.

0.5::edge(15,39):- e1539.

abducible e1636.

0.5::edge(16,36):- e1636.

abducible e1648.

0.5::edge(16,48):- e1648.

abducible e1731.

0.5::edge(17,31):- e1731.

abducible e1746.

0.5::edge(17,46):- e1746.

abducible e1823.

0.5::edge(18,23):- e1823.

abducible e1935.

0.5::edge(19,35):- e1935.

abducible e1937.

0.5::edge(19,37):- e1937.

abducible e1938.

0.5::edge(19,38):- e1938.

abducible e1945.

0.5::edge(19,45):- e1945.

abducible e1949.

0.5::edge(19,49):- e1949.

abducible e1952.

0.5::edge(19,52):- e1952.

abducible e1962.

0.5::edge(19,62):- e1962.

abducible e2024.

0.5::edge(20,24):- e2024.

abducible e2056.

0.5::edge(20,56):- e2056.

abducible e2158.

0.5::edge(21,58):- e2158.

abducible e2364.

0.5::edge(23,64):- e2364.

abducible e2426.

0.5::edge(24,26):- e2426.

abducible e2632.

0.5::edge(26,32):- e2632.

abducible e2663.

0.5::edge(26,63):- e2663.

abducible e2843.

0.5::edge(28,43):- e2843.

abducible e2844.

0.5::edge(28,44):- e2844.

abducible e2849.

0.5::edge(28,49):- e2849.

abducible e2953.

0.5::edge(29,53):- e2953.

abducible e3036.

0.5::edge(30,36):- e3036.

abducible e3044.

0.5::edge(30,44):- e3044.

abducible e3362.

0.5::edge(33,62):- e3362.

abducible e3759.

0.5::edge(37,59):- e3759.

abducible e3847.

0.5::edge(38,47):- e3847.

abducible e3855.

0.5::edge(38,55):- e3855.

abducible e3857.

0.5::edge(38,57):- e3857.

abducible e3859.

0.5::edge(38,59):- e3859.

abducible e3865.

0.5::edge(38,65):- e3865.

abducible e3942.

0.5::edge(39,42):- e3942.

abducible e3960.

0.5::edge(39,60):- e3960.

abducible e4043.

0.5::edge(40,43):- e4043.

abducible e4146.

0.5::edge(41,46):- e4146.

abducible e4850.

0.5::edge(48,50):- e4850.

abducible e4868.

0.5::edge(48,68):- e4868.

abducible e5056.

0.5::edge(50,56):- e5056.

abducible e5160.

0.5::edge(51,60):- e5160.

abducible e6067.

0.5::edge(60,67):- e6067.

abducible e6569.

0.5::edge(65,69):- e6569.



:- path(0, 69 ,L), L < 6.





ev :- path(0,69,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

