:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e03.

0.5::edge(0,3):- e03.

abducible e07.

0.5::edge(0,7):- e07.

abducible e08.

0.5::edge(0,8):- e08.

abducible e010.

0.5::edge(0,10):- e010.

abducible e013.

0.5::edge(0,13):- e013.

abducible e014.

0.5::edge(0,14):- e014.

abducible e017.

0.5::edge(0,17):- e017.

abducible e041.

0.5::edge(0,41):- e041.

abducible e053.

0.5::edge(0,53):- e053.

abducible e055.

0.5::edge(0,55):- e055.

abducible e057.

0.5::edge(0,57):- e057.

abducible e062.

0.5::edge(0,62):- e062.

abducible e064.

0.5::edge(0,64):- e064.

abducible e068.

0.5::edge(0,68):- e068.

abducible e12.

0.5::edge(1,2):- e12.

abducible e15.

0.5::edge(1,5):- e15.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e210.

0.5::edge(2,10):- e210.

abducible e234.

0.5::edge(2,34):- e234.

abducible e236.

0.5::edge(2,36):- e236.

abducible e237.

0.5::edge(2,37):- e237.

abducible e240.

0.5::edge(2,40):- e240.

abducible e255.

0.5::edge(2,55):- e255.

abducible e256.

0.5::edge(2,56):- e256.

abducible e262.

0.5::edge(2,62):- e262.

abducible e34.

0.5::edge(3,4):- e34.

abducible e36.

0.5::edge(3,6):- e36.

abducible e37.

0.5::edge(3,7):- e37.

abducible e39.

0.5::edge(3,9):- e39.

abducible e313.

0.5::edge(3,13):- e313.

abducible e321.

0.5::edge(3,21):- e321.

abducible e332.

0.5::edge(3,32):- e332.

abducible e339.

0.5::edge(3,39):- e339.

abducible e346.

0.5::edge(3,46):- e346.

abducible e349.

0.5::edge(3,49):- e349.

abducible e350.

0.5::edge(3,50):- e350.

abducible e352.

0.5::edge(3,52):- e352.

abducible e361.

0.5::edge(3,61):- e361.

abducible e45.

0.5::edge(4,5):- e45.

abducible e412.

0.5::edge(4,12):- e412.

abducible e416.

0.5::edge(4,16):- e416.

abducible e423.

0.5::edge(4,23):- e423.

abducible e433.

0.5::edge(4,33):- e433.

abducible e440.

0.5::edge(4,40):- e440.

abducible e445.

0.5::edge(4,45):- e445.

abducible e447.

0.5::edge(4,47):- e447.

abducible e461.

0.5::edge(4,61):- e461.

abducible e56.

0.5::edge(5,6):- e56.

abducible e59.

0.5::edge(5,9):- e59.

abducible e514.

0.5::edge(5,14):- e514.

abducible e515.

0.5::edge(5,15):- e515.

abducible e518.

0.5::edge(5,18):- e518.

abducible e519.

0.5::edge(5,19):- e519.

abducible e523.

0.5::edge(5,23):- e523.

abducible e525.

0.5::edge(5,25):- e525.

abducible e527.

0.5::edge(5,27):- e527.

abducible e528.

0.5::edge(5,28):- e528.

abducible e533.

0.5::edge(5,33):- e533.

abducible e541.

0.5::edge(5,41):- e541.

abducible e542.

0.5::edge(5,42):- e542.

abducible e612.

0.5::edge(6,12):- e612.

abducible e620.

0.5::edge(6,20):- e620.

abducible e622.

0.5::edge(6,22):- e622.

abducible e78.

0.5::edge(7,8):- e78.

abducible e719.

0.5::edge(7,19):- e719.

abducible e731.

0.5::edge(7,31):- e731.

abducible e827.

0.5::edge(8,27):- e827.

abducible e911.

0.5::edge(9,11):- e911.

abducible e915.

0.5::edge(9,15):- e915.

abducible e918.

0.5::edge(9,18):- e918.

abducible e924.

0.5::edge(9,24):- e924.

abducible e929.

0.5::edge(9,29):- e929.

abducible e946.

0.5::edge(9,46):- e946.

abducible e956.

0.5::edge(9,56):- e956.

abducible e960.

0.5::edge(9,60):- e960.

abducible e967.

0.5::edge(9,67):- e967.

abducible e1011.

0.5::edge(10,11):- e1011.

abducible e1017.

0.5::edge(10,17):- e1017.

abducible e1044.

0.5::edge(10,44):- e1044.

abducible e1045.

0.5::edge(10,45):- e1045.

abducible e1143.

0.5::edge(11,43):- e1143.

abducible e1163.

0.5::edge(11,63):- e1163.

abducible e1222.

0.5::edge(12,22):- e1222.

abducible e1248.

0.5::edge(12,48):- e1248.

abducible e1269.

0.5::edge(12,69):- e1269.

abducible e1321.

0.5::edge(13,21):- e1321.

abducible e1335.

0.5::edge(13,35):- e1335.

abducible e1349.

0.5::edge(13,49):- e1349.

abducible e1354.

0.5::edge(13,54):- e1354.

abducible e1424.

0.5::edge(14,24):- e1424.

abducible e1426.

0.5::edge(14,26):- e1426.

abducible e1429.

0.5::edge(14,29):- e1429.

abducible e1436.

0.5::edge(14,36):- e1436.

abducible e1444.

0.5::edge(14,44):- e1444.

abducible e1447.

0.5::edge(14,47):- e1447.

abducible e1458.

0.5::edge(14,58):- e1458.

abducible e1516.

0.5::edge(15,16):- e1516.

abducible e1520.

0.5::edge(15,20):- e1520.

abducible e1525.

0.5::edge(15,25):- e1525.

abducible e1537.

0.5::edge(15,37):- e1537.

abducible e1552.

0.5::edge(15,52):- e1552.

abducible e1564.

0.5::edge(15,64):- e1564.

abducible e1826.

0.5::edge(18,26):- e1826.

abducible e1830.

0.5::edge(18,30):- e1830.

abducible e1831.

0.5::edge(18,31):- e1831.

abducible e1930.

0.5::edge(19,30):- e1930.

abducible e1951.

0.5::edge(19,51):- e1951.

abducible e1954.

0.5::edge(19,54):- e1954.

abducible e1957.

0.5::edge(19,57):- e1957.

abducible e1960.

0.5::edge(19,60):- e1960.

abducible e2028.

0.5::edge(20,28):- e2028.

abducible e2038.

0.5::edge(20,38):- e2038.

abducible e2135.

0.5::edge(21,35):- e2135.

abducible e2232.

0.5::edge(22,32):- e2232.

abducible e2534.

0.5::edge(25,34):- e2534.

abducible e2742.

0.5::edge(27,42):- e2742.

abducible e2759.

0.5::edge(27,59):- e2759.

abducible e2765.

0.5::edge(27,65):- e2765.

abducible e2938.

0.5::edge(29,38):- e2938.

abducible e2943.

0.5::edge(29,43):- e2943.

abducible e3039.

0.5::edge(30,39):- e3039.

abducible e3353.

0.5::edge(33,53):- e3353.

abducible e3451.

0.5::edge(34,51):- e3451.

abducible e3948.

0.5::edge(39,48):- e3948.

abducible e4058.

0.5::edge(40,58):- e4058.

abducible e4550.

0.5::edge(45,50):- e4550.

abducible e4768.

0.5::edge(47,68):- e4768.

abducible e4866.

0.5::edge(48,66):- e4866.

abducible e4965.

0.5::edge(49,65):- e4965.

abducible e5067.

0.5::edge(50,67):- e5067.

abducible e5069.

0.5::edge(50,69):- e5069.

abducible e5163.

0.5::edge(51,63):- e5163.

abducible e5359.

0.5::edge(53,59):- e5359.

abducible e5466.

0.5::edge(54,66):- e5466.



:- path(0, 69 ,L), L < 6.





ev :- path(0,69,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

