:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e04.

0.5::edge(0,4):- e04.

abducible e07.

0.5::edge(0,7):- e07.

abducible e028.

0.5::edge(0,28):- e028.

abducible e032.

0.5::edge(0,32):- e032.

abducible e039.

0.5::edge(0,39):- e039.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e23.

0.5::edge(2,3):- e23.

abducible e25.

0.5::edge(2,5):- e25.

abducible e26.

0.5::edge(2,6):- e26.

abducible e28.

0.5::edge(2,8):- e28.

abducible e29.

0.5::edge(2,9):- e29.

abducible e210.

0.5::edge(2,10):- e210.

abducible e211.

0.5::edge(2,11):- e211.

abducible e212.

0.5::edge(2,12):- e212.

abducible e213.

0.5::edge(2,13):- e213.

abducible e216.

0.5::edge(2,16):- e216.

abducible e218.

0.5::edge(2,18):- e218.

abducible e225.

0.5::edge(2,25):- e225.

abducible e226.

0.5::edge(2,26):- e226.

abducible e227.

0.5::edge(2,27):- e227.

abducible e229.

0.5::edge(2,29):- e229.

abducible e230.

0.5::edge(2,30):- e230.

abducible e231.

0.5::edge(2,31):- e231.

abducible e236.

0.5::edge(2,36):- e236.

abducible e240.

0.5::edge(2,40):- e240.

abducible e247.

0.5::edge(2,47):- e247.

abducible e34.

0.5::edge(3,4):- e34.

abducible e35.

0.5::edge(3,5):- e35.

abducible e37.

0.5::edge(3,7):- e37.

abducible e38.

0.5::edge(3,8):- e38.

abducible e310.

0.5::edge(3,10):- e310.

abducible e317.

0.5::edge(3,17):- e317.

abducible e324.

0.5::edge(3,24):- e324.

abducible e329.

0.5::edge(3,29):- e329.

abducible e331.

0.5::edge(3,31):- e331.

abducible e333.

0.5::edge(3,33):- e333.

abducible e344.

0.5::edge(3,44):- e344.

abducible e46.

0.5::edge(4,6):- e46.

abducible e434.

0.5::edge(4,34):- e434.

abducible e69.

0.5::edge(6,9):- e69.

abducible e611.

0.5::edge(6,11):- e611.

abducible e619.

0.5::edge(6,19):- e619.

abducible e622.

0.5::edge(6,22):- e622.

abducible e713.

0.5::edge(7,13):- e713.

abducible e714.

0.5::edge(7,14):- e714.

abducible e715.

0.5::edge(7,15):- e715.

abducible e717.

0.5::edge(7,17):- e717.

abducible e723.

0.5::edge(7,23):- e723.

abducible e724.

0.5::edge(7,24):- e724.

abducible e730.

0.5::edge(7,30):- e730.

abducible e737.

0.5::edge(7,37):- e737.

abducible e745.

0.5::edge(7,45):- e745.

abducible e747.

0.5::edge(7,47):- e747.

abducible e822.

0.5::edge(8,22):- e822.

abducible e842.

0.5::edge(8,42):- e842.

abducible e912.

0.5::edge(9,12):- e912.

abducible e921.

0.5::edge(9,21):- e921.

abducible e928.

0.5::edge(9,28):- e928.

abducible e944.

0.5::edge(9,44):- e944.

abducible e945.

0.5::edge(9,45):- e945.

abducible e1020.

0.5::edge(10,20):- e1020.

abducible e1032.

0.5::edge(10,32):- e1032.

abducible e1114.

0.5::edge(11,14):- e1114.

abducible e1120.

0.5::edge(11,20):- e1120.

abducible e1121.

0.5::edge(11,21):- e1121.

abducible e1123.

0.5::edge(11,23):- e1123.

abducible e1140.

0.5::edge(11,40):- e1140.

abducible e1142.

0.5::edge(11,42):- e1142.

abducible e1143.

0.5::edge(11,43):- e1143.

abducible e1316.

0.5::edge(13,16):- e1316.

abducible e1415.

0.5::edge(14,15):- e1415.

abducible e1434.

0.5::edge(14,34):- e1434.

abducible e1535.

0.5::edge(15,35):- e1535.

abducible e1619.

0.5::edge(16,19):- e1619.

abducible e1625.

0.5::edge(16,25):- e1625.

abducible e1641.

0.5::edge(16,41):- e1641.

abducible e1643.

0.5::edge(16,43):- e1643.

abducible e1649.

0.5::edge(16,49):- e1649.

abducible e1718.

0.5::edge(17,18):- e1718.

abducible e1827.

0.5::edge(18,27):- e1827.

abducible e1841.

0.5::edge(18,41):- e1841.

abducible e1938.

0.5::edge(19,38):- e1938.

abducible e2048.

0.5::edge(20,48):- e2048.

abducible e2326.

0.5::edge(23,26):- e2326.

abducible e2433.

0.5::edge(24,33):- e2433.

abducible e2435.

0.5::edge(24,35):- e2435.

abducible e2437.

0.5::edge(24,37):- e2437.

abducible e2836.

0.5::edge(28,36):- e2836.

abducible e2846.

0.5::edge(28,46):- e2846.

abducible e3139.

0.5::edge(31,39):- e3139.

abducible e3148.

0.5::edge(31,48):- e3148.

abducible e3338.

0.5::edge(33,38):- e3338.

abducible e3946.

0.5::edge(39,46):- e3946.

abducible e4449.

0.5::edge(44,49):- e4449.



0.5:- path(0, 49 ,L), L < 6.




ev :- path(0,49,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

