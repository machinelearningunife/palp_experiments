:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e034.

0.5::edge(0,34):- e034.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e15.

0.5::edge(1,5):- e15.

abducible e17.

0.5::edge(1,7):- e17.

abducible e113.

0.5::edge(1,13):- e113.

abducible e114.

0.5::edge(1,14):- e114.

abducible e118.

0.5::edge(1,18):- e118.

abducible e147.

0.5::edge(1,47):- e147.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e25.

0.5::edge(2,5):- e25.

abducible e26.

0.5::edge(2,6):- e26.

abducible e27.

0.5::edge(2,7):- e27.

abducible e28.

0.5::edge(2,8):- e28.

abducible e29.

0.5::edge(2,9):- e29.

abducible e219.

0.5::edge(2,19):- e219.

abducible e220.

0.5::edge(2,20):- e220.

abducible e232.

0.5::edge(2,32):- e232.

abducible e235.

0.5::edge(2,35):- e235.

abducible e237.

0.5::edge(2,37):- e237.

abducible e239.

0.5::edge(2,39):- e239.

abducible e36.

0.5::edge(3,6):- e36.

abducible e38.

0.5::edge(3,8):- e38.

abducible e39.

0.5::edge(3,9):- e39.

abducible e310.

0.5::edge(3,10):- e310.

abducible e314.

0.5::edge(3,14):- e314.

abducible e315.

0.5::edge(3,15):- e315.

abducible e316.

0.5::edge(3,16):- e316.

abducible e323.

0.5::edge(3,23):- e323.

abducible e324.

0.5::edge(3,24):- e324.

abducible e329.

0.5::edge(3,29):- e329.

abducible e333.

0.5::edge(3,33):- e333.

abducible e335.

0.5::edge(3,35):- e335.

abducible e341.

0.5::edge(3,41):- e341.

abducible e343.

0.5::edge(3,43):- e343.

abducible e348.

0.5::edge(3,48):- e348.

abducible e410.

0.5::edge(4,10):- e410.

abducible e412.

0.5::edge(4,12):- e412.

abducible e416.

0.5::edge(4,16):- e416.

abducible e427.

0.5::edge(4,27):- e427.

abducible e441.

0.5::edge(4,41):- e441.

abducible e444.

0.5::edge(4,44):- e444.

abducible e511.

0.5::edge(5,11):- e511.

abducible e512.

0.5::edge(5,12):- e512.

abducible e513.

0.5::edge(5,13):- e513.

abducible e521.

0.5::edge(5,21):- e521.

abducible e527.

0.5::edge(5,27):- e527.

abducible e528.

0.5::edge(5,28):- e528.

abducible e536.

0.5::edge(5,36):- e536.

abducible e545.

0.5::edge(5,45):- e545.

abducible e811.

0.5::edge(8,11):- e811.

abducible e817.

0.5::edge(8,17):- e817.

abducible e825.

0.5::edge(8,25):- e825.

abducible e842.

0.5::edge(8,42):- e842.

abducible e847.

0.5::edge(8,47):- e847.

abducible e945.

0.5::edge(9,45):- e945.

abducible e1049.

0.5::edge(10,49):- e1049.

abducible e1115.

0.5::edge(11,15):- e1115.

abducible e1124.

0.5::edge(11,24):- e1124.

abducible e1126.

0.5::edge(11,26):- e1126.

abducible e1134.

0.5::edge(11,34):- e1134.

abducible e1221.

0.5::edge(12,21):- e1221.

abducible e1340.

0.5::edge(13,40):- e1340.

abducible e1419.

0.5::edge(14,19):- e1419.

abducible e1420.

0.5::edge(14,20):- e1420.

abducible e1422.

0.5::edge(14,22):- e1422.

abducible e1425.

0.5::edge(14,25):- e1425.

abducible e1438.

0.5::edge(14,38):- e1438.

abducible e1517.

0.5::edge(15,17):- e1517.

abducible e1523.

0.5::edge(15,23):- e1523.

abducible e1528.

0.5::edge(15,28):- e1528.

abducible e1531.

0.5::edge(15,31):- e1531.

abducible e1626.

0.5::edge(16,26):- e1626.

abducible e1636.

0.5::edge(16,36):- e1636.

abducible e1637.

0.5::edge(16,37):- e1637.

abducible e1718.

0.5::edge(17,18):- e1718.

abducible e1830.

0.5::edge(18,30):- e1830.

abducible e1846.

0.5::edge(18,46):- e1846.

abducible e1932.

0.5::edge(19,32):- e1932.

abducible e2022.

0.5::edge(20,22):- e2022.

abducible e2044.

0.5::edge(20,44):- e2044.

abducible e2243.

0.5::edge(22,43):- e2243.

abducible e2329.

0.5::edge(23,29):- e2329.

abducible e2531.

0.5::edge(25,31):- e2531.

abducible e2542.

0.5::edge(25,42):- e2542.

abducible e2639.

0.5::edge(26,39):- e2639.

abducible e2733.

0.5::edge(27,33):- e2733.

abducible e2930.

0.5::edge(29,30):- e2930.

abducible e3138.

0.5::edge(31,38):- e3138.

abducible e3149.

0.5::edge(31,49):- e3149.

abducible e3440.

0.5::edge(34,40):- e3440.

abducible e4046.

0.5::edge(40,46):- e4046.

abducible e4548.

0.5::edge(45,48):- e4548.



0.5:- path(0, 49 ,L), L < 6.




ev :- path(0,49,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

