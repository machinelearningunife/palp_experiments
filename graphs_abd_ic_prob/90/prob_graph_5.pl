:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e010.

0.5::edge(0,10):- e010.

abducible e023.

0.5::edge(0,23):- e023.

abducible e036.

0.5::edge(0,36):- e036.

abducible e040.

0.5::edge(0,40):- e040.

abducible e068.

0.5::edge(0,68):- e068.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e15.

0.5::edge(1,5):- e15.

abducible e117.

0.5::edge(1,17):- e117.

abducible e129.

0.5::edge(1,29):- e129.

abducible e152.

0.5::edge(1,52):- e152.

abducible e157.

0.5::edge(1,57):- e157.

abducible e163.

0.5::edge(1,63):- e163.

abducible e174.

0.5::edge(1,74):- e174.

abducible e175.

0.5::edge(1,75):- e175.

abducible e177.

0.5::edge(1,77):- e177.

abducible e181.

0.5::edge(1,81):- e181.

abducible e182.

0.5::edge(1,82):- e182.

abducible e189.

0.5::edge(1,89):- e189.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e27.

0.5::edge(2,7):- e27.

abducible e29.

0.5::edge(2,9):- e29.

abducible e212.

0.5::edge(2,12):- e212.

abducible e213.

0.5::edge(2,13):- e213.

abducible e219.

0.5::edge(2,19):- e219.

abducible e221.

0.5::edge(2,21):- e221.

abducible e224.

0.5::edge(2,24):- e224.

abducible e226.

0.5::edge(2,26):- e226.

abducible e234.

0.5::edge(2,34):- e234.

abducible e242.

0.5::edge(2,42):- e242.

abducible e252.

0.5::edge(2,52):- e252.

abducible e279.

0.5::edge(2,79):- e279.

abducible e287.

0.5::edge(2,87):- e287.

abducible e36.

0.5::edge(3,6):- e36.

abducible e38.

0.5::edge(3,8):- e38.

abducible e314.

0.5::edge(3,14):- e314.

abducible e315.

0.5::edge(3,15):- e315.

abducible e322.

0.5::edge(3,22):- e322.

abducible e364.

0.5::edge(3,64):- e364.

abducible e385.

0.5::edge(3,85):- e385.

abducible e386.

0.5::edge(3,86):- e386.

abducible e45.

0.5::edge(4,5):- e45.

abducible e46.

0.5::edge(4,6):- e46.

abducible e47.

0.5::edge(4,7):- e47.

abducible e412.

0.5::edge(4,12):- e412.

abducible e414.

0.5::edge(4,14):- e414.

abducible e425.

0.5::edge(4,25):- e425.

abducible e433.

0.5::edge(4,33):- e433.

abducible e437.

0.5::edge(4,37):- e437.

abducible e443.

0.5::edge(4,43):- e443.

abducible e445.

0.5::edge(4,45):- e445.

abducible e450.

0.5::edge(4,50):- e450.

abducible e453.

0.5::edge(4,53):- e453.

abducible e475.

0.5::edge(4,75):- e475.

abducible e58.

0.5::edge(5,8):- e58.

abducible e511.

0.5::edge(5,11):- e511.

abducible e518.

0.5::edge(5,18):- e518.

abducible e522.

0.5::edge(5,22):- e522.

abducible e530.

0.5::edge(5,30):- e530.

abducible e544.

0.5::edge(5,44):- e544.

abducible e555.

0.5::edge(5,55):- e555.

abducible e558.

0.5::edge(5,58):- e558.

abducible e562.

0.5::edge(5,62):- e562.

abducible e566.

0.5::edge(5,66):- e566.

abducible e586.

0.5::edge(5,86):- e586.

abducible e617.

0.5::edge(6,17):- e617.

abducible e627.

0.5::edge(6,27):- e627.

abducible e634.

0.5::edge(6,34):- e634.

abducible e658.

0.5::edge(6,58):- e658.

abducible e710.

0.5::edge(7,10):- e710.

abducible e711.

0.5::edge(7,11):- e711.

abducible e713.

0.5::edge(7,13):- e713.

abducible e719.

0.5::edge(7,19):- e719.

abducible e723.

0.5::edge(7,23):- e723.

abducible e728.

0.5::edge(7,28):- e728.

abducible e731.

0.5::edge(7,31):- e731.

abducible e740.

0.5::edge(7,40):- e740.

abducible e741.

0.5::edge(7,41):- e741.

abducible e747.

0.5::edge(7,47):- e747.

abducible e754.

0.5::edge(7,54):- e754.

abducible e757.

0.5::edge(7,57):- e757.

abducible e759.

0.5::edge(7,59):- e759.

abducible e767.

0.5::edge(7,67):- e767.

abducible e776.

0.5::edge(7,76):- e776.

abducible e89.

0.5::edge(8,9):- e89.

abducible e825.

0.5::edge(8,25):- e825.

abducible e846.

0.5::edge(8,46):- e846.

abducible e849.

0.5::edge(8,49):- e849.

abducible e888.

0.5::edge(8,88):- e888.

abducible e916.

0.5::edge(9,16):- e916.

abducible e924.

0.5::edge(9,24):- e924.

abducible e949.

0.5::edge(9,49):- e949.

abducible e1015.

0.5::edge(10,15):- e1015.

abducible e1018.

0.5::edge(10,18):- e1018.

abducible e1020.

0.5::edge(10,20):- e1020.

abducible e1035.

0.5::edge(10,35):- e1035.

abducible e1036.

0.5::edge(10,36):- e1036.

abducible e1051.

0.5::edge(10,51):- e1051.

abducible e1133.

0.5::edge(11,33):- e1133.

abducible e1141.

0.5::edge(11,41):- e1141.

abducible e1167.

0.5::edge(11,67):- e1167.

abducible e1316.

0.5::edge(13,16):- e1316.

abducible e1389.

0.5::edge(13,89):- e1389.

abducible e1520.

0.5::edge(15,20):- e1520.

abducible e1535.

0.5::edge(15,35):- e1535.

abducible e1561.

0.5::edge(15,61):- e1561.

abducible e1628.

0.5::edge(16,28):- e1628.

abducible e1629.

0.5::edge(16,29):- e1629.

abducible e1659.

0.5::edge(16,59):- e1659.

abducible e1821.

0.5::edge(18,21):- e1821.

abducible e1837.

0.5::edge(18,37):- e1837.

abducible e1863.

0.5::edge(18,63):- e1863.

abducible e1866.

0.5::edge(18,66):- e1866.

abducible e1927.

0.5::edge(19,27):- e1927.

abducible e1931.

0.5::edge(19,31):- e1931.

abducible e1932.

0.5::edge(19,32):- e1932.

abducible e1948.

0.5::edge(19,48):- e1948.

abducible e1950.

0.5::edge(19,50):- e1950.

abducible e2080.

0.5::edge(20,80):- e2080.

abducible e2126.

0.5::edge(21,26):- e2126.

abducible e2165.

0.5::edge(21,65):- e2165.

abducible e2183.

0.5::edge(21,83):- e2183.

abducible e2230.

0.5::edge(22,30):- e2230.

abducible e2238.

0.5::edge(22,38):- e2238.

abducible e2243.

0.5::edge(22,43):- e2243.

abducible e2247.

0.5::edge(22,47):- e2247.

abducible e2284.

0.5::edge(22,84):- e2284.

abducible e2339.

0.5::edge(23,39):- e2339.

abducible e2376.

0.5::edge(23,76):- e2376.

abducible e2378.

0.5::edge(23,78):- e2378.

abducible e2565.

0.5::edge(25,65):- e2565.

abducible e2574.

0.5::edge(25,74):- e2574.

abducible e2654.

0.5::edge(26,54):- e2654.

abducible e2732.

0.5::edge(27,32):- e2732.

abducible e2746.

0.5::edge(27,46):- e2746.

abducible e2844.

0.5::edge(28,44):- e2844.

abducible e3438.

0.5::edge(34,38):- e3438.

abducible e3445.

0.5::edge(34,45):- e3445.

abducible e3468.

0.5::edge(34,68):- e3468.

abducible e3482.

0.5::edge(34,82):- e3482.

abducible e3487.

0.5::edge(34,87):- e3487.

abducible e3548.

0.5::edge(35,48):- e3548.

abducible e3556.

0.5::edge(35,56):- e3556.

abducible e3639.

0.5::edge(36,39):- e3639.

abducible e3681.

0.5::edge(36,81):- e3681.

abducible e3842.

0.5::edge(38,42):- e3842.

abducible e3860.

0.5::edge(38,60):- e3860.

abducible e3861.

0.5::edge(38,61):- e3861.

abducible e3956.

0.5::edge(39,56):- e3956.

abducible e3973.

0.5::edge(39,73):- e3973.

abducible e4072.

0.5::edge(40,72):- e4072.

abducible e4151.

0.5::edge(41,51):- e4151.

abducible e4153.

0.5::edge(41,53):- e4153.

abducible e4262.

0.5::edge(42,62):- e4262.

abducible e4264.

0.5::edge(42,64):- e4264.

abducible e4269.

0.5::edge(42,69):- e4269.

abducible e4955.

0.5::edge(49,55):- e4955.

abducible e5270.

0.5::edge(52,70):- e5270.

abducible e5460.

0.5::edge(54,60):- e5460.

abducible e6077.

0.5::edge(60,77):- e6077.

abducible e6169.

0.5::edge(61,69):- e6169.

abducible e6170.

0.5::edge(61,70):- e6170.

abducible e6171.

0.5::edge(61,71):- e6171.

abducible e6479.

0.5::edge(64,79):- e6479.

abducible e6671.

0.5::edge(66,71):- e6671.

abducible e6973.

0.5::edge(69,73):- e6973.

abducible e7072.

0.5::edge(70,72):- e7072.

abducible e7180.

0.5::edge(71,80):- e7180.

abducible e7585.

0.5::edge(75,85):- e7585.

abducible e7678.

0.5::edge(76,78):- e7678.

abducible e7684.

0.5::edge(76,84):- e7684.

abducible e8283.

0.5::edge(82,83):- e8283.

abducible e8688.

0.5::edge(86,88):- e8688.



:- path(0, 89 ,L), L < 6.





ev :- path(0,89,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

