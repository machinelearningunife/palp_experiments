:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e06.

0.5::edge(0,6):- e06.

abducible e09.

0.5::edge(0,9):- e09.

abducible e013.

0.5::edge(0,13):- e013.

abducible e016.

0.5::edge(0,16):- e016.

abducible e028.

0.5::edge(0,28):- e028.

abducible e034.

0.5::edge(0,34):- e034.

abducible e036.

0.5::edge(0,36):- e036.

abducible e042.

0.5::edge(0,42):- e042.

abducible e054.

0.5::edge(0,54):- e054.

abducible e083.

0.5::edge(0,83):- e083.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e16.

0.5::edge(1,6):- e16.

abducible e112.

0.5::edge(1,12):- e112.

abducible e121.

0.5::edge(1,21):- e121.

abducible e130.

0.5::edge(1,30):- e130.

abducible e136.

0.5::edge(1,36):- e136.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e27.

0.5::edge(2,7):- e27.

abducible e28.

0.5::edge(2,8):- e28.

abducible e29.

0.5::edge(2,9):- e29.

abducible e212.

0.5::edge(2,12):- e212.

abducible e213.

0.5::edge(2,13):- e213.

abducible e214.

0.5::edge(2,14):- e214.

abducible e218.

0.5::edge(2,18):- e218.

abducible e219.

0.5::edge(2,19):- e219.

abducible e235.

0.5::edge(2,35):- e235.

abducible e244.

0.5::edge(2,44):- e244.

abducible e255.

0.5::edge(2,55):- e255.

abducible e266.

0.5::edge(2,66):- e266.

abducible e267.

0.5::edge(2,67):- e267.

abducible e270.

0.5::edge(2,70):- e270.

abducible e280.

0.5::edge(2,80):- e280.

abducible e285.

0.5::edge(2,85):- e285.

abducible e289.

0.5::edge(2,89):- e289.

abducible e35.

0.5::edge(3,5):- e35.

abducible e311.

0.5::edge(3,11):- e311.

abducible e45.

0.5::edge(4,5):- e45.

abducible e415.

0.5::edge(4,15):- e415.

abducible e418.

0.5::edge(4,18):- e418.

abducible e426.

0.5::edge(4,26):- e426.

abducible e444.

0.5::edge(4,44):- e444.

abducible e452.

0.5::edge(4,52):- e452.

abducible e460.

0.5::edge(4,60):- e460.

abducible e476.

0.5::edge(4,76):- e476.

abducible e510.

0.5::edge(5,10):- e510.

abducible e511.

0.5::edge(5,11):- e511.

abducible e523.

0.5::edge(5,23):- e523.

abducible e533.

0.5::edge(5,33):- e533.

abducible e549.

0.5::edge(5,49):- e549.

abducible e558.

0.5::edge(5,58):- e558.

abducible e67.

0.5::edge(6,7):- e67.

abducible e68.

0.5::edge(6,8):- e68.

abducible e610.

0.5::edge(6,10):- e610.

abducible e614.

0.5::edge(6,14):- e614.

abducible e615.

0.5::edge(6,15):- e615.

abducible e616.

0.5::edge(6,16):- e616.

abducible e617.

0.5::edge(6,17):- e617.

abducible e619.

0.5::edge(6,19):- e619.

abducible e620.

0.5::edge(6,20):- e620.

abducible e626.

0.5::edge(6,26):- e626.

abducible e631.

0.5::edge(6,31):- e631.

abducible e633.

0.5::edge(6,33):- e633.

abducible e638.

0.5::edge(6,38):- e638.

abducible e639.

0.5::edge(6,39):- e639.

abducible e640.

0.5::edge(6,40):- e640.

abducible e643.

0.5::edge(6,43):- e643.

abducible e649.

0.5::edge(6,49):- e649.

abducible e653.

0.5::edge(6,53):- e653.

abducible e659.

0.5::edge(6,59):- e659.

abducible e661.

0.5::edge(6,61):- e661.

abducible e686.

0.5::edge(6,86):- e686.

abducible e728.

0.5::edge(7,28):- e728.

abducible e742.

0.5::edge(7,42):- e742.

abducible e772.

0.5::edge(7,72):- e772.

abducible e782.

0.5::edge(7,82):- e782.

abducible e887.

0.5::edge(8,87):- e887.

abducible e929.

0.5::edge(9,29):- e929.

abducible e948.

0.5::edge(9,48):- e948.

abducible e965.

0.5::edge(9,65):- e965.

abducible e1081.

0.5::edge(10,81):- e1081.

abducible e1125.

0.5::edge(11,25):- e1125.

abducible e1141.

0.5::edge(11,41):- e1141.

abducible e1155.

0.5::edge(11,55):- e1155.

abducible e1161.

0.5::edge(11,61):- e1161.

abducible e1222.

0.5::edge(12,22):- e1222.

abducible e1258.

0.5::edge(12,58):- e1258.

abducible e1384.

0.5::edge(13,84):- e1384.

abducible e1388.

0.5::edge(13,88):- e1388.

abducible e1457.

0.5::edge(14,57):- e1457.

abducible e1569.

0.5::edge(15,69):- e1569.

abducible e1617.

0.5::edge(16,17):- e1617.

abducible e1620.

0.5::edge(16,20):- e1620.

abducible e1624.

0.5::edge(16,24):- e1624.

abducible e1625.

0.5::edge(16,25):- e1625.

abducible e1648.

0.5::edge(16,48):- e1648.

abducible e1723.

0.5::edge(17,23):- e1723.

abducible e1727.

0.5::edge(17,27):- e1727.

abducible e1729.

0.5::edge(17,29):- e1729.

abducible e1730.

0.5::edge(17,30):- e1730.

abducible e1827.

0.5::edge(18,27):- e1827.

abducible e1834.

0.5::edge(18,34):- e1834.

abducible e1851.

0.5::edge(18,51):- e1851.

abducible e1863.

0.5::edge(18,63):- e1863.

abducible e1865.

0.5::edge(18,65):- e1865.

abducible e1869.

0.5::edge(18,69):- e1869.

abducible e2021.

0.5::edge(20,21):- e2021.

abducible e2031.

0.5::edge(20,31):- e2031.

abducible e2032.

0.5::edge(20,32):- e2032.

abducible e2056.

0.5::edge(20,56):- e2056.

abducible e2063.

0.5::edge(20,63):- e2063.

abducible e2078.

0.5::edge(20,78):- e2078.

abducible e2122.

0.5::edge(21,22):- e2122.

abducible e2132.

0.5::edge(21,32):- e2132.

abducible e2153.

0.5::edge(21,53):- e2153.

abducible e2179.

0.5::edge(21,79):- e2179.

abducible e2181.

0.5::edge(21,81):- e2181.

abducible e2187.

0.5::edge(21,87):- e2187.

abducible e2224.

0.5::edge(22,24):- e2224.

abducible e2252.

0.5::edge(22,52):- e2252.

abducible e2274.

0.5::edge(22,74):- e2274.

abducible e2371.

0.5::edge(23,71):- e2371.

abducible e2646.

0.5::edge(26,46):- e2646.

abducible e2647.

0.5::edge(26,47):- e2647.

abducible e2662.

0.5::edge(26,62):- e2662.

abducible e2680.

0.5::edge(26,80):- e2680.

abducible e2741.

0.5::edge(27,41):- e2741.

abducible e2785.

0.5::edge(27,85):- e2785.

abducible e2940.

0.5::edge(29,40):- e2940.

abducible e3039.

0.5::edge(30,39):- e3039.

abducible e3045.

0.5::edge(30,45):- e3045.

abducible e3071.

0.5::edge(30,71):- e3071.

abducible e3079.

0.5::edge(30,79):- e3079.

abducible e3137.

0.5::edge(31,37):- e3137.

abducible e3151.

0.5::edge(31,51):- e3151.

abducible e3235.

0.5::edge(32,35):- e3235.

abducible e3238.

0.5::edge(32,38):- e3238.

abducible e3243.

0.5::edge(32,43):- e3243.

abducible e3245.

0.5::edge(32,45):- e3245.

abducible e3277.

0.5::edge(32,77):- e3277.

abducible e3337.

0.5::edge(33,37):- e3337.

abducible e3473.

0.5::edge(34,73):- e3473.

abducible e3850.

0.5::edge(38,50):- e3850.

abducible e3882.

0.5::edge(38,82):- e3882.

abducible e3954.

0.5::edge(39,54):- e3954.

abducible e3989.

0.5::edge(39,89):- e3989.

abducible e4056.

0.5::edge(40,56):- e4056.

abducible e4174.

0.5::edge(41,74):- e4174.

abducible e4247.

0.5::edge(42,47):- e4247.

abducible e4270.

0.5::edge(42,70):- e4270.

abducible e4278.

0.5::edge(42,78):- e4278.

abducible e4346.

0.5::edge(43,46):- e4346.

abducible e4576.

0.5::edge(45,76):- e4576.

abducible e4650.

0.5::edge(46,50):- e4650.

abducible e4662.

0.5::edge(46,62):- e4662.

abducible e4959.

0.5::edge(49,59):- e4959.

abducible e4960.

0.5::edge(49,60):- e4960.

abducible e4964.

0.5::edge(49,64):- e4964.

abducible e5066.

0.5::edge(50,66):- e5066.

abducible e5157.

0.5::edge(51,57):- e5157.

abducible e5273.

0.5::edge(52,73):- e5273.

abducible e5275.

0.5::edge(52,75):- e5275.

abducible e5288.

0.5::edge(52,88):- e5288.

abducible e5864.

0.5::edge(58,64):- e5864.

abducible e5877.

0.5::edge(58,77):- e5877.

abducible e5883.

0.5::edge(58,83):- e5883.

abducible e6367.

0.5::edge(63,67):- e6367.

abducible e6568.

0.5::edge(65,68):- e6568.

abducible e6575.

0.5::edge(65,75):- e6575.

abducible e6768.

0.5::edge(67,68):- e6768.

abducible e6772.

0.5::edge(67,72):- e6772.

abducible e7784.

0.5::edge(77,84):- e7784.

abducible e7886.

0.5::edge(78,86):- e7886.



:- path(0, 89 ,L), L < 6.





ev :- path(0,89,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

