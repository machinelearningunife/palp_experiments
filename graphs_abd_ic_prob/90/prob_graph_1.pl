:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e040.

0.5::edge(0,40):- e040.

abducible e069.

0.5::edge(0,69):- e069.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e19.

0.5::edge(1,9):- e19.

abducible e110.

0.5::edge(1,10):- e110.

abducible e112.

0.5::edge(1,12):- e112.

abducible e113.

0.5::edge(1,13):- e113.

abducible e114.

0.5::edge(1,14):- e114.

abducible e119.

0.5::edge(1,19):- e119.

abducible e120.

0.5::edge(1,20):- e120.

abducible e122.

0.5::edge(1,22):- e122.

abducible e123.

0.5::edge(1,23):- e123.

abducible e124.

0.5::edge(1,24):- e124.

abducible e127.

0.5::edge(1,27):- e127.

abducible e130.

0.5::edge(1,30):- e130.

abducible e137.

0.5::edge(1,37):- e137.

abducible e153.

0.5::edge(1,53):- e153.

abducible e159.

0.5::edge(1,59):- e159.

abducible e165.

0.5::edge(1,65):- e165.

abducible e172.

0.5::edge(1,72):- e172.

abducible e176.

0.5::edge(1,76):- e176.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e25.

0.5::edge(2,5):- e25.

abducible e221.

0.5::edge(2,21):- e221.

abducible e229.

0.5::edge(2,29):- e229.

abducible e239.

0.5::edge(2,39):- e239.

abducible e255.

0.5::edge(2,55):- e255.

abducible e264.

0.5::edge(2,64):- e264.

abducible e36.

0.5::edge(3,6):- e36.

abducible e37.

0.5::edge(3,7):- e37.

abducible e317.

0.5::edge(3,17):- e317.

abducible e318.

0.5::edge(3,18):- e318.

abducible e348.

0.5::edge(3,48):- e348.

abducible e349.

0.5::edge(3,49):- e349.

abducible e351.

0.5::edge(3,51):- e351.

abducible e379.

0.5::edge(3,79):- e379.

abducible e45.

0.5::edge(4,5):- e45.

abducible e46.

0.5::edge(4,6):- e46.

abducible e428.

0.5::edge(4,28):- e428.

abducible e438.

0.5::edge(4,38):- e438.

abducible e444.

0.5::edge(4,44):- e444.

abducible e460.

0.5::edge(4,60):- e460.

abducible e57.

0.5::edge(5,7):- e57.

abducible e58.

0.5::edge(5,8):- e58.

abducible e514.

0.5::edge(5,14):- e514.

abducible e531.

0.5::edge(5,31):- e531.

abducible e532.

0.5::edge(5,32):- e532.

abducible e546.

0.5::edge(5,46):- e546.

abducible e549.

0.5::edge(5,49):- e549.

abducible e566.

0.5::edge(5,66):- e566.

abducible e573.

0.5::edge(5,73):- e573.

abducible e582.

0.5::edge(5,82):- e582.

abducible e583.

0.5::edge(5,83):- e583.

abducible e68.

0.5::edge(6,8):- e68.

abducible e623.

0.5::edge(6,23):- e623.

abducible e663.

0.5::edge(6,63):- e663.

abducible e666.

0.5::edge(6,66):- e666.

abducible e79.

0.5::edge(7,9):- e79.

abducible e711.

0.5::edge(7,11):- e711.

abducible e712.

0.5::edge(7,12):- e712.

abducible e713.

0.5::edge(7,13):- e713.

abducible e725.

0.5::edge(7,25):- e725.

abducible e729.

0.5::edge(7,29):- e729.

abducible e730.

0.5::edge(7,30):- e730.

abducible e740.

0.5::edge(7,40):- e740.

abducible e746.

0.5::edge(7,46):- e746.

abducible e747.

0.5::edge(7,47):- e747.

abducible e751.

0.5::edge(7,51):- e751.

abducible e752.

0.5::edge(7,52):- e752.

abducible e755.

0.5::edge(7,55):- e755.

abducible e758.

0.5::edge(7,58):- e758.

abducible e771.

0.5::edge(7,71):- e771.

abducible e811.

0.5::edge(8,11):- e811.

abducible e833.

0.5::edge(8,33):- e833.

abducible e834.

0.5::edge(8,34):- e834.

abducible e836.

0.5::edge(8,36):- e836.

abducible e868.

0.5::edge(8,68):- e868.

abducible e910.

0.5::edge(9,10):- e910.

abducible e916.

0.5::edge(9,16):- e916.

abducible e934.

0.5::edge(9,34):- e934.

abducible e943.

0.5::edge(9,43):- e943.

abducible e956.

0.5::edge(9,56):- e956.

abducible e962.

0.5::edge(9,62):- e962.

abducible e969.

0.5::edge(9,69):- e969.

abducible e984.

0.5::edge(9,84):- e984.

abducible e987.

0.5::edge(9,87):- e987.

abducible e1015.

0.5::edge(10,15):- e1015.

abducible e1026.

0.5::edge(10,26):- e1026.

abducible e1036.

0.5::edge(10,36):- e1036.

abducible e1042.

0.5::edge(10,42):- e1042.

abducible e1043.

0.5::edge(10,43):- e1043.

abducible e1054.

0.5::edge(10,54):- e1054.

abducible e1067.

0.5::edge(10,67):- e1067.

abducible e1116.

0.5::edge(11,16):- e1116.

abducible e1121.

0.5::edge(11,21):- e1121.

abducible e1127.

0.5::edge(11,27):- e1127.

abducible e1132.

0.5::edge(11,32):- e1132.

abducible e1135.

0.5::edge(11,35):- e1135.

abducible e1153.

0.5::edge(11,53):- e1153.

abducible e1215.

0.5::edge(12,15):- e1215.

abducible e1226.

0.5::edge(12,26):- e1226.

abducible e1238.

0.5::edge(12,38):- e1238.

abducible e1248.

0.5::edge(12,48):- e1248.

abducible e1270.

0.5::edge(12,70):- e1270.

abducible e1341.

0.5::edge(13,41):- e1341.

abducible e1388.

0.5::edge(13,88):- e1388.

abducible e1418.

0.5::edge(14,18):- e1418.

abducible e1422.

0.5::edge(14,22):- e1422.

abducible e1424.

0.5::edge(14,24):- e1424.

abducible e1435.

0.5::edge(14,35):- e1435.

abducible e1437.

0.5::edge(14,37):- e1437.

abducible e1462.

0.5::edge(14,62):- e1462.

abducible e1617.

0.5::edge(16,17):- e1617.

abducible e1639.

0.5::edge(16,39):- e1639.

abducible e1642.

0.5::edge(16,42):- e1642.

abducible e1650.

0.5::edge(16,50):- e1650.

abducible e1654.

0.5::edge(16,54):- e1654.

abducible e1719.

0.5::edge(17,19):- e1719.

abducible e1720.

0.5::edge(17,20):- e1720.

abducible e1771.

0.5::edge(17,71):- e1771.

abducible e1772.

0.5::edge(17,72):- e1772.

abducible e1774.

0.5::edge(17,74):- e1774.

abducible e1776.

0.5::edge(17,76):- e1776.

abducible e1931.

0.5::edge(19,31):- e1931.

abducible e2163.

0.5::edge(21,63):- e2163.

abducible e2256.

0.5::edge(22,56):- e2256.

abducible e2275.

0.5::edge(22,75):- e2275.

abducible e2278.

0.5::edge(22,78):- e2278.

abducible e2279.

0.5::edge(22,79):- e2279.

abducible e2280.

0.5::edge(22,80):- e2280.

abducible e2325.

0.5::edge(23,25):- e2325.

abducible e2365.

0.5::edge(23,65):- e2365.

abducible e2387.

0.5::edge(23,87):- e2387.

abducible e2528.

0.5::edge(25,28):- e2528.

abducible e2561.

0.5::edge(25,61):- e2561.

abducible e2633.

0.5::edge(26,33):- e2633.

abducible e2657.

0.5::edge(26,57):- e2657.

abducible e2659.

0.5::edge(26,59):- e2659.

abducible e2741.

0.5::edge(27,41):- e2741.

abducible e2881.

0.5::edge(28,81):- e2881.

abducible e3064.

0.5::edge(30,64):- e3064.

abducible e3247.

0.5::edge(32,47):- e3247.

abducible e3468.

0.5::edge(34,68):- e3468.

abducible e3744.

0.5::edge(37,44):- e3744.

abducible e3780.

0.5::edge(37,80):- e3780.

abducible e3873.

0.5::edge(38,73):- e3873.

abducible e4045.

0.5::edge(40,45):- e4045.

abducible e4150.

0.5::edge(41,50):- e4150.

abducible e4160.

0.5::edge(41,60):- e4160.

abducible e4245.

0.5::edge(42,45):- e4245.

abducible e4258.

0.5::edge(42,58):- e4258.

abducible e4374.

0.5::edge(43,74):- e4374.

abducible e4375.

0.5::edge(43,75):- e4375.

abducible e4378.

0.5::edge(43,78):- e4378.

abducible e4761.

0.5::edge(47,61):- e4761.

abducible e4852.

0.5::edge(48,52):- e4852.

abducible e4967.

0.5::edge(49,67):- e4967.

abducible e5189.

0.5::edge(51,89):- e5189.

abducible e5377.

0.5::edge(53,77):- e5377.

abducible e5657.

0.5::edge(56,57):- e5657.

abducible e5783.

0.5::edge(57,83):- e5783.

abducible e5785.

0.5::edge(57,85):- e5785.

abducible e6086.

0.5::edge(60,86):- e6086.

abducible e6177.

0.5::edge(61,77):- e6177.

abducible e6381.

0.5::edge(63,81):- e6381.

abducible e6385.

0.5::edge(63,85):- e6385.

abducible e6670.

0.5::edge(66,70):- e6670.

abducible e6686.

0.5::edge(66,86):- e6686.

abducible e7482.

0.5::edge(74,82):- e7482.

abducible e8184.

0.5::edge(81,84):- e8184.

abducible e8788.

0.5::edge(87,88):- e8788.

abducible e8889.

0.5::edge(88,89):- e8889.



:- path(0, 89 ,L), L < 6.





ev :- path(0,89,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

