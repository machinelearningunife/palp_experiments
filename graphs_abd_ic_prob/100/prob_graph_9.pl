:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e04.

0.5::edge(0,4):- e04.

abducible e021.

0.5::edge(0,21):- e021.

abducible e030.

0.5::edge(0,30):- e030.

abducible e093.

0.5::edge(0,93):- e093.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e17.

0.5::edge(1,7):- e17.

abducible e18.

0.5::edge(1,8):- e18.

abducible e112.

0.5::edge(1,12):- e112.

abducible e113.

0.5::edge(1,13):- e113.

abducible e115.

0.5::edge(1,15):- e115.

abducible e118.

0.5::edge(1,18):- e118.

abducible e119.

0.5::edge(1,19):- e119.

abducible e125.

0.5::edge(1,25):- e125.

abducible e127.

0.5::edge(1,27):- e127.

abducible e140.

0.5::edge(1,40):- e140.

abducible e141.

0.5::edge(1,41):- e141.

abducible e146.

0.5::edge(1,46):- e146.

abducible e149.

0.5::edge(1,49):- e149.

abducible e163.

0.5::edge(1,63):- e163.

abducible e175.

0.5::edge(1,75):- e175.

abducible e177.

0.5::edge(1,77):- e177.

abducible e192.

0.5::edge(1,92):- e192.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e26.

0.5::edge(2,6):- e26.

abducible e27.

0.5::edge(2,7):- e27.

abducible e28.

0.5::edge(2,8):- e28.

abducible e29.

0.5::edge(2,9):- e29.

abducible e211.

0.5::edge(2,11):- e211.

abducible e212.

0.5::edge(2,12):- e212.

abducible e213.

0.5::edge(2,13):- e213.

abducible e217.

0.5::edge(2,17):- e217.

abducible e224.

0.5::edge(2,24):- e224.

abducible e233.

0.5::edge(2,33):- e233.

abducible e234.

0.5::edge(2,34):- e234.

abducible e239.

0.5::edge(2,39):- e239.

abducible e242.

0.5::edge(2,42):- e242.

abducible e247.

0.5::edge(2,47):- e247.

abducible e250.

0.5::edge(2,50):- e250.

abducible e251.

0.5::edge(2,51):- e251.

abducible e257.

0.5::edge(2,57):- e257.

abducible e258.

0.5::edge(2,58):- e258.

abducible e271.

0.5::edge(2,71):- e271.

abducible e274.

0.5::edge(2,74):- e274.

abducible e278.

0.5::edge(2,78):- e278.

abducible e285.

0.5::edge(2,85):- e285.

abducible e286.

0.5::edge(2,86):- e286.

abducible e290.

0.5::edge(2,90):- e290.

abducible e35.

0.5::edge(3,5):- e35.

abducible e311.

0.5::edge(3,11):- e311.

abducible e314.

0.5::edge(3,14):- e314.

abducible e323.

0.5::edge(3,23):- e323.

abducible e335.

0.5::edge(3,35):- e335.

abducible e344.

0.5::edge(3,44):- e344.

abducible e363.

0.5::edge(3,63):- e363.

abducible e380.

0.5::edge(3,80):- e380.

abducible e45.

0.5::edge(4,5):- e45.

abducible e46.

0.5::edge(4,6):- e46.

abducible e49.

0.5::edge(4,9):- e49.

abducible e410.

0.5::edge(4,10):- e410.

abducible e418.

0.5::edge(4,18):- e418.

abducible e422.

0.5::edge(4,22):- e422.

abducible e426.

0.5::edge(4,26):- e426.

abducible e433.

0.5::edge(4,33):- e433.

abducible e440.

0.5::edge(4,40):- e440.

abducible e444.

0.5::edge(4,44):- e444.

abducible e453.

0.5::edge(4,53):- e453.

abducible e456.

0.5::edge(4,56):- e456.

abducible e461.

0.5::edge(4,61):- e461.

abducible e487.

0.5::edge(4,87):- e487.

abducible e510.

0.5::edge(5,10):- e510.

abducible e519.

0.5::edge(5,19):- e519.

abducible e520.

0.5::edge(5,20):- e520.

abducible e521.

0.5::edge(5,21):- e521.

abducible e522.

0.5::edge(5,22):- e522.

abducible e529.

0.5::edge(5,29):- e529.

abducible e530.

0.5::edge(5,30):- e530.

abducible e537.

0.5::edge(5,37):- e537.

abducible e545.

0.5::edge(5,45):- e545.

abducible e547.

0.5::edge(5,47):- e547.

abducible e549.

0.5::edge(5,49):- e549.

abducible e568.

0.5::edge(5,68):- e568.

abducible e581.

0.5::edge(5,81):- e581.

abducible e588.

0.5::edge(5,88):- e588.

abducible e589.

0.5::edge(5,89):- e589.

abducible e591.

0.5::edge(5,91):- e591.

abducible e593.

0.5::edge(5,93):- e593.

abducible e643.

0.5::edge(6,43):- e643.

abducible e661.

0.5::edge(6,61):- e661.

abducible e695.

0.5::edge(6,95):- e695.

abducible e714.

0.5::edge(7,14):- e714.

abducible e736.

0.5::edge(7,36):- e736.

abducible e768.

0.5::edge(7,68):- e768.

abducible e820.

0.5::edge(8,20):- e820.

abducible e843.

0.5::edge(8,43):- e843.

abducible e871.

0.5::edge(8,71):- e871.

abducible e938.

0.5::edge(9,38):- e938.

abducible e950.

0.5::edge(9,50):- e950.

abducible e1059.

0.5::edge(10,59):- e1059.

abducible e1064.

0.5::edge(10,64):- e1064.

abducible e1083.

0.5::edge(10,83):- e1083.

abducible e1092.

0.5::edge(10,92):- e1092.

abducible e1216.

0.5::edge(12,16):- e1216.

abducible e1224.

0.5::edge(12,24):- e1224.

abducible e1258.

0.5::edge(12,58):- e1258.

abducible e1316.

0.5::edge(13,16):- e1316.

abducible e1317.

0.5::edge(13,17):- e1317.

abducible e1415.

0.5::edge(14,15):- e1415.

abducible e1483.

0.5::edge(14,83):- e1483.

abducible e1498.

0.5::edge(14,98):- e1498.

abducible e1528.

0.5::edge(15,28):- e1528.

abducible e1541.

0.5::edge(15,41):- e1541.

abducible e1584.

0.5::edge(15,84):- e1584.

abducible e1597.

0.5::edge(15,97):- e1597.

abducible e1627.

0.5::edge(16,27):- e1627.

abducible e1628.

0.5::edge(16,28):- e1628.

abducible e1642.

0.5::edge(16,42):- e1642.

abducible e1646.

0.5::edge(16,46):- e1646.

abducible e1669.

0.5::edge(16,69):- e1669.

abducible e1679.

0.5::edge(16,79):- e1679.

abducible e1738.

0.5::edge(17,38):- e1738.

abducible e1778.

0.5::edge(17,78):- e1778.

abducible e1823.

0.5::edge(18,23):- e1823.

abducible e1832.

0.5::edge(18,32):- e1832.

abducible e1852.

0.5::edge(18,52):- e1852.

abducible e1929.

0.5::edge(19,29):- e1929.

abducible e1954.

0.5::edge(19,54):- e1954.

abducible e1957.

0.5::edge(19,57):- e1957.

abducible e1959.

0.5::edge(19,59):- e1959.

abducible e1969.

0.5::edge(19,69):- e1969.

abducible e1975.

0.5::edge(19,75):- e1975.

abducible e1986.

0.5::edge(19,86):- e1986.

abducible e2032.

0.5::edge(20,32):- e2032.

abducible e2034.

0.5::edge(20,34):- e2034.

abducible e2048.

0.5::edge(20,48):- e2048.

abducible e2125.

0.5::edge(21,25):- e2125.

abducible e2170.

0.5::edge(21,70):- e2170.

abducible e2172.

0.5::edge(21,72):- e2172.

abducible e2248.

0.5::edge(22,48):- e2248.

abducible e2267.

0.5::edge(22,67):- e2267.

abducible e2326.

0.5::edge(23,26):- e2326.

abducible e2336.

0.5::edge(23,36):- e2336.

abducible e2453.

0.5::edge(24,53):- e2453.

abducible e2456.

0.5::edge(24,56):- e2456.

abducible e2496.

0.5::edge(24,96):- e2496.

abducible e2677.

0.5::edge(26,77):- e2677.

abducible e2737.

0.5::edge(27,37):- e2737.

abducible e2765.

0.5::edge(27,65):- e2765.

abducible e2831.

0.5::edge(28,31):- e2831.

abducible e2855.

0.5::edge(28,55):- e2855.

abducible e2865.

0.5::edge(28,65):- e2865.

abducible e2874.

0.5::edge(28,74):- e2874.

abducible e2887.

0.5::edge(28,87):- e2887.

abducible e2894.

0.5::edge(28,94):- e2894.

abducible e2931.

0.5::edge(29,31):- e2931.

abducible e2945.

0.5::edge(29,45):- e2945.

abducible e2954.

0.5::edge(29,54):- e2954.

abducible e2962.

0.5::edge(29,62):- e2962.

abducible e3035.

0.5::edge(30,35):- e3035.

abducible e3062.

0.5::edge(30,62):- e3062.

abducible e3291.

0.5::edge(32,91):- e3291.

abducible e3351.

0.5::edge(33,51):- e3351.

abducible e3476.

0.5::edge(34,76):- e3476.

abducible e3573.

0.5::edge(35,73):- e3573.

abducible e3652.

0.5::edge(36,52):- e3652.

abducible e3766.

0.5::edge(37,66):- e3766.

abducible e3839.

0.5::edge(38,39):- e3839.

abducible e3888.

0.5::edge(38,88):- e3888.

abducible e4155.

0.5::edge(41,55):- e4155.

abducible e4198.

0.5::edge(41,98):- e4198.

abducible e4481.

0.5::edge(44,81):- e4481.

abducible e4482.

0.5::edge(44,82):- e4482.

abducible e4489.

0.5::edge(44,89):- e4489.

abducible e4490.

0.5::edge(44,90):- e4490.

abducible e4567.

0.5::edge(45,67):- e4567.

abducible e4573.

0.5::edge(45,73):- e4573.

abducible e4760.

0.5::edge(47,60):- e4760.

abducible e4784.

0.5::edge(47,84):- e4784.

abducible e4864.

0.5::edge(48,64):- e4864.

abducible e5060.

0.5::edge(50,60):- e5060.

abducible e5066.

0.5::edge(50,66):- e5066.

abducible e5072.

0.5::edge(50,72):- e5072.

abducible e5097.

0.5::edge(50,97):- e5097.

abducible e5570.

0.5::edge(55,70):- e5570.

abducible e5694.

0.5::edge(56,94):- e5694.

abducible e5799.

0.5::edge(57,99):- e5799.

abducible e6376.

0.5::edge(63,76):- e6376.

abducible e7182.

0.5::edge(71,82):- e7182.

abducible e7395.

0.5::edge(73,95):- e7395.

abducible e7396.

0.5::edge(73,96):- e7396.

abducible e7399.

0.5::edge(73,99):- e7399.

abducible e7579.

0.5::edge(75,79):- e7579.

abducible e7680.

0.5::edge(76,80):- e7680.

abducible e7785.

0.5::edge(77,85):- e7785.



:- path(0, 99 ,L), L < 6.





ev :- path(0,99,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

