:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e05.

0.5::edge(0,5):- e05.

abducible e016.

0.5::edge(0,16):- e016.

abducible e035.

0.5::edge(0,35):- e035.

abducible e040.

0.5::edge(0,40):- e040.

abducible e048.

0.5::edge(0,48):- e048.

abducible e049.

0.5::edge(0,49):- e049.

abducible e052.

0.5::edge(0,52):- e052.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e14.

0.5::edge(1,4):- e14.

abducible e15.

0.5::edge(1,5):- e15.

abducible e18.

0.5::edge(1,8):- e18.

abducible e110.

0.5::edge(1,10):- e110.

abducible e112.

0.5::edge(1,12):- e112.

abducible e115.

0.5::edge(1,15):- e115.

abducible e122.

0.5::edge(1,22):- e122.

abducible e127.

0.5::edge(1,27):- e127.

abducible e128.

0.5::edge(1,28):- e128.

abducible e131.

0.5::edge(1,31):- e131.

abducible e134.

0.5::edge(1,34):- e134.

abducible e139.

0.5::edge(1,39):- e139.

abducible e147.

0.5::edge(1,47):- e147.

abducible e158.

0.5::edge(1,58):- e158.

abducible e174.

0.5::edge(1,74):- e174.

abducible e178.

0.5::edge(1,78):- e178.

abducible e188.

0.5::edge(1,88):- e188.

abducible e194.

0.5::edge(1,94):- e194.

abducible e195.

0.5::edge(1,95):- e195.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e27.

0.5::edge(2,7):- e27.

abducible e225.

0.5::edge(2,25):- e225.

abducible e226.

0.5::edge(2,26):- e226.

abducible e229.

0.5::edge(2,29):- e229.

abducible e230.

0.5::edge(2,30):- e230.

abducible e234.

0.5::edge(2,34):- e234.

abducible e236.

0.5::edge(2,36):- e236.

abducible e237.

0.5::edge(2,37):- e237.

abducible e238.

0.5::edge(2,38):- e238.

abducible e243.

0.5::edge(2,43):- e243.

abducible e245.

0.5::edge(2,45):- e245.

abducible e251.

0.5::edge(2,51):- e251.

abducible e269.

0.5::edge(2,69):- e269.

abducible e281.

0.5::edge(2,81):- e281.

abducible e285.

0.5::edge(2,85):- e285.

abducible e296.

0.5::edge(2,96):- e296.

abducible e297.

0.5::edge(2,97):- e297.

abducible e36.

0.5::edge(3,6):- e36.

abducible e39.

0.5::edge(3,9):- e39.

abducible e310.

0.5::edge(3,10):- e310.

abducible e316.

0.5::edge(3,16):- e316.

abducible e339.

0.5::edge(3,39):- e339.

abducible e345.

0.5::edge(3,45):- e345.

abducible e362.

0.5::edge(3,62):- e362.

abducible e370.

0.5::edge(3,70):- e370.

abducible e387.

0.5::edge(3,87):- e387.

abducible e411.

0.5::edge(4,11):- e411.

abducible e412.

0.5::edge(4,12):- e412.

abducible e415.

0.5::edge(4,15):- e415.

abducible e420.

0.5::edge(4,20):- e420.

abducible e424.

0.5::edge(4,24):- e424.

abducible e430.

0.5::edge(4,30):- e430.

abducible e432.

0.5::edge(4,32):- e432.

abducible e433.

0.5::edge(4,33):- e433.

abducible e435.

0.5::edge(4,35):- e435.

abducible e472.

0.5::edge(4,72):- e472.

abducible e473.

0.5::edge(4,73):- e473.

abducible e483.

0.5::edge(4,83):- e483.

abducible e56.

0.5::edge(5,6):- e56.

abducible e57.

0.5::edge(5,7):- e57.

abducible e59.

0.5::edge(5,9):- e59.

abducible e513.

0.5::edge(5,13):- e513.

abducible e517.

0.5::edge(5,17):- e517.

abducible e518.

0.5::edge(5,18):- e518.

abducible e519.

0.5::edge(5,19):- e519.

abducible e520.

0.5::edge(5,20):- e520.

abducible e523.

0.5::edge(5,23):- e523.

abducible e533.

0.5::edge(5,33):- e533.

abducible e541.

0.5::edge(5,41):- e541.

abducible e558.

0.5::edge(5,58):- e558.

abducible e559.

0.5::edge(5,59):- e559.

abducible e573.

0.5::edge(5,73):- e573.

abducible e68.

0.5::edge(6,8):- e68.

abducible e626.

0.5::edge(6,26):- e626.

abducible e637.

0.5::edge(6,37):- e637.

abducible e642.

0.5::edge(6,42):- e642.

abducible e661.

0.5::edge(6,61):- e661.

abducible e662.

0.5::edge(6,62):- e662.

abducible e668.

0.5::edge(6,68):- e668.

abducible e682.

0.5::edge(6,82):- e682.

abducible e719.

0.5::edge(7,19):- e719.

abducible e721.

0.5::edge(7,21):- e721.

abducible e724.

0.5::edge(7,24):- e724.

abducible e727.

0.5::edge(7,27):- e727.

abducible e731.

0.5::edge(7,31):- e731.

abducible e751.

0.5::edge(7,51):- e751.

abducible e754.

0.5::edge(7,54):- e754.

abducible e764.

0.5::edge(7,64):- e764.

abducible e787.

0.5::edge(7,87):- e787.

abducible e798.

0.5::edge(7,98):- e798.

abducible e811.

0.5::edge(8,11):- e811.

abducible e825.

0.5::edge(8,25):- e825.

abducible e846.

0.5::edge(8,46):- e846.

abducible e914.

0.5::edge(9,14):- e914.

abducible e922.

0.5::edge(9,22):- e922.

abducible e948.

0.5::edge(9,48):- e948.

abducible e1017.

0.5::edge(10,17):- e1017.

abducible e1028.

0.5::edge(10,28):- e1028.

abducible e1085.

0.5::edge(10,85):- e1085.

abducible e1113.

0.5::edge(11,13):- e1113.

abducible e1114.

0.5::edge(11,14):- e1114.

abducible e1129.

0.5::edge(11,29):- e1129.

abducible e1138.

0.5::edge(11,38):- e1138.

abducible e1144.

0.5::edge(11,44):- e1144.

abducible e1147.

0.5::edge(11,47):- e1147.

abducible e1188.

0.5::edge(11,88):- e1188.

abducible e1189.

0.5::edge(11,89):- e1189.

abducible e1346.

0.5::edge(13,46):- e1346.

abducible e1359.

0.5::edge(13,59):- e1359.

abducible e1365.

0.5::edge(13,65):- e1365.

abducible e1476.

0.5::edge(14,76):- e1476.

abducible e1518.

0.5::edge(15,18):- e1518.

abducible e1553.

0.5::edge(15,53):- e1553.

abducible e1555.

0.5::edge(15,55):- e1555.

abducible e1723.

0.5::edge(17,23):- e1723.

abducible e1755.

0.5::edge(17,55):- e1755.

abducible e1861.

0.5::edge(18,61):- e1861.

abducible e1875.

0.5::edge(18,75):- e1875.

abducible e2021.

0.5::edge(20,21):- e2021.

abducible e2143.

0.5::edge(21,43):- e2143.

abducible e2183.

0.5::edge(21,83):- e2183.

abducible e2198.

0.5::edge(21,98):- e2198.

abducible e2260.

0.5::edge(22,60):- e2260.

abducible e2280.

0.5::edge(22,80):- e2280.

abducible e2290.

0.5::edge(22,90):- e2290.

abducible e2291.

0.5::edge(22,91):- e2291.

abducible e2295.

0.5::edge(22,95):- e2295.

abducible e2336.

0.5::edge(23,36):- e2336.

abducible e2349.

0.5::edge(23,49):- e2349.

abducible e2440.

0.5::edge(24,40):- e2440.

abducible e2575.

0.5::edge(25,75):- e2575.

abducible e2576.

0.5::edge(25,76):- e2576.

abducible e2582.

0.5::edge(25,82):- e2582.

abducible e2732.

0.5::edge(27,32):- e2732.

abducible e2742.

0.5::edge(27,42):- e2742.

abducible e2757.

0.5::edge(27,57):- e2757.

abducible e2766.

0.5::edge(27,66):- e2766.

abducible e2844.

0.5::edge(28,44):- e2844.

abducible e2881.

0.5::edge(28,81):- e2881.

abducible e2884.

0.5::edge(28,84):- e2884.

abducible e2899.

0.5::edge(28,99):- e2899.

abducible e2954.

0.5::edge(29,54):- e2954.

abducible e2972.

0.5::edge(29,72):- e2972.

abducible e2974.

0.5::edge(29,74):- e2974.

abducible e2979.

0.5::edge(29,79):- e2979.

abducible e3063.

0.5::edge(30,63):- e3063.

abducible e3086.

0.5::edge(30,86):- e3086.

abducible e3160.

0.5::edge(31,60):- e3160.

abducible e3178.

0.5::edge(31,78):- e3178.

abducible e3194.

0.5::edge(31,94):- e3194.

abducible e3263.

0.5::edge(32,63):- e3263.

abducible e3297.

0.5::edge(32,97):- e3297.

abducible e3357.

0.5::edge(33,57):- e3357.

abducible e3364.

0.5::edge(33,64):- e3364.

abducible e3386.

0.5::edge(33,86):- e3386.

abducible e3392.

0.5::edge(33,92):- e3392.

abducible e3441.

0.5::edge(34,41):- e3441.

abducible e3452.

0.5::edge(34,52):- e3452.

abducible e3493.

0.5::edge(34,93):- e3493.

abducible e3667.

0.5::edge(36,67):- e3667.

abducible e3850.

0.5::edge(38,50):- e3850.

abducible e3984.

0.5::edge(39,84):- e3984.

abducible e4156.

0.5::edge(41,56):- e4156.

abducible e4170.

0.5::edge(41,70):- e4170.

abducible e4266.

0.5::edge(42,66):- e4266.

abducible e4392.

0.5::edge(43,92):- e4392.

abducible e4393.

0.5::edge(43,93):- e4393.

abducible e4450.

0.5::edge(44,50):- e4450.

abducible e4553.

0.5::edge(45,53):- e4553.

abducible e4671.

0.5::edge(46,71):- e4671.

abducible e4689.

0.5::edge(46,89):- e4689.

abducible e4991.

0.5::edge(49,91):- e4991.

abducible e5277.

0.5::edge(52,77):- e5277.

abducible e5296.

0.5::edge(52,96):- e5296.

abducible e5456.

0.5::edge(54,56):- e5456.

abducible e5690.

0.5::edge(56,90):- e5690.

abducible e5965.

0.5::edge(59,65):- e5965.

abducible e5967.

0.5::edge(59,67):- e5967.

abducible e6069.

0.5::edge(60,69):- e6069.

abducible e6180.

0.5::edge(61,80):- e6180.

abducible e6468.

0.5::edge(64,68):- e6468.

abducible e6599.

0.5::edge(65,99):- e6599.

abducible e6871.

0.5::edge(68,71):- e6871.

abducible e7077.

0.5::edge(70,77):- e7077.

abducible e7579.

0.5::edge(75,79):- e7579.



:- path(0, 99 ,L), L < 6.





ev :- path(0,99,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

