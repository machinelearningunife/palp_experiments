:- use_module(library(pita)).

:- pita.

:- begin_lpad.



path(A,B,N):-

path_(A,B,L),

flatten(L,LF),

length(LF,N).



path_(X,X,X).

path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).

abducible e02.

0.5::edge(0,2):- e02.

abducible e05.

0.5::edge(0,5):- e05.

abducible e09.

0.5::edge(0,9):- e09.

abducible e018.

0.5::edge(0,18):- e018.

abducible e027.

0.5::edge(0,27):- e027.

abducible e046.

0.5::edge(0,46):- e046.

abducible e12.

0.5::edge(1,2):- e12.

abducible e13.

0.5::edge(1,3):- e13.

abducible e15.

0.5::edge(1,5):- e15.

abducible e16.

0.5::edge(1,6):- e16.

abducible e17.

0.5::edge(1,7):- e17.

abducible e110.

0.5::edge(1,10):- e110.

abducible e111.

0.5::edge(1,11):- e111.

abducible e113.

0.5::edge(1,13):- e113.

abducible e114.

0.5::edge(1,14):- e114.

abducible e118.

0.5::edge(1,18):- e118.

abducible e121.

0.5::edge(1,21):- e121.

abducible e127.

0.5::edge(1,27):- e127.

abducible e135.

0.5::edge(1,35):- e135.

abducible e138.

0.5::edge(1,38):- e138.

abducible e150.

0.5::edge(1,50):- e150.

abducible e156.

0.5::edge(1,56):- e156.

abducible e170.

0.5::edge(1,70):- e170.

abducible e188.

0.5::edge(1,88):- e188.

abducible e198.

0.5::edge(1,98):- e198.

abducible e23.

0.5::edge(2,3):- e23.

abducible e24.

0.5::edge(2,4):- e24.

abducible e28.

0.5::edge(2,8):- e28.

abducible e211.

0.5::edge(2,11):- e211.

abducible e219.

0.5::edge(2,19):- e219.

abducible e226.

0.5::edge(2,26):- e226.

abducible e253.

0.5::edge(2,53):- e253.

abducible e258.

0.5::edge(2,58):- e258.

abducible e259.

0.5::edge(2,59):- e259.

abducible e262.

0.5::edge(2,62):- e262.

abducible e265.

0.5::edge(2,65):- e265.

abducible e270.

0.5::edge(2,70):- e270.

abducible e271.

0.5::edge(2,71):- e271.

abducible e272.

0.5::edge(2,72):- e272.

abducible e274.

0.5::edge(2,74):- e274.

abducible e280.

0.5::edge(2,80):- e280.

abducible e295.

0.5::edge(2,95):- e295.

abducible e34.

0.5::edge(3,4):- e34.

abducible e37.

0.5::edge(3,7):- e37.

abducible e39.

0.5::edge(3,9):- e39.

abducible e313.

0.5::edge(3,13):- e313.

abducible e331.

0.5::edge(3,31):- e331.

abducible e333.

0.5::edge(3,33):- e333.

abducible e340.

0.5::edge(3,40):- e340.

abducible e341.

0.5::edge(3,41):- e341.

abducible e344.

0.5::edge(3,44):- e344.

abducible e360.

0.5::edge(3,60):- e360.

abducible e361.

0.5::edge(3,61):- e361.

abducible e385.

0.5::edge(3,85):- e385.

abducible e393.

0.5::edge(3,93):- e393.

abducible e46.

0.5::edge(4,6):- e46.

abducible e414.

0.5::edge(4,14):- e414.

abducible e420.

0.5::edge(4,20):- e420.

abducible e425.

0.5::edge(4,25):- e425.

abducible e426.

0.5::edge(4,26):- e426.

abducible e428.

0.5::edge(4,28):- e428.

abducible e429.

0.5::edge(4,29):- e429.

abducible e434.

0.5::edge(4,34):- e434.

abducible e439.

0.5::edge(4,39):- e439.

abducible e447.

0.5::edge(4,47):- e447.

abducible e449.

0.5::edge(4,49):- e449.

abducible e457.

0.5::edge(4,57):- e457.

abducible e468.

0.5::edge(4,68):- e468.

abducible e475.

0.5::edge(4,75):- e475.

abducible e496.

0.5::edge(4,96):- e496.

abducible e515.

0.5::edge(5,15):- e515.

abducible e520.

0.5::edge(5,20):- e520.

abducible e525.

0.5::edge(5,25):- e525.

abducible e619.

0.5::edge(6,19):- e619.

abducible e642.

0.5::edge(6,42):- e642.

abducible e663.

0.5::edge(6,63):- e663.

abducible e684.

0.5::edge(6,84):- e684.

abducible e78.

0.5::edge(7,8):- e78.

abducible e712.

0.5::edge(7,12):- e712.

abducible e716.

0.5::edge(7,16):- e716.

abducible e722.

0.5::edge(7,22):- e722.

abducible e742.

0.5::edge(7,42):- e742.

abducible e755.

0.5::edge(7,55):- e755.

abducible e779.

0.5::edge(7,79):- e779.

abducible e810.

0.5::edge(8,10):- e810.

abducible e821.

0.5::edge(8,21):- e821.

abducible e858.

0.5::edge(8,58):- e858.

abducible e873.

0.5::edge(8,73):- e873.

abducible e879.

0.5::edge(8,79):- e879.

abducible e930.

0.5::edge(9,30):- e930.

abducible e1012.

0.5::edge(10,12):- e1012.

abducible e1017.

0.5::edge(10,17):- e1017.

abducible e1022.

0.5::edge(10,22):- e1022.

abducible e1023.

0.5::edge(10,23):- e1023.

abducible e1050.

0.5::edge(10,50):- e1050.

abducible e1077.

0.5::edge(10,77):- e1077.

abducible e1116.

0.5::edge(11,16):- e1116.

abducible e1132.

0.5::edge(11,32):- e1132.

abducible e1134.

0.5::edge(11,34):- e1134.

abducible e1137.

0.5::edge(11,37):- e1137.

abducible e1139.

0.5::edge(11,39):- e1139.

abducible e1146.

0.5::edge(11,46):- e1146.

abducible e1148.

0.5::edge(11,48):- e1148.

abducible e1160.

0.5::edge(11,60):- e1160.

abducible e1165.

0.5::edge(11,65):- e1165.

abducible e1231.

0.5::edge(12,31):- e1231.

abducible e1344.

0.5::edge(13,44):- e1344.

abducible e1374.

0.5::edge(13,74):- e1374.

abducible e1383.

0.5::edge(13,83):- e1383.

abducible e1397.

0.5::edge(13,97):- e1397.

abducible e1415.

0.5::edge(14,15):- e1415.

abducible e1423.

0.5::edge(14,23):- e1423.

abducible e1448.

0.5::edge(14,48):- e1448.

abducible e1449.

0.5::edge(14,49):- e1449.

abducible e1467.

0.5::edge(14,67):- e1467.

abducible e1482.

0.5::edge(14,82):- e1482.

abducible e1517.

0.5::edge(15,17):- e1517.

abducible e1541.

0.5::edge(15,41):- e1541.

abducible e1551.

0.5::edge(15,51):- e1551.

abducible e1652.

0.5::edge(16,52):- e1652.

abducible e1656.

0.5::edge(16,56):- e1656.

abducible e1737.

0.5::edge(17,37):- e1737.

abducible e1743.

0.5::edge(17,43):- e1743.

abducible e1781.

0.5::edge(17,81):- e1781.

abducible e1795.

0.5::edge(17,95):- e1795.

abducible e1855.

0.5::edge(18,55):- e1855.

abducible e1878.

0.5::edge(18,78):- e1878.

abducible e1924.

0.5::edge(19,24):- e1924.

abducible e1932.

0.5::edge(19,32):- e1932.

abducible e1989.

0.5::edge(19,89):- e1989.

abducible e2040.

0.5::edge(20,40):- e2040.

abducible e2064.

0.5::edge(20,64):- e2064.

abducible e2128.

0.5::edge(21,28):- e2128.

abducible e2184.

0.5::edge(21,84):- e2184.

abducible e2189.

0.5::edge(21,89):- e2189.

abducible e2198.

0.5::edge(21,98):- e2198.

abducible e2324.

0.5::edge(23,24):- e2324.

abducible e2336.

0.5::edge(23,36):- e2336.

abducible e2366.

0.5::edge(23,66):- e2366.

abducible e2371.

0.5::edge(23,71):- e2371.

abducible e2392.

0.5::edge(23,92):- e2392.

abducible e2554.

0.5::edge(25,54):- e2554.

abducible e2563.

0.5::edge(25,63):- e2563.

abducible e2569.

0.5::edge(25,69):- e2569.

abducible e2572.

0.5::edge(25,72):- e2572.

abducible e2597.

0.5::edge(25,97):- e2597.

abducible e2635.

0.5::edge(26,35):- e2635.

abducible e2659.

0.5::edge(26,59):- e2659.

abducible e2678.

0.5::edge(26,78):- e2678.

abducible e2736.

0.5::edge(27,36):- e2736.

abducible e2753.

0.5::edge(27,53):- e2753.

abducible e2757.

0.5::edge(27,57):- e2757.

abducible e2799.

0.5::edge(27,99):- e2799.

abducible e2829.

0.5::edge(28,29):- e2829.

abducible e2833.

0.5::edge(28,33):- e2833.

abducible e2851.

0.5::edge(28,51):- e2851.

abducible e2896.

0.5::edge(28,96):- e2896.

abducible e2930.

0.5::edge(29,30):- e2930.

abducible e2961.

0.5::edge(29,61):- e2961.

abducible e2968.

0.5::edge(29,68):- e2968.

abducible e3073.

0.5::edge(30,73):- e3073.

abducible e3099.

0.5::edge(30,99):- e3099.

abducible e3147.

0.5::edge(31,47):- e3147.

abducible e3176.

0.5::edge(31,76):- e3176.

abducible e3238.

0.5::edge(32,38):- e3238.

abducible e3243.

0.5::edge(32,43):- e3243.

abducible e3280.

0.5::edge(32,80):- e3280.

abducible e3391.

0.5::edge(33,91):- e3391.

abducible e3486.

0.5::edge(34,86):- e3486.

abducible e3745.

0.5::edge(37,45):- e3745.

abducible e3769.

0.5::edge(37,69):- e3769.

abducible e3782.

0.5::edge(37,82):- e3782.

abducible e3786.

0.5::edge(37,86):- e3786.

abducible e3793.

0.5::edge(37,93):- e3793.

abducible e3854.

0.5::edge(38,54):- e3854.

abducible e3864.

0.5::edge(38,64):- e3864.

abducible e3877.

0.5::edge(38,77):- e3877.

abducible e3887.

0.5::edge(38,87):- e3887.

abducible e3987.

0.5::edge(39,87):- e3987.

abducible e4191.

0.5::edge(41,91):- e4191.

abducible e4445.

0.5::edge(44,45):- e4445.

abducible e4462.

0.5::edge(44,62):- e4462.

abducible e4467.

0.5::edge(44,67):- e4467.

abducible e4576.

0.5::edge(45,76):- e4576.

abducible e4952.

0.5::edge(49,52):- e4952.

abducible e4975.

0.5::edge(49,75):- e4975.

abducible e5894.

0.5::edge(58,94):- e5894.

abducible e6166.

0.5::edge(61,66):- e6166.

abducible e6588.

0.5::edge(65,88):- e6588.

abducible e6683.

0.5::edge(66,83):- e6683.

abducible e6690.

0.5::edge(66,90):- e6690.

abducible e7081.

0.5::edge(70,81):- e7081.

abducible e7685.

0.5::edge(76,85):- e7685.

abducible e8194.

0.5::edge(81,94):- e8194.

abducible e8492.

0.5::edge(84,92):- e8492.

abducible e8890.

0.5::edge(88,90):- e8890.



:- path(0, 99 ,L), L < 6.





ev :- path(0,99,_).



:- end_lpad.

run:-

	statistics(runtime, [Start | _]), 

	abd_prob(ev, Prob, Abd),

	statistics(runtime, [Stop | _]),

	Runtime is Stop - Start,

	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

