:- use_module(library(pita)).


:- pita.

:- begin_lpad.
% CP-theory for size 6

a0 :- a1.
abducible aba1.
0.5::a1:- aba1.
0.4999999975::a0;0.4999999975::a1:-a2.

abducible aba2.
0.5::a2:- aba2.
0.333333332222::a0;0.333333332222::a1;0.333333332222::a2:-a3.

abducible aba3.
0.5::a3:- aba3.
0.249999999375::a0;0.249999999375::a1;0.249999999375::a2;0.249999999375::a3:-a4.

abducible aba4.
0.5::a4:- aba4.
0.1999999996::a0;0.1999999996::a1;0.1999999996::a2;0.1999999996::a3;0.1999999996::a4:-a5.

abducible aba5.
0.5::a5:- aba5.
ev :- a0.

:- end_lpad.

run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

