:- use_module(library(pita)).


:- pita.

:- begin_lpad.
% CP-theory for size 3

a0 :- a1.
abducible aba1.
0.5::a1:- aba1.
0.4999999975::a0;0.4999999975::a1:-a2.

abducible aba2.
0.5::a2:- aba2.
ev :- a0.

:- end_lpad.

run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

