# Abduction datasets
This repository contains four datasets.

The following folders contain the programs used to compute the results with only abducibles (no integrity constraints):
```
- blood_abd: dataset for blood experiments
- gh_abd: dataset for growing head (gh) experiments
- gnb_abd: dataset for growing negated body (gnb) experiments
- graph_complete_abd: dataset for probabilistic complete graph experiments
- graphs_abd: dataset for probabilistic graph experiments, following Barabási-Albert model, generated with `GenBarAlbGraph.py`. In every subfolder there are 10 graphs with the number of edges equal to the name of the subfolder itself
```

The following folders contain the programs used to compute the results with abducibles and deterministic integrity constraint. 
They differ from the previous set of folders only by the fact that these contain integrity constraints. 
The structures of the programs are the same.
```
- blood_abd_ic
- gh_abd_ic
- gnb_abd_ic
- graph_complete_ic
- graphs_abd_ic
```

The following folders contain the programs used to compute the results with abducibles and probabilistic integrity constraint, with probability set to 0.5. 
They differ from the previous set of folders only by the fact that these contain probabilities attached to integrity constraints. 
The structures of the programs are the same.
```
- blood_abd_ic_prob
- gh_abd_ic_prob
- gnb_abd_ic_prob
- graph_complete_ic_prob
- graphs_abd_ic_prob
```

The online system is available at the following url: [http://cplint.eu](http://cplint.eu)

## Prerequisites
For a local installation, you need to install SWI Prolog ([https://www.swi-prolog.org/build/unix.html](https://www.swi-prolog.org/build/unix.html)) and then install the cplint package using the following commands:
```
swipl
?- pack_install(cplint).
```

You can also try these programs online at the following url: [http://cplint.eu](http://cplint.eu)