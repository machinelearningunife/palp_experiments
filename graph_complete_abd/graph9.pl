:- use_module(library(pita)).
:- pita.
:- begin_lpad.
path(A, B, N):-
path_(A, B, L),flatten(L, LF),length(LF, N).
path_(X, X, X).
path_(X, Y, [Y | T]):- path_(X, Z, T), edge(Z, Y).

0.5::edge(1,2):- e12.

abducible e12.

0.5::edge(1,3):- e13.

abducible e13.

0.5::edge(2,3):- e23.

abducible e23.

0.5::edge(1,4):- e14.

abducible e14.

0.5::edge(2,4):- e24.

abducible e24.

0.5::edge(3,4):- e34.

abducible e34.

0.5::edge(1,5):- e15.

abducible e15.

0.5::edge(2,5):- e25.

abducible e25.

0.5::edge(3,5):- e35.

abducible e35.

0.5::edge(4,5):- e45.

abducible e45.

0.5::edge(1,6):- e16.

abducible e16.

0.5::edge(2,6):- e26.

abducible e26.

0.5::edge(3,6):- e36.

abducible e36.

0.5::edge(4,6):- e46.

abducible e46.

0.5::edge(5,6):- e56.

abducible e56.

0.5::edge(1,7):- e17.

abducible e17.

0.5::edge(2,7):- e27.

abducible e27.

0.5::edge(3,7):- e37.

abducible e37.

0.5::edge(4,7):- e47.

abducible e47.

0.5::edge(5,7):- e57.

abducible e57.

0.5::edge(6,7):- e67.

abducible e67.

0.5::edge(1,8):- e18.

abducible e18.

0.5::edge(2,8):- e28.

abducible e28.

0.5::edge(3,8):- e38.

abducible e38.

0.5::edge(4,8):- e48.

abducible e48.

0.5::edge(5,8):- e58.

abducible e58.

0.5::edge(6,8):- e68.

abducible e68.

0.5::edge(7,8):- e78.

abducible e78.

0.5::edge(1,9):- e19.

abducible e19.

0.5::edge(2,9):- e29.

abducible e29.

0.5::edge(3,9):- e39.

abducible e39.

0.5::edge(4,9):- e49.

abducible e49.

0.5::edge(5,9):- e59.

abducible e59.

0.5::edge(6,9):- e69.

abducible e69.

0.5::edge(7,9):- e79.

abducible e79.

0.5::edge(8,9):- e89.

abducible e89.

ev:- path(1,9,_).
:- end_lpad.


run:- statistics(runtime, [Start | _]),abd_prob(ev, Prob, Abd),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).
