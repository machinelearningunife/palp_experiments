import networkx as nx

n_edges = 2

for i in range(50,101,10):
	for j in range(1,11):
		filename = "graphs_abd/" + str(i) + "/graph_" + str(j) + ".pl"
		filename_ic = "graphs_abd_ic/" + str(i) + "/graph_" + str(j) + ".pl"
		out_file = open(filename,"w")
		out_file_ic = open(filename_ic,"w")

		# out_file.write(":- use_module(library(pita)).\n:- pita.\n:- begin_lpad.\n\npath(X, X).\npath(X, Y):- path(X, Z), edge(Z, Y).\n\n")
		# out_file_ic.write(":- use_module(library(pita)).\n:- pita.\n:- begin_lpad.\n\npath(X, X).\npath(X, Y):- path(X, Z), edge(Z, Y).\n\n")
		out_file.write(":- use_module(library(pita)).\n:- pita.\n:- begin_lpad.\n\npath(A,B,N):-\npath_(A,B,L),\nflatten(L,LF),\nlength(LF,N).\n\npath_(X,X,X).\npath_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).\n")
		out_file_ic.write(":- use_module(library(pita)).\n:- pita.\n:- begin_lpad.\n\npath(A,B,N):-\npath_(A,B,L),\nflatten(L,LF),\nlength(LF,N).\n\npath_(X,X,X).\npath_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).\n")

		ba = nx.barabasi_albert_graph(int(i), int(n_edges)) 
		# print(ba.edges())
		for a in ba.edges:
			out_file.write("abducible e" + str(a[0]) + str(a[1]) + ".\n")
			out_file_ic.write("abducible e" + str(a[0]) + str(a[1]) + ".\n")
			out_file.write("0.5::edge(" + str(a[0]) + "," + str(a[1]) + "):- e" + str(a[0]) + str(a[1]) + ".\n")
			out_file_ic.write("0.5::edge(" + str(a[0]) + "," + str(a[1]) + "):- e" + str(a[0]) + str(a[1]) + ".\n")
		
		dest = i - 1

		# print("\n:- path(0,", str(i - 1), ",2).", file=out_file_ic)
		# print(":- path(0,", str(i - 1), ",3).", file=out_file_ic)
		# print(":- path(0,", str(i - 1), ",4).", file=out_file_ic)
		# print(":- path(0,", str(i - 1), ",5).\n", file=out_file_ic)
		print("\n:- path(0,", str(i - 1), ",L), L < 6.\n", file=out_file_ic)

		out_file.write("\nev :- path(0," + str(dest) + ",_).\n\n:- end_lpad.")
		out_file_ic.write("\nev :- path(0," + str(dest) + ",_).\n\n:- end_lpad.")
		out_file.write('\nrun:-\n\tstatistics(runtime, [Start | _]), \n\tabd_prob(ev, Prob, Abd),\n\tstatistics(runtime, [Stop | _]),\n\tRuntime is Stop - Start,\n\tformat(\'Prob ~w, Abd ~w: Runtime: ~w~n\', [Prob, Abd, Runtime]).\n')
		out_file_ic.write('\nrun:-\n\tstatistics(runtime, [Start | _]), \n\tabd_prob(ev, Prob, Abd),\n\tstatistics(runtime, [Stop | _]),\n\tRuntime is Stop - Start,\n\tformat(\'Prob ~w, Abd ~w: Runtime: ~w~n\', [Prob, Abd, Runtime]).\n')

		print("generated " + filename)
		out_file.close()
    
