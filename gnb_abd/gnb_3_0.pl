:- use_module(library(pita)).


:- pita.

:- begin_lpad.
% CP-theory for size 3

abducible aba0.
0.5::a0:- a1, aba0.

0.5::a0:- \+a1,a2, aba0.

abducible aba1.
0.5::a1:- a2, aba1.

abducible aba2.
0.5::a2:- aba2.

ev :- a0.

:- end_lpad.

run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

