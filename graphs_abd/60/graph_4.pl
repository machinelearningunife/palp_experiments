:- use_module(library(pita)).
:- pita.
:- begin_lpad.

path(A,B,N):-
path_(A,B,L),
flatten(L,LF),
length(LF,N).

path_(X,X,X).
path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).
abducible e02.
0.5::edge(0,2):- e02.
abducible e03.
0.5::edge(0,3):- e03.
abducible e04.
0.5::edge(0,4):- e04.
abducible e05.
0.5::edge(0,5):- e05.
abducible e010.
0.5::edge(0,10):- e010.
abducible e015.
0.5::edge(0,15):- e015.
abducible e032.
0.5::edge(0,32):- e032.
abducible e12.
0.5::edge(1,2):- e12.
abducible e14.
0.5::edge(1,4):- e14.
abducible e15.
0.5::edge(1,5):- e15.
abducible e135.
0.5::edge(1,35):- e135.
abducible e146.
0.5::edge(1,46):- e146.
abducible e149.
0.5::edge(1,49):- e149.
abducible e23.
0.5::edge(2,3):- e23.
abducible e215.
0.5::edge(2,15):- e215.
abducible e218.
0.5::edge(2,18):- e218.
abducible e229.
0.5::edge(2,29):- e229.
abducible e234.
0.5::edge(2,34):- e234.
abducible e241.
0.5::edge(2,41):- e241.
abducible e258.
0.5::edge(2,58):- e258.
abducible e317.
0.5::edge(3,17):- e317.
abducible e319.
0.5::edge(3,19):- e319.
abducible e321.
0.5::edge(3,21):- e321.
abducible e323.
0.5::edge(3,23):- e323.
abducible e324.
0.5::edge(3,24):- e324.
abducible e333.
0.5::edge(3,33):- e333.
abducible e356.
0.5::edge(3,56):- e356.
abducible e359.
0.5::edge(3,59):- e359.
abducible e46.
0.5::edge(4,6):- e46.
abducible e47.
0.5::edge(4,7):- e47.
abducible e48.
0.5::edge(4,8):- e48.
abducible e410.
0.5::edge(4,10):- e410.
abducible e417.
0.5::edge(4,17):- e417.
abducible e420.
0.5::edge(4,20):- e420.
abducible e423.
0.5::edge(4,23):- e423.
abducible e425.
0.5::edge(4,25):- e425.
abducible e426.
0.5::edge(4,26):- e426.
abducible e427.
0.5::edge(4,27):- e427.
abducible e428.
0.5::edge(4,28):- e428.
abducible e434.
0.5::edge(4,34):- e434.
abducible e435.
0.5::edge(4,35):- e435.
abducible e437.
0.5::edge(4,37):- e437.
abducible e439.
0.5::edge(4,39):- e439.
abducible e449.
0.5::edge(4,49):- e449.
abducible e451.
0.5::edge(4,51):- e451.
abducible e452.
0.5::edge(4,52):- e452.
abducible e455.
0.5::edge(4,55):- e455.
abducible e56.
0.5::edge(5,6):- e56.
abducible e57.
0.5::edge(5,7):- e57.
abducible e58.
0.5::edge(5,8):- e58.
abducible e59.
0.5::edge(5,9):- e59.
abducible e511.
0.5::edge(5,11):- e511.
abducible e514.
0.5::edge(5,14):- e514.
abducible e529.
0.5::edge(5,29):- e529.
abducible e533.
0.5::edge(5,33):- e533.
abducible e544.
0.5::edge(5,44):- e544.
abducible e548.
0.5::edge(5,48):- e548.
abducible e69.
0.5::edge(6,9):- e69.
abducible e613.
0.5::edge(6,13):- e613.
abducible e616.
0.5::edge(6,16):- e616.
abducible e622.
0.5::edge(6,22):- e622.
abducible e627.
0.5::edge(6,27):- e627.
abducible e651.
0.5::edge(6,51):- e651.
abducible e712.
0.5::edge(7,12):- e712.
abducible e854.
0.5::edge(8,54):- e854.
abducible e919.
0.5::edge(9,19):- e919.
abducible e1011.
0.5::edge(10,11):- e1011.
abducible e1012.
0.5::edge(10,12):- e1012.
abducible e1013.
0.5::edge(10,13):- e1013.
abducible e1014.
0.5::edge(10,14):- e1014.
abducible e1016.
0.5::edge(10,16):- e1016.
abducible e1020.
0.5::edge(10,20):- e1020.
abducible e1026.
0.5::edge(10,26):- e1026.
abducible e1050.
0.5::edge(10,50):- e1050.
abducible e1056.
0.5::edge(10,56):- e1056.
abducible e1125.
0.5::edge(11,25):- e1125.
abducible e1143.
0.5::edge(11,43):- e1143.
abducible e1337.
0.5::edge(13,37):- e1337.
abducible e1340.
0.5::edge(13,40):- e1340.
abducible e1418.
0.5::edge(14,18):- e1418.
abducible e1440.
0.5::edge(14,40):- e1440.
abducible e1522.
0.5::edge(15,22):- e1522.
abducible e1530.
0.5::edge(15,30):- e1530.
abducible e1531.
0.5::edge(15,31):- e1531.
abducible e1724.
0.5::edge(17,24):- e1724.
abducible e1828.
0.5::edge(18,28):- e1828.
abducible e1830.
0.5::edge(18,30):- e1830.
abducible e1831.
0.5::edge(18,31):- e1831.
abducible e1836.
0.5::edge(18,36):- e1836.
abducible e1845.
0.5::edge(18,45):- e1845.
abducible e1852.
0.5::edge(18,52):- e1852.
abducible e1854.
0.5::edge(18,54):- e1854.
abducible e1921.
0.5::edge(19,21):- e1921.
abducible e2053.
0.5::edge(20,53):- e2053.
abducible e2142.
0.5::edge(21,42):- e2142.
abducible e2145.
0.5::edge(21,45):- e2145.
abducible e2243.
0.5::edge(22,43):- e2243.
abducible e2253.
0.5::edge(22,53):- e2253.
abducible e2332.
0.5::edge(23,32):- e2332.
abducible e2347.
0.5::edge(23,47):- e2347.
abducible e2444.
0.5::edge(24,44):- e2444.
abducible e2536.
0.5::edge(25,36):- e2536.
abducible e2538.
0.5::edge(25,38):- e2538.
abducible e2747.
0.5::edge(27,47):- e2747.
abducible e2941.
0.5::edge(29,41):- e2941.
abducible e3038.
0.5::edge(30,38):- e3038.
abducible e3148.
0.5::edge(31,48):- e3148.
abducible e3357.
0.5::edge(33,57):- e3357.
abducible e3839.
0.5::edge(38,39):- e3839.
abducible e3858.
0.5::edge(38,58):- e3858.
abducible e4042.
0.5::edge(40,42):- e4042.
abducible e4046.
0.5::edge(40,46):- e4046.
abducible e4059.
0.5::edge(40,59):- e4059.
abducible e4650.
0.5::edge(46,50):- e4650.
abducible e4655.
0.5::edge(46,55):- e4655.
abducible e5657.
0.5::edge(56,57):- e5657.

ev :- path(0,59,_).

:- end_lpad.
run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).
