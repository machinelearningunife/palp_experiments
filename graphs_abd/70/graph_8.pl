:- use_module(library(pita)).
:- pita.
:- begin_lpad.

path(A,B,N):-
path_(A,B,L),
flatten(L,LF),
length(LF,N).

path_(X,X,X).
path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).
abducible e02.
0.5::edge(0,2):- e02.
abducible e04.
0.5::edge(0,4):- e04.
abducible e015.
0.5::edge(0,15):- e015.
abducible e022.
0.5::edge(0,22):- e022.
abducible e028.
0.5::edge(0,28):- e028.
abducible e029.
0.5::edge(0,29):- e029.
abducible e036.
0.5::edge(0,36):- e036.
abducible e12.
0.5::edge(1,2):- e12.
abducible e13.
0.5::edge(1,3):- e13.
abducible e14.
0.5::edge(1,4):- e14.
abducible e15.
0.5::edge(1,5):- e15.
abducible e16.
0.5::edge(1,6):- e16.
abducible e17.
0.5::edge(1,7):- e17.
abducible e18.
0.5::edge(1,8):- e18.
abducible e19.
0.5::edge(1,9):- e19.
abducible e110.
0.5::edge(1,10):- e110.
abducible e111.
0.5::edge(1,11):- e111.
abducible e113.
0.5::edge(1,13):- e113.
abducible e114.
0.5::edge(1,14):- e114.
abducible e117.
0.5::edge(1,17):- e117.
abducible e125.
0.5::edge(1,25):- e125.
abducible e127.
0.5::edge(1,27):- e127.
abducible e130.
0.5::edge(1,30):- e130.
abducible e132.
0.5::edge(1,32):- e132.
abducible e133.
0.5::edge(1,33):- e133.
abducible e135.
0.5::edge(1,35):- e135.
abducible e139.
0.5::edge(1,39):- e139.
abducible e142.
0.5::edge(1,42):- e142.
abducible e143.
0.5::edge(1,43):- e143.
abducible e145.
0.5::edge(1,45):- e145.
abducible e146.
0.5::edge(1,46):- e146.
abducible e158.
0.5::edge(1,58):- e158.
abducible e159.
0.5::edge(1,59):- e159.
abducible e162.
0.5::edge(1,62):- e162.
abducible e23.
0.5::edge(2,3):- e23.
abducible e27.
0.5::edge(2,7):- e27.
abducible e28.
0.5::edge(2,8):- e28.
abducible e211.
0.5::edge(2,11):- e211.
abducible e213.
0.5::edge(2,13):- e213.
abducible e219.
0.5::edge(2,19):- e219.
abducible e220.
0.5::edge(2,20):- e220.
abducible e226.
0.5::edge(2,26):- e226.
abducible e233.
0.5::edge(2,33):- e233.
abducible e236.
0.5::edge(2,36):- e236.
abducible e241.
0.5::edge(2,41):- e241.
abducible e244.
0.5::edge(2,44):- e244.
abducible e260.
0.5::edge(2,60):- e260.
abducible e39.
0.5::edge(3,9):- e39.
abducible e310.
0.5::edge(3,10):- e310.
abducible e312.
0.5::edge(3,12):- e312.
abducible e316.
0.5::edge(3,16):- e316.
abducible e321.
0.5::edge(3,21):- e321.
abducible e324.
0.5::edge(3,24):- e324.
abducible e325.
0.5::edge(3,25):- e325.
abducible e326.
0.5::edge(3,26):- e326.
abducible e330.
0.5::edge(3,30):- e330.
abducible e332.
0.5::edge(3,32):- e332.
abducible e353.
0.5::edge(3,53):- e353.
abducible e364.
0.5::edge(3,64):- e364.
abducible e365.
0.5::edge(3,65):- e365.
abducible e45.
0.5::edge(4,5):- e45.
abducible e422.
0.5::edge(4,22):- e422.
abducible e440.
0.5::edge(4,40):- e440.
abducible e454.
0.5::edge(4,54):- e454.
abducible e455.
0.5::edge(4,55):- e455.
abducible e469.
0.5::edge(4,69):- e469.
abducible e56.
0.5::edge(5,6):- e56.
abducible e515.
0.5::edge(5,15):- e515.
abducible e529.
0.5::edge(5,29):- e529.
abducible e540.
0.5::edge(5,40):- e540.
abducible e541.
0.5::edge(5,41):- e541.
abducible e627.
0.5::edge(6,27):- e627.
abducible e658.
0.5::edge(6,58):- e658.
abducible e728.
0.5::edge(7,28):- e728.
abducible e757.
0.5::edge(7,57):- e757.
abducible e766.
0.5::edge(7,66):- e766.
abducible e844.
0.5::edge(8,44):- e844.
abducible e912.
0.5::edge(9,12):- e912.
abducible e918.
0.5::edge(9,18):- e918.
abducible e919.
0.5::edge(9,19):- e919.
abducible e949.
0.5::edge(9,49):- e949.
abducible e1017.
0.5::edge(10,17):- e1017.
abducible e1269.
0.5::edge(12,69):- e1269.
abducible e1314.
0.5::edge(13,14):- e1314.
abducible e1316.
0.5::edge(13,16):- e1316.
abducible e1318.
0.5::edge(13,18):- e1318.
abducible e1334.
0.5::edge(13,34):- e1334.
abducible e1363.
0.5::edge(13,63):- e1363.
abducible e1438.
0.5::edge(14,38):- e1438.
abducible e1439.
0.5::edge(14,39):- e1439.
abducible e1445.
0.5::edge(14,45):- e1445.
abducible e1447.
0.5::edge(14,47):- e1447.
abducible e1449.
0.5::edge(14,49):- e1449.
abducible e1451.
0.5::edge(14,51):- e1451.
abducible e1464.
0.5::edge(14,64):- e1464.
abducible e1467.
0.5::edge(14,67):- e1467.
abducible e1531.
0.5::edge(15,31):- e1531.
abducible e1623.
0.5::edge(16,23):- e1623.
abducible e1721.
0.5::edge(17,21):- e1721.
abducible e1920.
0.5::edge(19,20):- e1920.
abducible e1923.
0.5::edge(19,23):- e1923.
abducible e1943.
0.5::edge(19,43):- e1943.
abducible e2047.
0.5::edge(20,47):- e2047.
abducible e2155.
0.5::edge(21,55):- e2155.
abducible e2224.
0.5::edge(22,24):- e2224.
abducible e2457.
0.5::edge(24,57):- e2457.
abducible e2461.
0.5::edge(24,61):- e2461.
abducible e2537.
0.5::edge(25,37):- e2537.
abducible e2538.
0.5::edge(25,38):- e2538.
abducible e2634.
0.5::edge(26,34):- e2634.
abducible e2642.
0.5::edge(26,42):- e2642.
abducible e2731.
0.5::edge(27,31):- e2731.
abducible e2853.
0.5::edge(28,53):- e2853.
abducible e2854.
0.5::edge(28,54):- e2854.
abducible e2868.
0.5::edge(28,68):- e2868.
abducible e2935.
0.5::edge(29,35):- e2935.
abducible e2960.
0.5::edge(29,60):- e2960.
abducible e3037.
0.5::edge(30,37):- e3037.
abducible e3148.
0.5::edge(31,48):- e3148.
abducible e3461.
0.5::edge(34,61):- e3461.
abducible e3550.
0.5::edge(35,50):- e3550.
abducible e3551.
0.5::edge(35,51):- e3551.
abducible e3659.
0.5::edge(36,59):- e3659.
abducible e3765.
0.5::edge(37,65):- e3765.
abducible e3848.
0.5::edge(38,48):- e3848.
abducible e3950.
0.5::edge(39,50):- e3950.
abducible e4063.
0.5::edge(40,63):- e4063.
abducible e4067.
0.5::edge(40,67):- e4067.
abducible e4156.
0.5::edge(41,56):- e4156.
abducible e4446.
0.5::edge(44,46):- e4446.
abducible e4456.
0.5::edge(44,56):- e4456.
abducible e4462.
0.5::edge(44,62):- e4462.
abducible e4466.
0.5::edge(44,66):- e4466.
abducible e4652.
0.5::edge(46,52):- e4652.
abducible e4752.
0.5::edge(47,52):- e4752.
abducible e5368.
0.5::edge(53,68):- e5368.

ev :- path(0,69,_).

:- end_lpad.
run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).
