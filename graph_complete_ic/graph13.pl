:- use_module(library(pita)).
:- pita.
:- begin_lpad.
path(A, B, N):-
path_(A, B, L),flatten(L, LF),length(LF, N).
path_(X, X, X).
path_(X, Y, [Y | T]):- path_(X, Z, T), edge(Z, Y).

0.5::edge(1,2):- e12.

abducible e12.

0.5::edge(1,3):- e13.

abducible e13.

0.5::edge(2,3):- e23.

abducible e23.

0.5::edge(1,4):- e14.

abducible e14.

0.5::edge(2,4):- e24.

abducible e24.

0.5::edge(3,4):- e34.

abducible e34.

0.5::edge(1,5):- e15.

abducible e15.

0.5::edge(2,5):- e25.

abducible e25.

0.5::edge(3,5):- e35.

abducible e35.

0.5::edge(4,5):- e45.

abducible e45.

0.5::edge(1,6):- e16.

abducible e16.

0.5::edge(2,6):- e26.

abducible e26.

0.5::edge(3,6):- e36.

abducible e36.

0.5::edge(4,6):- e46.

abducible e46.

0.5::edge(5,6):- e56.

abducible e56.

0.5::edge(1,7):- e17.

abducible e17.

0.5::edge(2,7):- e27.

abducible e27.

0.5::edge(3,7):- e37.

abducible e37.

0.5::edge(4,7):- e47.

abducible e47.

0.5::edge(5,7):- e57.

abducible e57.

0.5::edge(6,7):- e67.

abducible e67.

0.5::edge(1,8):- e18.

abducible e18.

0.5::edge(2,8):- e28.

abducible e28.

0.5::edge(3,8):- e38.

abducible e38.

0.5::edge(4,8):- e48.

abducible e48.

0.5::edge(5,8):- e58.

abducible e58.

0.5::edge(6,8):- e68.

abducible e68.

0.5::edge(7,8):- e78.

abducible e78.

0.5::edge(1,9):- e19.

abducible e19.

0.5::edge(2,9):- e29.

abducible e29.

0.5::edge(3,9):- e39.

abducible e39.

0.5::edge(4,9):- e49.

abducible e49.

0.5::edge(5,9):- e59.

abducible e59.

0.5::edge(6,9):- e69.

abducible e69.

0.5::edge(7,9):- e79.

abducible e79.

0.5::edge(8,9):- e89.

abducible e89.

0.5::edge(1,10):- e110.

abducible e110.

0.5::edge(2,10):- e210.

abducible e210.

0.5::edge(3,10):- e310.

abducible e310.

0.5::edge(4,10):- e410.

abducible e410.

0.5::edge(5,10):- e510.

abducible e510.

0.5::edge(6,10):- e610.

abducible e610.

0.5::edge(7,10):- e710.

abducible e710.

0.5::edge(8,10):- e810.

abducible e810.

0.5::edge(9,10):- e910.

abducible e910.

0.5::edge(1,11):- e111.

abducible e111.

0.5::edge(2,11):- e211.

abducible e211.

0.5::edge(3,11):- e311.

abducible e311.

0.5::edge(4,11):- e411.

abducible e411.

0.5::edge(5,11):- e511.

abducible e511.

0.5::edge(6,11):- e611.

abducible e611.

0.5::edge(7,11):- e711.

abducible e711.

0.5::edge(8,11):- e811.

abducible e811.

0.5::edge(9,11):- e911.

abducible e911.

0.5::edge(10,11):- e1011.

abducible e1011.

0.5::edge(1,12):- e112.

abducible e112.

0.5::edge(2,12):- e212.

abducible e212.

0.5::edge(3,12):- e312.

abducible e312.

0.5::edge(4,12):- e412.

abducible e412.

0.5::edge(5,12):- e512.

abducible e512.

0.5::edge(6,12):- e612.

abducible e612.

0.5::edge(7,12):- e712.

abducible e712.

0.5::edge(8,12):- e812.

abducible e812.

0.5::edge(9,12):- e912.

abducible e912.

0.5::edge(10,12):- e1012.

abducible e1012.

0.5::edge(11,12):- e1112.

abducible e1112.

0.5::edge(1,13):- e113.

abducible e113.

0.5::edge(2,13):- e213.

abducible e213.

0.5::edge(3,13):- e313.

abducible e313.

0.5::edge(4,13):- e413.

abducible e413.

0.5::edge(5,13):- e513.

abducible e513.

0.5::edge(6,13):- e613.

abducible e613.

0.5::edge(7,13):- e713.

abducible e713.

0.5::edge(8,13):- e813.

abducible e813.

0.5::edge(9,13):- e913.

abducible e913.

0.5::edge(10,13):- e1013.

abducible e1013.

0.5::edge(11,13):- e1113.

abducible e1113.

0.5::edge(12,13):- e1213.

abducible e1213.

ev:- path(1,13,_).
:- path(1,13,2).
:- path(1,13,3).
:-path(1,13,4).
:- end_lpad.


run:- statistics(runtime, [Start | _]),abd_prob(ev, Prob, Abd),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).
