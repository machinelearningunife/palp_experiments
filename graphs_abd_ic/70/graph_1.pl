:- use_module(library(pita)).
:- pita.
:- begin_lpad.

path(A,B,N):-
path_(A,B,L),
flatten(L,LF),
length(LF,N).

path_(X,X,X).
path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).
abducible e02.
0.5::edge(0,2):- e02.
abducible e03.
0.5::edge(0,3):- e03.
abducible e06.
0.5::edge(0,6):- e06.
abducible e08.
0.5::edge(0,8):- e08.
abducible e010.
0.5::edge(0,10):- e010.
abducible e013.
0.5::edge(0,13):- e013.
abducible e014.
0.5::edge(0,14):- e014.
abducible e017.
0.5::edge(0,17):- e017.
abducible e020.
0.5::edge(0,20):- e020.
abducible e030.
0.5::edge(0,30):- e030.
abducible e037.
0.5::edge(0,37):- e037.
abducible e049.
0.5::edge(0,49):- e049.
abducible e050.
0.5::edge(0,50):- e050.
abducible e051.
0.5::edge(0,51):- e051.
abducible e055.
0.5::edge(0,55):- e055.
abducible e12.
0.5::edge(1,2):- e12.
abducible e18.
0.5::edge(1,8):- e18.
abducible e161.
0.5::edge(1,61):- e161.
abducible e23.
0.5::edge(2,3):- e23.
abducible e24.
0.5::edge(2,4):- e24.
abducible e25.
0.5::edge(2,5):- e25.
abducible e214.
0.5::edge(2,14):- e214.
abducible e215.
0.5::edge(2,15):- e215.
abducible e225.
0.5::edge(2,25):- e225.
abducible e235.
0.5::edge(2,35):- e235.
abducible e249.
0.5::edge(2,49):- e249.
abducible e34.
0.5::edge(3,4):- e34.
abducible e39.
0.5::edge(3,9):- e39.
abducible e323.
0.5::edge(3,23):- e323.
abducible e326.
0.5::edge(3,26):- e326.
abducible e330.
0.5::edge(3,30):- e330.
abducible e333.
0.5::edge(3,33):- e333.
abducible e334.
0.5::edge(3,34):- e334.
abducible e339.
0.5::edge(3,39):- e339.
abducible e345.
0.5::edge(3,45):- e345.
abducible e346.
0.5::edge(3,46):- e346.
abducible e357.
0.5::edge(3,57):- e357.
abducible e360.
0.5::edge(3,60):- e360.
abducible e363.
0.5::edge(3,63):- e363.
abducible e45.
0.5::edge(4,5):- e45.
abducible e47.
0.5::edge(4,7):- e47.
abducible e436.
0.5::edge(4,36):- e436.
abducible e441.
0.5::edge(4,41):- e441.
abducible e445.
0.5::edge(4,45):- e445.
abducible e463.
0.5::edge(4,63):- e463.
abducible e56.
0.5::edge(5,6):- e56.
abducible e57.
0.5::edge(5,7):- e57.
abducible e59.
0.5::edge(5,9):- e59.
abducible e510.
0.5::edge(5,10):- e510.
abducible e511.
0.5::edge(5,11):- e511.
abducible e513.
0.5::edge(5,13):- e513.
abducible e519.
0.5::edge(5,19):- e519.
abducible e520.
0.5::edge(5,20):- e520.
abducible e522.
0.5::edge(5,22):- e522.
abducible e527.
0.5::edge(5,27):- e527.
abducible e528.
0.5::edge(5,28):- e528.
abducible e529.
0.5::edge(5,29):- e529.
abducible e538.
0.5::edge(5,38):- e538.
abducible e553.
0.5::edge(5,53):- e553.
abducible e560.
0.5::edge(5,60):- e560.
abducible e565.
0.5::edge(5,65):- e565.
abducible e568.
0.5::edge(5,68):- e568.
abducible e612.
0.5::edge(6,12):- e612.
abducible e711.
0.5::edge(7,11):- e711.
abducible e721.
0.5::edge(7,21):- e721.
abducible e918.
0.5::edge(9,18):- e918.
abducible e929.
0.5::edge(9,29):- e929.
abducible e934.
0.5::edge(9,34):- e934.
abducible e946.
0.5::edge(9,46):- e946.
abducible e1012.
0.5::edge(10,12):- e1012.
abducible e1016.
0.5::edge(10,16):- e1016.
abducible e1017.
0.5::edge(10,17):- e1017.
abducible e1018.
0.5::edge(10,18):- e1018.
abducible e1024.
0.5::edge(10,24):- e1024.
abducible e1025.
0.5::edge(10,25):- e1025.
abducible e1032.
0.5::edge(10,32):- e1032.
abducible e1052.
0.5::edge(10,52):- e1052.
abducible e1064.
0.5::edge(10,64):- e1064.
abducible e1066.
0.5::edge(10,66):- e1066.
abducible e1142.
0.5::edge(11,42):- e1142.
abducible e1147.
0.5::edge(11,47):- e1147.
abducible e1315.
0.5::edge(13,15):- e1315.
abducible e1364.
0.5::edge(13,64):- e1364.
abducible e1369.
0.5::edge(13,69):- e1369.
abducible e1424.
0.5::edge(14,24):- e1424.
abducible e1426.
0.5::edge(14,26):- e1426.
abducible e1443.
0.5::edge(14,43):- e1443.
abducible e1444.
0.5::edge(14,44):- e1444.
abducible e1465.
0.5::edge(14,65):- e1465.
abducible e1516.
0.5::edge(15,16):- e1516.
abducible e1519.
0.5::edge(15,19):- e1519.
abducible e1522.
0.5::edge(15,22):- e1522.
abducible e1721.
0.5::edge(17,21):- e1721.
abducible e1728.
0.5::edge(17,28):- e1728.
abducible e1754.
0.5::edge(17,54):- e1754.
abducible e1756.
0.5::edge(17,56):- e1756.
abducible e1851.
0.5::edge(18,51):- e1851.
abducible e1855.
0.5::edge(18,55):- e1855.
abducible e2023.
0.5::edge(20,23):- e2023.
abducible e2031.
0.5::edge(20,31):- e2031.
abducible e2037.
0.5::edge(20,37):- e2037.
abducible e2038.
0.5::edge(20,38):- e2038.
abducible e2040.
0.5::edge(20,40):- e2040.
abducible e2044.
0.5::edge(20,44):- e2044.
abducible e2062.
0.5::edge(20,62):- e2062.
abducible e2131.
0.5::edge(21,31):- e2131.
abducible e2139.
0.5::edge(21,39):- e2139.
abducible e2147.
0.5::edge(21,47):- e2147.
abducible e2232.
0.5::edge(22,32):- e2232.
abducible e2235.
0.5::edge(22,35):- e2235.
abducible e2240.
0.5::edge(22,40):- e2240.
abducible e2243.
0.5::edge(22,43):- e2243.
abducible e2262.
0.5::edge(22,62):- e2262.
abducible e2267.
0.5::edge(22,67):- e2267.
abducible e2327.
0.5::edge(23,27):- e2327.
abducible e2548.
0.5::edge(25,48):- e2548.
abducible e2633.
0.5::edge(26,33):- e2633.
abducible e2648.
0.5::edge(26,48):- e2648.
abducible e2659.
0.5::edge(26,59):- e2659.
abducible e2842.
0.5::edge(28,42):- e2842.
abducible e2858.
0.5::edge(28,58):- e2858.
abducible e2861.
0.5::edge(28,61):- e2861.
abducible e3141.
0.5::edge(31,41):- e3141.
abducible e3152.
0.5::edge(31,52):- e3152.
abducible e3436.
0.5::edge(34,36):- e3436.
abducible e3756.
0.5::edge(37,56):- e3756.
abducible e3950.
0.5::edge(39,50):- e3950.
abducible e3953.
0.5::edge(39,53):- e3953.
abducible e3954.
0.5::edge(39,54):- e3954.
abducible e3959.
0.5::edge(39,59):- e3959.
abducible e4968.
0.5::edge(49,68):- e4968.
abducible e5058.
0.5::edge(50,58):- e5058.
abducible e5257.
0.5::edge(52,57):- e5257.
abducible e5966.
0.5::edge(59,66):- e5966.
abducible e6269.
0.5::edge(62,69):- e6269.
abducible e6467.
0.5::edge(64,67):- e6467.

:- path(0, 69 ,L), L < 6.


ev :- path(0,69,_).

:- end_lpad.
run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).
