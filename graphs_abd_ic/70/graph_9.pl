:- use_module(library(pita)).
:- pita.
:- begin_lpad.

path(A,B,N):-
path_(A,B,L),
flatten(L,LF),
length(LF,N).

path_(X,X,X).
path_(X,Y,[Y|T]):- path_(X,Z,T), edge(Z,Y).
abducible e02.
0.5::edge(0,2):- e02.
abducible e03.
0.5::edge(0,3):- e03.
abducible e04.
0.5::edge(0,4):- e04.
abducible e06.
0.5::edge(0,6):- e06.
abducible e09.
0.5::edge(0,9):- e09.
abducible e015.
0.5::edge(0,15):- e015.
abducible e021.
0.5::edge(0,21):- e021.
abducible e025.
0.5::edge(0,25):- e025.
abducible e028.
0.5::edge(0,28):- e028.
abducible e029.
0.5::edge(0,29):- e029.
abducible e030.
0.5::edge(0,30):- e030.
abducible e032.
0.5::edge(0,32):- e032.
abducible e035.
0.5::edge(0,35):- e035.
abducible e046.
0.5::edge(0,46):- e046.
abducible e050.
0.5::edge(0,50):- e050.
abducible e053.
0.5::edge(0,53):- e053.
abducible e058.
0.5::edge(0,58):- e058.
abducible e062.
0.5::edge(0,62):- e062.
abducible e12.
0.5::edge(1,2):- e12.
abducible e14.
0.5::edge(1,4):- e14.
abducible e15.
0.5::edge(1,5):- e15.
abducible e18.
0.5::edge(1,8):- e18.
abducible e110.
0.5::edge(1,10):- e110.
abducible e124.
0.5::edge(1,24):- e124.
abducible e133.
0.5::edge(1,33):- e133.
abducible e143.
0.5::edge(1,43):- e143.
abducible e147.
0.5::edge(1,47):- e147.
abducible e154.
0.5::edge(1,54):- e154.
abducible e156.
0.5::edge(1,56):- e156.
abducible e23.
0.5::edge(2,3):- e23.
abducible e26.
0.5::edge(2,6):- e26.
abducible e27.
0.5::edge(2,7):- e27.
abducible e28.
0.5::edge(2,8):- e28.
abducible e216.
0.5::edge(2,16):- e216.
abducible e217.
0.5::edge(2,17):- e217.
abducible e218.
0.5::edge(2,18):- e218.
abducible e227.
0.5::edge(2,27):- e227.
abducible e229.
0.5::edge(2,29):- e229.
abducible e235.
0.5::edge(2,35):- e235.
abducible e237.
0.5::edge(2,37):- e237.
abducible e241.
0.5::edge(2,41):- e241.
abducible e245.
0.5::edge(2,45):- e245.
abducible e248.
0.5::edge(2,48):- e248.
abducible e251.
0.5::edge(2,51):- e251.
abducible e252.
0.5::edge(2,52):- e252.
abducible e266.
0.5::edge(2,66):- e266.
abducible e35.
0.5::edge(3,5):- e35.
abducible e320.
0.5::edge(3,20):- e320.
abducible e322.
0.5::edge(3,22):- e322.
abducible e49.
0.5::edge(4,9):- e49.
abducible e410.
0.5::edge(4,10):- e410.
abducible e412.
0.5::edge(4,12):- e412.
abducible e413.
0.5::edge(4,13):- e413.
abducible e418.
0.5::edge(4,18):- e418.
abducible e420.
0.5::edge(4,20):- e420.
abducible e423.
0.5::edge(4,23):- e423.
abducible e425.
0.5::edge(4,25):- e425.
abducible e426.
0.5::edge(4,26):- e426.
abducible e431.
0.5::edge(4,31):- e431.
abducible e440.
0.5::edge(4,40):- e440.
abducible e447.
0.5::edge(4,47):- e447.
abducible e448.
0.5::edge(4,48):- e448.
abducible e449.
0.5::edge(4,49):- e449.
abducible e452.
0.5::edge(4,52):- e452.
abducible e462.
0.5::edge(4,62):- e462.
abducible e467.
0.5::edge(4,67):- e467.
abducible e57.
0.5::edge(5,7):- e57.
abducible e512.
0.5::edge(5,12):- e512.
abducible e514.
0.5::edge(5,14):- e514.
abducible e519.
0.5::edge(5,19):- e519.
abducible e531.
0.5::edge(5,31):- e531.
abducible e533.
0.5::edge(5,33):- e533.
abducible e537.
0.5::edge(5,37):- e537.
abducible e539.
0.5::edge(5,39):- e539.
abducible e559.
0.5::edge(5,59):- e559.
abducible e560.
0.5::edge(5,60):- e560.
abducible e568.
0.5::edge(5,68):- e568.
abducible e711.
0.5::edge(7,11):- e711.
abducible e753.
0.5::edge(7,53):- e753.
abducible e755.
0.5::edge(7,55):- e755.
abducible e811.
0.5::edge(8,11):- e811.
abducible e817.
0.5::edge(8,17):- e817.
abducible e823.
0.5::edge(8,23):- e823.
abducible e914.
0.5::edge(9,14):- e914.
abducible e964.
0.5::edge(9,64):- e964.
abducible e1032.
0.5::edge(10,32):- e1032.
abducible e1213.
0.5::edge(12,13):- e1213.
abducible e1221.
0.5::edge(12,21):- e1221.
abducible e1243.
0.5::edge(12,43):- e1243.
abducible e1245.
0.5::edge(12,45):- e1245.
abducible e1327.
0.5::edge(13,27):- e1327.
abducible e1328.
0.5::edge(13,28):- e1328.
abducible e1415.
0.5::edge(14,15):- e1415.
abducible e1416.
0.5::edge(14,16):- e1416.
abducible e1450.
0.5::edge(14,50):- e1450.
abducible e1457.
0.5::edge(14,57):- e1457.
abducible e1469.
0.5::edge(14,69):- e1469.
abducible e1519.
0.5::edge(15,19):- e1519.
abducible e1522.
0.5::edge(15,22):- e1522.
abducible e1534.
0.5::edge(15,34):- e1534.
abducible e1536.
0.5::edge(15,36):- e1536.
abducible e1538.
0.5::edge(15,38):- e1538.
abducible e1546.
0.5::edge(15,46):- e1546.
abducible e1557.
0.5::edge(15,57):- e1557.
abducible e1656.
0.5::edge(16,56):- e1656.
abducible e1724.
0.5::edge(17,24):- e1724.
abducible e1726.
0.5::edge(17,26):- e1726.
abducible e1738.
0.5::edge(17,38):- e1738.
abducible e1760.
0.5::edge(17,60):- e1760.
abducible e1861.
0.5::edge(18,61):- e1861.
abducible e2036.
0.5::edge(20,36):- e2036.
abducible e2039.
0.5::edge(20,39):- e2039.
abducible e2049.
0.5::edge(20,49):- e2049.
abducible e2051.
0.5::edge(20,51):- e2051.
abducible e2130.
0.5::edge(21,30):- e2130.
abducible e2134.
0.5::edge(21,34):- e2134.
abducible e2440.
0.5::edge(24,40):- e2440.
abducible e2544.
0.5::edge(25,44):- e2544.
abducible e2861.
0.5::edge(28,61):- e2861.
abducible e2942.
0.5::edge(29,42):- e2942.
abducible e2963.
0.5::edge(29,63):- e2963.
abducible e3041.
0.5::edge(30,41):- e3041.
abducible e3168.
0.5::edge(31,68):- e3168.
abducible e3254.
0.5::edge(32,54):- e3254.
abducible e3265.
0.5::edge(32,65):- e3265.
abducible e3344.
0.5::edge(33,44):- e3344.
abducible e3455.
0.5::edge(34,55):- e3455.
abducible e3842.
0.5::edge(38,42):- e3842.
abducible e3959.
0.5::edge(39,59):- e3959.
abducible e3967.
0.5::edge(39,67):- e3967.
abducible e4358.
0.5::edge(43,58):- e4358.
abducible e4365.
0.5::edge(43,65):- e4365.
abducible e4666.
0.5::edge(46,66):- e4666.
abducible e6163.
0.5::edge(61,63):- e6163.
abducible e6169.
0.5::edge(61,69):- e6169.
abducible e6364.
0.5::edge(63,64):- e6364.

:- path(0, 69 ,L), L < 6.


ev :- path(0,69,_).

:- end_lpad.
run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).
