:- use_module(library(pita)).


:- pita.

:- begin_lpad.
% CP-theory for size 7

abducible aba0.
0.5::a0:- a1, aba0.

0.5::a0:- \+a1,a2, aba0.

0.5::a0:- \+a1,\+a2,a3, aba0.

0.5::a0:- \+a1,\+a2,\+a3,a4, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,a5, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,a6, aba0.

abducible aba1.
0.5::a1:- a2, aba1.

0.5::a1:- \+a2,a3, aba1.

0.5::a1:- \+a2,\+a3,a4, aba1.

0.5::a1:- \+a2,\+a3,\+a4,a5, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,a6, aba1.

abducible aba2.
0.5::a2:- a3, aba2.

0.5::a2:- \+a3,a4, aba2.

0.5::a2:- \+a3,\+a4,a5, aba2.

0.5::a2:- \+a3,\+a4,\+a5,a6, aba2.

abducible aba3.
0.5::a3:- a4, aba3.

0.5::a3:- \+a4,a5, aba3.

0.5::a3:- \+a4,\+a5,a6, aba3.

abducible aba4.
0.5::a4:- a5, aba4.

0.5::a4:- \+a5,a6, aba4.

abducible aba5.
0.5::a5:- a6, aba5.

abducible aba6.
0.5::a6:- aba6.

0.5:- r. 

r:- \+aba0.
r:- \+aba1.
r:- \+aba2.
r:- \+aba3.
r:- \+aba4.
r:- \+aba5.
r:- \+aba6.


ev :- a0.

:- end_lpad.

run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

