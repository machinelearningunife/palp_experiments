:- use_module(library(pita)).


:- pita.

:- begin_lpad.
% CP-theory for size 13

abducible aba0.
0.5::a0:- a1, aba0.

0.5::a0:- \+a1,a2, aba0.

0.5::a0:- \+a1,\+a2,a3, aba0.

0.5::a0:- \+a1,\+a2,\+a3,a4, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,a5, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,a6, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,\+a6,a7, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,\+a6,\+a7,a8, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,a9, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,a10, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,a11, aba0.

0.5::a0:- \+a1,\+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,\+a11,a12, aba0.

abducible aba1.
0.5::a1:- a2, aba1.

0.5::a1:- \+a2,a3, aba1.

0.5::a1:- \+a2,\+a3,a4, aba1.

0.5::a1:- \+a2,\+a3,\+a4,a5, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,a6, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,\+a6,a7, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,\+a6,\+a7,a8, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,a9, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,a10, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,a11, aba1.

0.5::a1:- \+a2,\+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,\+a11,a12, aba1.

abducible aba2.
0.5::a2:- a3, aba2.

0.5::a2:- \+a3,a4, aba2.

0.5::a2:- \+a3,\+a4,a5, aba2.

0.5::a2:- \+a3,\+a4,\+a5,a6, aba2.

0.5::a2:- \+a3,\+a4,\+a5,\+a6,a7, aba2.

0.5::a2:- \+a3,\+a4,\+a5,\+a6,\+a7,a8, aba2.

0.5::a2:- \+a3,\+a4,\+a5,\+a6,\+a7,\+a8,a9, aba2.

0.5::a2:- \+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,a10, aba2.

0.5::a2:- \+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,a11, aba2.

0.5::a2:- \+a3,\+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,\+a11,a12, aba2.

abducible aba3.
0.5::a3:- a4, aba3.

0.5::a3:- \+a4,a5, aba3.

0.5::a3:- \+a4,\+a5,a6, aba3.

0.5::a3:- \+a4,\+a5,\+a6,a7, aba3.

0.5::a3:- \+a4,\+a5,\+a6,\+a7,a8, aba3.

0.5::a3:- \+a4,\+a5,\+a6,\+a7,\+a8,a9, aba3.

0.5::a3:- \+a4,\+a5,\+a6,\+a7,\+a8,\+a9,a10, aba3.

0.5::a3:- \+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,a11, aba3.

0.5::a3:- \+a4,\+a5,\+a6,\+a7,\+a8,\+a9,\+a10,\+a11,a12, aba3.

abducible aba4.
0.5::a4:- a5, aba4.

0.5::a4:- \+a5,a6, aba4.

0.5::a4:- \+a5,\+a6,a7, aba4.

0.5::a4:- \+a5,\+a6,\+a7,a8, aba4.

0.5::a4:- \+a5,\+a6,\+a7,\+a8,a9, aba4.

0.5::a4:- \+a5,\+a6,\+a7,\+a8,\+a9,a10, aba4.

0.5::a4:- \+a5,\+a6,\+a7,\+a8,\+a9,\+a10,a11, aba4.

0.5::a4:- \+a5,\+a6,\+a7,\+a8,\+a9,\+a10,\+a11,a12, aba4.

abducible aba5.
0.5::a5:- a6, aba5.

0.5::a5:- \+a6,a7, aba5.

0.5::a5:- \+a6,\+a7,a8, aba5.

0.5::a5:- \+a6,\+a7,\+a8,a9, aba5.

0.5::a5:- \+a6,\+a7,\+a8,\+a9,a10, aba5.

0.5::a5:- \+a6,\+a7,\+a8,\+a9,\+a10,a11, aba5.

0.5::a5:- \+a6,\+a7,\+a8,\+a9,\+a10,\+a11,a12, aba5.

abducible aba6.
0.5::a6:- a7, aba6.

0.5::a6:- \+a7,a8, aba6.

0.5::a6:- \+a7,\+a8,a9, aba6.

0.5::a6:- \+a7,\+a8,\+a9,a10, aba6.

0.5::a6:- \+a7,\+a8,\+a9,\+a10,a11, aba6.

0.5::a6:- \+a7,\+a8,\+a9,\+a10,\+a11,a12, aba6.

abducible aba7.
0.5::a7:- a8, aba7.

0.5::a7:- \+a8,a9, aba7.

0.5::a7:- \+a8,\+a9,a10, aba7.

0.5::a7:- \+a8,\+a9,\+a10,a11, aba7.

0.5::a7:- \+a8,\+a9,\+a10,\+a11,a12, aba7.

abducible aba8.
0.5::a8:- a9, aba8.

0.5::a8:- \+a9,a10, aba8.

0.5::a8:- \+a9,\+a10,a11, aba8.

0.5::a8:- \+a9,\+a10,\+a11,a12, aba8.

abducible aba9.
0.5::a9:- a10, aba9.

0.5::a9:- \+a10,a11, aba9.

0.5::a9:- \+a10,\+a11,a12, aba9.

abducible aba10.
0.5::a10:- a11, aba10.

0.5::a10:- \+a11,a12, aba10.

abducible aba11.
0.5::a11:- a12, aba11.

abducible aba12.
0.5::a12:- aba12.

0.5:- r. 

r:- \+aba0.
r:- \+aba1.
r:- \+aba2.
r:- \+aba3.
r:- \+aba4.
r:- \+aba5.
r:- \+aba6.
r:- \+aba7.
r:- \+aba8.
r:- \+aba9.
r:- \+aba10.
r:- \+aba11.
r:- \+aba12.

ev :- a0.

:- end_lpad.

run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

