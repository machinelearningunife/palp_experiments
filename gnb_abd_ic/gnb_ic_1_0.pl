:- use_module(library(pita)).


:- pita.

:- begin_lpad.
% CP-theory for size 1

abducible aba0.
0.5::a0:- aba0.

:- r. 

r:- \+aba0.

ev :- a0.
:- end_lpad.

run:-
	statistics(runtime, [Start | _]), 
	abd_prob(ev, Prob, Abd),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).

