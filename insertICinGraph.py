if __name__ == "__main__":
    for graph_size in range(50, 101, 10):
        for i in range(1, 11):

            to_read = "graphs_abd_small/" + str(graph_size) + "/"
            # if i == 10:
                # to_read = to_read + "graph_" + str(i) + ".pl"
            # else:
            to_read = to_read + "graph_" + str(i) + ".pl"
            
            f = open(to_read, "r")
            to_write = to_read.replace('graphs_abd', 'graphs_abd_ic')
            f_write = open(to_write, "w")

            printed = 0

            for line in f:
                if line.startswith('abducible'):
                    print(line, file=f_write)
                elif line == ':- end_lpad.\n':
                    # inserisco vincoli - rimuovo percorsi di lunghezza 2, 3, 4 e 5
                    print("\n:- path(0,",str(graph_size - 1),",2).", file=f_write)
                    print(":- path(0,",str(graph_size - 1),",3).", file=f_write)
                    print(":- path(0,",str(graph_size - 1),",4).", file=f_write)
                    print(":- path(0,",str(graph_size - 1),",5).\n", file=f_write)
                    print(line,file=f_write)
                    # print(line)
                    # print('p:0.5.')
                elif line.startswith("path"):
                    if printed == 0:
                        print("path(A, B, N):-\n\tpath_(A, B, L),\n\tflatten(L, LF),\n\tlength(LF, N).\n\npath_(X, X, X).\npath_(X, Y, [Y | T]):- path_(X, Z, T), edge(Z, Y).", file=f_write)
                        printed = 1
                elif line.startswith("ev"):
                    print("ev:- path(0,",str(graph_size - 1),",_).",file=f_write )
                else:
                    line = line.replace('\n', '')
                    print(line, file=f_write)
    
            print('Generated dataset: ' + to_write)
            f.close()
            f_write.close()
