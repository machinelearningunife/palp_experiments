if __name__ == "__main__":
    for s in range(5, 101, 1):
        to_write = "graph_complete/graph" + str(s) + ".pl"
        to_write_ic = "graph_complete_ic/graph" + str(s) + ".pl"

        f_write = open(to_write, "w")
        f_write_ic = open(to_write_ic, "w")

        f_write.write(":- use_module(library(pita)).\n:- pita.\n:- begin_lpad.\npath(A, B, N):-\npath_(A, B, L),flatten(L, LF),length(LF, N).\npath_(X, X, X).\npath_(X, Y, [Y | T]):- path_(X, Z, T), edge(Z, Y).\n\n")
        f_write_ic.write(":- use_module(library(pita)).\n:- pita.\n:- begin_lpad.\npath(A, B, N):-\npath_(A, B, L),flatten(L, LF),length(LF, N).\npath_(X, X, X).\npath_(X, Y, [Y | T]):- path_(X, Z, T), edge(Z, Y).\n\n")

        # f_write.write("edge(A,B):0.5:- e(A,B).\n\n")
        # f_write_ic.write("edge(A,B):0.5:- e(A,B).\n\n")

        for i in range(1,s+1):
            for j in range(1,i):
                # abducible e(1,2)
                # edge(a,b):0.5:- e(1,2).
                if i != j:
                    print("0.5::edge(", str(j), ",", str(i), "):- e",str(j), str(i), ".\n", file=f_write,sep='')
                    print("0.5::edge(", str(j), ",", str(i), "):- e",str(j), str(i), ".\n", file=f_write_ic,sep='')
                    print("abducible e",str(j),str(i),".\n",file=f_write,sep='')
                    print("abducible e",str(j),str(i),".\n",file=f_write_ic,sep='')
        
        print("ev:- path(1,", str(s),",_).\n:- end_lpad.\n\n", file=f_write,sep='')
        print("ev:- path(1,", str(s), ",_).\n:- path(1,", str(s),",2).\n:- path(1,", str(s), ",3).\n:-path(1,", str(s), ",4).\n:- end_lpad.\n\n", file=f_write_ic,sep='')
        print("run:- statistics(runtime, [Start | _]),abd_prob(ev, Prob, Abd),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).", file=f_write,sep='')
        print("run:- statistics(runtime, [Start | _]),abd_prob(ev, Prob, Abd),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Prob ~w, Abd ~w: Runtime: ~w~n', [Prob, Abd, Runtime]).", file=f_write_ic,sep='')

        f_write.close()
        f_write_ic.close()
